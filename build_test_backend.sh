#!/usr/bin/bash

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

echo 'Building backend'

cd "${PROJECT_DIRECTORY}/backend"

###
### setting env variables for backend begin ###

export APP_NAME="JobNo.One Career Pages"
export APP_ENV="testing"
export APP_KEY="base64:ZAx440BKQn4HgxvzH1yVPL5oLNX6RNI0g2m6w7ajsDw="
export APP_DEBUG="false"
export APP_URL="http://job-career-admin"
export APP_TIMEZONE="Europe/Prague"
export APP_LOCALE="en"

export LOG_CHANNEL="daily"
export TELESCOPE_ENABLED="false"

export DB_CONNECTION="mysql"
export DB_HOST="127.0.0.1"
export DB_PORT="3306"
export DB_DATABASE="pipelines"
export DB_USERNAME="homestead"
export DB_PASSWORD="secret"

export BROADCAST_DRIVER="log"
export CACHE_DRIVER="array"
export QUEUE_CONNECTION="sync"
export SESSION_DRIVER="file"
export SESSION_LIFETIME="120"

export MAIL_MAILER="smtp"
export MAIL_FROM_ADDRESS="info@job-career-admin.dev"
export MAIL_FROM_NAME=$APP_NAME

# file system configurations
export ALLOWED_FILE_EXTENSIONS="pdf,jpg,jpeg,png,docx,zip"
export ALLOWED_FILE_CATEGORIES="temporary,avatar,cv"

# mhub configurations
export MHUB_BASEURL="https://mhub-stage.service.jobno.one/api/v1"
export MHUB_DEFAULT_RECORDS_PER_PAGE="100"

# system configurations
export SUPER_ADMIN_EMAIL="admin@job-career-admin"
export SUPPORT_EMAIL="support@job-career-admin"
export DEFAULT_RECORDS_PER_PAGE="10"
# this param is from Job#1 Demo to set the grace period before deleting a candidate who's CV failed to get parsed by mHub
export DELETE_FAILED_CANDIDATES_AFTER_HOURS="24"
# https://www.php.net/manual/en/dateinterval.construct.php
export GDPR_AGREED_PERIOD="P10Y"
# https://www.php.net/manual/en/dateinterval.construct.php
export GDPR_DISAGREED_PERIOD="P3M"
# cron notation. See https://ole.michelsen.dk/blog/schedule-jobs-with-crontab-on-mac-osx/
export GET_VACANCIES_FROM_MHUB_FREQUENCY="*/30 * * * *"
# system configurations - notification configurations
export NOTIFICATIONS_SEND_EMAIL="true"
export NOTIFICATIONS_SEND_SMS="false"
export NOTIFICATIONS_SEND_SLACK="false"
export NOTIFICATIONS_SMS_SERVICE_PROVIDER=""

# company configurations for the MVP
export COMPANY_NAME="Test Company"
export COMPANY_CONTACT_EMAIL="contact@job-career.dev"
export COMPANY_FE_URL="job-career.dev"
# this param sets the grace period in hours before deleting incomplete candidates. i.e. candidates without an email.
export COMPANY_DELETE_INCOMPLETE_CANDIDATES_OLDER_THAN="1"

### setting env variables for backend ends ###
###

composer install

php artisan migrate --force --env=testing
php artisan db:seed --force --env=testing
php artisan cms:populate-permissions

npm install
npm run dev

chown -R www:www bootstrap/cache
chown -R www:www storage

echo 'Testing backend'

php artisan migrate:fresh --force --env=testing
php artisan test --exclude-group ignore
