## Handling translations for JobOne

### Introduction

This document will explain how the translations are achieved, and how new translations should be added.

### Translation setup

- As of the present day, the language for a particular environment is defined as part of the build params. More information can be found in the README.
- Currently, no 3rd party translation libraries are used to achieve translations, as these are dependent on SSR, and we are currently requested to go with SSG.
- A ‘useTranslation’ hook is defined in hooks\useTranslation.js. The exported ‘t’ function can be used to achieve a translation for a given key

### Locales folder

Currently, we have the following structure in the locales folder, where all our translations are stored:

- Within locales folder, there is a subfolder for each language code.
- Within each subfolder, you will find files similar to the following:

Common.json – contains common translations for all customers who use that particular language

Default.json - contains specific translations for default customer

<customer>.json - contains specific translations for specific customer

- If a translation is defined in both common.json and <customer>.json, then <customer>.json’s value will get selected as the translation.
- Translation keys must be in camelCase.

### Adding new translation

When adding a new translation, firstly you need to check if it is going to be used commonly among all customers. In that case, the common.json file for **all language codes** must be updated (e.g. \en\common.json and \cs\common.json).

If a new translation is added to default.json, then remember to add it to **all language codes’** default.json.

If it is going to be used by a specific customer, then you can add it only to that customer’s json file.

### Parameterization of translations

We have followed the standard approach for parameterization, like in standard libraries such as react-i18next.

- #### Correct approach:

We can add placeholder texts within the translation strings. E.g. in the following string, **noOfJobs** is a placeholder that will be replaced by a value at runtime.

Translation file:

"weFoundNoOfJobs": "We found <b>{{ noOfJobs }} jobs</b> that we think you’ll love!"

Component file:

**t**("weFoundNoOfJobs", { noOfJobs: openVacancies.length })

The values that placeholder texts must replace, must be passed in as the second argument of the ‘t’ function, as a javascript object. In the above example, **openVacancies.length** will replace **noOfJobs** at runtime.

Placeholder texts must be surrounded by 2 sets of curly braces on either side.

- #### Incorrect approach:

Instead of using parameterization as above, the developer maybe tempted to break the string into translation strings. E.g.

`${t(“weFound”} <b>${openVacancies.length}</b> ${t(‘thatWeThinkYoullLove’)}`

The problem of this approach is, that in non-English languages, the order of words maybe different to express the same meaning. So we cannot actually map these translation strings to non-English languages, as the position of the words within the sentence are different.
