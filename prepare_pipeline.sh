#!/usr/bin/bash

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

# Set Environment Variables
#
# set bash commands to non-interactive mode
export DEBIAN_FRONTEND="noninteractive"

# install packages
LC_ALL=en_US.UTF-8 apt-get update && \
    apt-get upgrade -y && \
    # install applications
    apt-get install -y --no-install-recommends software-properties-common \
    curl \
    gettext-base \
    language-pack-en-base \
    openssl \
    zip unzip && \
    # install PHP and PHP libraries
    LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y --no-install-recommends php7.4-fpm \
    php7.4-bcmath \
    php7.4-ctype \
    php7.4-curl \
    php7.4-fileinfo \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-memcached \
    php7.4-mysql \
    php7.4-pdo \
    php7.4-redis \
    php7.4-tidy \
    php7.4-tokenizer \
    php7.4-xml \
    php7.4-xmlrpc \
    php7.4-zip

# do the cleaning and configurations
apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i -e 's/;\?daemonize\s*=\s*yes/daemonize = no/g' /etc/php/7.4/fpm/php-fpm.conf && \
    mkdir -p /run/php/ \
    # install composer
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.0.6 && \
    # add user group and user
    groupadd -g 1000 www && \
    useradd -u 1000 -ms /bin/bash -g www www && \
    # create log directories and log files
    touch /var/log/php7.4-fpm.log

# install Node.js 16.8.0
export NODE_VERSION="16.8.0"
export NPM_FETCH_RETRIES="2"
export NPM_FETCH_RETRY_FACTOR="10"
export NPM_FETCH_RETRY_MINTIMEOUT="10000"
export NPM_FETCH_RETRY_MAXTIMEOUT="60000"
export NVM_DIR="/home/www/.nvm"

mkdir -p "$NVM_DIR" && \
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash && \
    . "$NVM_DIR/nvm.sh" && \
    nvm install ${NODE_VERSION} && \
    nvm use ${NODE_VERSION} && \
    nvm alias ${NODE_VERSION} && \
    npm config set fetch-retries ${NPM_FETCH_RETRIES} && \
    npm config set fetch-retry-factor ${NPM_FETCH_RETRY_FACTOR} && \
    npm config set fetch-retry-mintimeout ${NPM_FETCH_RETRY_MINTIMEOUT} && \
    npm config set fetch-retry-maxtimeout ${NPM_FETCH_RETRY_MAXTIMEOUT} && \
    npm install -g gulp && \
    npm install -g cross-env
# wouldn't execute when added to the RUN statement in the above block
ln -s "npm bin --global" /home/www/.node-bin
# wouldn't execute when added to the RUN statement in the above block
# Source NVM when loading bash since ~/.profile isn't loaded on non-login shell
echo "" >> ~/.bashrc && \
    echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

echo "" >> ~/.bashrc && \
    echo 'export NVM_DIR="/home/www/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

# add PATH for node
export PATH="$PATH:/home/www/.node-bin"

# Make it so the node modules can be executed.
# We'll create symbolic links into '/usr/local/bin'.
find $NVM_DIR -type f -name node -exec ln -s {} /usr/local/bin/node \; && \
    NODE_MODS_DIR="$NVM_DIR/versions/node/$(node -v)/lib/node_modules" && \
    ln -s "$NODE_MODS_DIR/bower/bin/bower" /usr/local/bin/bower && \
    ln -s "$NODE_MODS_DIR/gulp/bin/gulp.js" /usr/local/bin/gulp && \
    ln -s "$NODE_MODS_DIR/npm/bin/npm-cli.js" /usr/local/bin/npm && \
    ln -s "$NODE_MODS_DIR/npm/bin/npx-cli.js" /usr/local/bin/npx && \
    ln -s "$NODE_MODS_DIR/vue-cli/bin/vue" /usr/local/bin/vue && \
    ln -s "$NODE_MODS_DIR/vue-cli/bin/vue-init" /usr/local/bin/vue-init && \
    ln -s "$NODE_MODS_DIR/vue-cli/bin/vue-list" /usr/local/bin/vue-list
