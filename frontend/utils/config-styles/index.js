import defaultStyles from "../../config.default.json";
const clientStyles = require(`../../config.chtest.json`);

const ConfigStyles = () => {
  const returnStyles = () => {
    if (
      !clientStyles ||
      !clientStyles.styles ||
      !Object.keys(clientStyles.styles).length
    ) {
      return defaultStyles.styles;
    } else {
      const newStyles = {};
      for (let key in defaultStyles.styles) {
        newStyles[key] = {
          ...defaultStyles.styles[key],
          ...clientStyles.styles[key],
        };
      }

      return newStyles;
    }
  };

  const getStyleObject = (defaultOrderArray, clientOrder) => {
    if (!clientOrder || !clientOrder.length) {
      return defaultOrderArray;
    }
    return clientOrder;
  };

  const ordersArrayForArrange = [
    "componentsOrderHome",
    "componentsOrderDetail",
    "opportunitiesOrder",
    "settingsOrder",
  ];

  const arrangedOrders = {};

  ordersArrayForArrange.forEach((item) => {
    arrangedOrders[item] = getStyleObject(
      defaultStyles[item],
      clientStyles[item]
    );
  });

  const globalPropsArrayToArrange = [
    "gallery",
    "agreement",
    "fonts",
    "upload",
    "loading",
    "results",
    "manualFilters",
    "opportunitiesList",
    "jobAlertNotification",
    "createJobAlert",
    "jobDescription",
    "applyForJob",
    "layout",
    "mainOpportunities",
    "googleTagManager",
    "text",
    "contactFloatingForm",
  ];

  const arrangedGlobalProps = {};

  globalPropsArrayToArrange.forEach((item) => {
    arrangedGlobalProps[item] = {
      ...defaultStyles[item],
      ...clientStyles[item],
    };
  });

  const styles = {
    logo: clientStyles.logo ?? defaultStyles.logo,
    icon: clientStyles.favicon,
    logoLink: clientStyles?.logoLink,
    logoPosition: clientStyles.logoPosition ?? defaultStyles.logoPosition,
    openNewWindow: clientStyles.openNewWindow ?? defaultStyles.openNewWindow,
    defaultFont: clientStyles.defaultFont ?? defaultStyles.defaultFont,
    styles: returnStyles(),
    ...arrangedGlobalProps,
    metaTags: {
      Home: { ...defaultStyles.metaTags.Home, ...clientStyles?.metaTags?.Home },
      opportunities: {
        ...defaultStyles.metaTags.opportunities,
        ...clientStyles?.metaTags?.opportunities,
      },
    },
    ...arrangedOrders,
  };
  return styles;
};
export default ConfigStyles;
