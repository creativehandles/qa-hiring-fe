const isRunningOnServer = typeof window === "undefined";

export const setGDPR = (value) => {
  localStorage.setItem("GDPRagreed", value);
};

export const getGDPR = () => {
  if (isRunningOnServer) {
    // when running on server, there is no concept of localStorage
    return "";
  }

  const value = localStorage.getItem("GDPRagreed");
  return value && value === "true";
};

export const clearGDPR = () => {
  localStorage.removeItem("GDPRagreed");
};
