export const getLanguage = () => {
  const language = process.env.NEXT_PUBLIC_LANGUAGE;

  if (!language) {
    throw new Error("Language param must be defined in build arguments");
  }

  return language;
};
