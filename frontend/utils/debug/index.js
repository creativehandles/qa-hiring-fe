const isRunningOnServer = typeof window === "undefined";

export const logInfoToConsole = (text) => {
  if (!isRunningOnServer) {
    console.log(text);
  }
};

export const logObjectToConsole = (text, object) => {
  if (!isRunningOnServer) {
    console.log(text, object);
  }
};
