export const getAPIURL = () => {
  let apiURL = "https://6206545d92dd6600171c09b1.mockapi.io/api/v1/";

  if (!apiURL) {
    throw new Error(
      "Could not retrieve NEXT_PUBLIC_API_URL environment variable"
    );
  }

  return apiURL;
};
