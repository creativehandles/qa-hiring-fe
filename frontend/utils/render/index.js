import { Detail } from "../../components/detail";
import { Upload } from "../../components/upload";
import { UploadHome } from "../../components/upload-home";
import { TextSection } from "../../functional-components/text-section";
import { OpportunitiesDesktop } from "../../components/opportunities-desktop";
import { Settings } from "../../components/settings";
import { mobileBreakpoint } from "../../constants/appConstants";
import { ContactFloatingForm } from "../../functional-components/contact-floating-form";
import { ContactFloatingMobile } from "../../functional-components/contact-floating-mobile";

export const renderUpload = (el, width, index) => {
  switch (el.componentName) {
    case "text":
      return (
        <TextSection key={index} screenWidth={width} {...el.componentProps} />
      );
    case "main":
      if (width) {
        return width > mobileBreakpoint ? (
          <UploadHome key={index}></UploadHome>
        ) : (
          <Upload key={index} enabledFetchDefaultVacancies></Upload>
        );
      }
      return null;
    case "contactForm":
      if (width && el.componentProps.showContactForm) {
        return width > mobileBreakpoint ? (
          <ContactFloatingForm screenWidth={width} key={index}></ContactFloatingForm>
        ) : (
          <ContactFloatingMobile
            enabledFetchDefaultVacancies={false}
            key={index}
          ></ContactFloatingMobile>
        );
      }
      return null;
    default:
      return null;
  }
};
export const renderDetail = (el, job) => {
  switch (el.componentName) {
    case "main":
      return <Detail job={job} />;
    default:
      return null;
  }
};

export const renderOpportunities = (el, index) => {
  switch (el.componentName) {
    case "main":
      return (
        <OpportunitiesDesktop
          key={index}
          {...el.componentProps}
        ></OpportunitiesDesktop>
      );
    default:
      return null;
  }
};

export const renderSettingsPage = (el, width) => {
  switch (el.componentName) {
    case "main":
      return <Settings mobile={width > 910 ? false : true} />;
    default:
      return null;
  }
};
