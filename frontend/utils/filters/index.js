import { postingAges } from "../../constants/postingAges";
import store from "../../redux";

export const appendFilterQueryParams = (uRLInput) => {
  const { filters } = store.getState();
  let url = new URL(uRLInput);
  if (filters) {
    if (filters.scheduleMin >= 0 && filters.scheduleMax) {
      // fteMin and fteMax are between 0 and 1
      const scheduleMin = filters.scheduleMin / 100;
      const scheduleMax = filters.scheduleMax / 100;
      // url.searchParams.set("fteMin", scheduleMin);
      // url.searchParams.set("fteMax", scheduleMax);
    }
    if (filters.postingAgeMin && filters.postingAgeMax) {
      const postingAgeMinDbAge = postingAges.find(
        (age) => age.label === filters.postingAgeMin
      );
      const postingAgeMaxDbAge = postingAges.find(
        (age) => age.label === filters.postingAgeMax
      );
      if (postingAgeMinDbAge && postingAgeMaxDbAge) {
        // url.searchParams.set("fromDate", postingAgeMinDbAge.dbValue);
        // url.searchParams.set("toDate", postingAgeMaxDbAge.dbValue);
      }
    }
  }

  return url;
};
