import store from "../../redux";
import { appendFilterQueryParams } from "../filters";

export const appendQueryParams = (uRLInput) => {
  const { query } = store.getState();
  let url = new URL(uRLInput);
  url = appendFilterQueryParams(url);
  if (query.query) {
    url.searchParams.set("q", query.query);
  }
  return url;
};
