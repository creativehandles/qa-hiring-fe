const isRunningOnServer = typeof window === "undefined";

export const setInputs = (value) => {
  localStorage.setItem("VacancyFormInputs", JSON.stringify(value));
};

export const getInputs = () => {
  if (isRunningOnServer) {
    // when running on server, there is no concept of localStorage
    return "";
  }

  const inputsFromStorage = JSON.parse(
    localStorage.getItem("VacancyFormInputs")
  );

  const vacancyFormData = {
    first_name: inputsFromStorage && inputsFromStorage.first_name,
    last_name: inputsFromStorage && inputsFromStorage.last_name,
    email: inputsFromStorage && inputsFromStorage.email,
    phone: inputsFromStorage && inputsFromStorage.phone,
  };

  return vacancyFormData;
};

export const clearInputs = () => {
  localStorage.removeItem("VacancyFormInputs");
};
