const prettyUrl = (string) => {
  const URL = string
    .split(" ")
    .filter((el) => el !== "-" && el !== " " && el)
    .join("-");
  return URL;
};

export default prettyUrl;
