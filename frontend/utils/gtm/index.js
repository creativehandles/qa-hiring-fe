export const getGTMID = () => {
  // eslint-disable-next-line no-undef
  const gTMID = process.env.NEXT_PUBLIC_GTM_ID;

  // check for the text INVALID. There is no way to pass null values via cmd line
  return gTMID === "INVALID" ? "" : gTMID;
};

export const pushToDataLayer = (data) => {
  if (
    !(
      getGTMID() &&
      typeof window !== "undefined" &&
      typeof window.dataLayer !== "undefined"
    )
  ) {
    return;
  }

  // IMPORTANT NOTE: All possible variables that can be passed into this function MUST be
  // initialized here to 'undefined'. Otherwise the behaviour mentioned in
  // https://www.simoahava.com/gtm-tips/remember-to-flush-unused-data-layer-variables/ will occur
  const dataToPush = {
    GAID: undefined,
    tenantID: undefined,
    tenantGAID: undefined,
    tenantFBID: undefined,
    tenantLIID: undefined,
    event: undefined,
    step: undefined,
    GAfourID: undefined,
    tenantGAfourID: undefined,
    tenantADWpixel: undefined,
    tenantADWlabel: undefined,
    tenantSklikID: undefined,
    tenantSklikKonverze: undefined,
    ...data,
  };

  Object.entries(dataToPush).map(([key, value]) => {
    if (value === undefined && key !== "event" && key !== "step") {
      delete dataToPush[key];
    }
  });

  window.dataLayer.push(dataToPush);
};
