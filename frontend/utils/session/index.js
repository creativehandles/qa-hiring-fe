import store from "../../redux";
import { removeCV, removeReserve } from "../../redux/slices/CV";
import { clearGDPR } from "../gdpr";
import { clearInputs } from "../vacancy-form-inputs";

export const manageUserSession = () => {
  const { dispatch } = store;
  const lastUserActivityDateString = localStorage.getItem("lastUserActivity");
  if (lastUserActivityDateString) {
    const lastUserActivityDateObject = new Date(lastUserActivityDateString);
    if (!isNaN(lastUserActivityDateObject.valueOf())) {
      const refDate = new Date();
      refDate.setHours(refDate.getHours() - 1);
      if (lastUserActivityDateObject < refDate) {
        dispatch(removeCV());
        dispatch(removeReserve());
        clearGDPR();
        clearInputs();
      }
    }
  }
  // store current date
  localStorage.setItem("lastUserActivity", new Date());
};
