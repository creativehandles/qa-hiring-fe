/* eslint-disable no-undef */
import React, { createContext } from "react";
import { getLanguage } from "../utils/translations";

const language = getLanguage();

const commonTranslations = require(`../locales/cs/common.json`);
const specificTranslations = require(`../locales/cs/chtest.json`);

/**
 * Language Context
 * params:
 *    localization;
 */
export const LanguageContext = createContext({
  localization: {
    commonTranslations,
    specificTranslations,
  },
});
