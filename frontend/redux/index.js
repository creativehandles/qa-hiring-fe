import { configureStore } from "@reduxjs/toolkit";
import styles from "./slices/global-styles";
import CV from "./slices/CV";
import modal from "./slices/modals";
import defaultVacancies from "./slices/vacancies/default";
import openVacancies from "./slices/vacancies/open-vacancies";
import vacancy from "./slices/vacancies/current_vacancy";
import candidate from "./slices/candidate";
import notifications from "./slices/notifications";
import filters from "./slices/filters";
import query from "./slices/query";
import allOpportunities from "./slices/all-opportunities";
import contactForm from "./slices/contact";

export default configureStore({
  reducer: {
    modal,
    CV,
    defaultVacancies,
    openVacancies,
    vacancy,
    candidate,
    styles,
    notifications,
    filters,
    query,
    allOpportunities,
    contactForm
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
