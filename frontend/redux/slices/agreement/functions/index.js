import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { getGDPR } from "../../../../utils/gdpr";

export const setGDPR = createAsyncThunk("setGDPR", async (apiURL, thunkAPI) => {
  const state = thunkAPI.getState();
  const { id } = state.candidate.value;
  const agreement = getGDPR();
  const data = {
    gdpr_agreed: agreement,
  };
  try {
    await axios.put(`${apiURL}candidates/${id}/set-gdpr`, data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
      },
    });
  } catch (error) {
    console.log(error);
  }
});

const extraReducers = {
  [setGDPR.fulfilled]: (state, action) => {},
};

export default extraReducers;
