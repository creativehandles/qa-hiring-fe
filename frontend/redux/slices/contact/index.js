import { createSlice } from "@reduxjs/toolkit";
import extraReducers from "./functions";

export const contactForm = createSlice({
  name: "contactForm",
  initialState: {
    value: null,
    loading: false,
    showSuccessDialog: false,
    showContactForm: true,
  },
  reducers: {
    setSuccessDialog: (state, action) => {
      return { ...state, showSuccessDialog: action.payload };
    },
    setContactFormVisibility: (state, action) => {
      return { ...state, showContactForm: action.payload };
    },
  },
  extraReducers,
});

export const { setSuccessDialog, setContactFormVisibility } =
  contactForm.actions;

export default contactForm.reducer;
