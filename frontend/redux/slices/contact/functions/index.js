import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const submitContactInfo = createAsyncThunk(
  "contactInfo",
  async (payload) => {
    const uRL = `${payload.apiURL}general/contact`;

    const response = await axios.post(uRL, payload.data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
      },
    });
    return response.data;
  }
);

const extraReducers = {
  [submitContactInfo.pending]: (state) => {
    return { ...state, loading: true };
  },
  [submitContactInfo.rejected]: (state) => {
    return { ...state, loading: false };
  },
  [submitContactInfo.fulfilled]: (state, action) => {
    return {
      ...state,
      value: action.payload,
      loading: false,
      showSuccessDialog: true,
    };
  },
};

export default extraReducers;
