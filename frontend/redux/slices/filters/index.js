import { createSlice } from "@reduxjs/toolkit";
import { initialFilterState } from "../../../functional-components/filters/filter-container";
import equal from "deep-equal";

export const filtersSlice = createSlice({
  name: "filters",
  initialState: initialFilterState,
  reducers: {
    setFilters: (state, action) => {
      // reduce unneccessary re-rendering
      if (equal(state, action.payload)) {
        return state;
      }
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { setFilters } = filtersSlice.actions;

export default filtersSlice.reducer;
