import { createAsyncThunk } from "@reduxjs/toolkit";
import { appendQueryParams } from "../../../../../utils/query-params";
import axios from "axios";

export const fetchDefaultVacancies = createAsyncThunk(
  "defaultVacancies",
  async (payload) => {
    const uRL = `${payload.apiURL}vacancies`;
    const uRLWithQueryParams = appendQueryParams(uRL);

    const response = await axios.get("https://6206545d92dd6600171c09b1.mockapi.io/api/v1/", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
      },
    });
    return response.data.data.items;
  }
);

const extraReducers = {
  [fetchDefaultVacancies.pending]: (state) => {
    return { ...state, loading: true };
  },
  [fetchDefaultVacancies.rejected]: (state) => {
    return { ...state, loading: false };
  },
  [fetchDefaultVacancies.fulfilled]: (state, action) => {
    return { ...state, value: action.payload, loading: false };
  },
};

export default extraReducers;
