import { createSlice } from "@reduxjs/toolkit";
import extraReducers from "./functions";

export const defaultVacancies = createSlice({
  name: "defaultVacancies",
  initialState: {
    value: [],
    loading: true,
  },
  extraReducers,
});

// export const { upload, remove } = defaultVacancies.actions;

export default defaultVacancies.reducer;
