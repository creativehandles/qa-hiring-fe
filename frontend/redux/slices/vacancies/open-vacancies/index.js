import { createSlice } from "@reduxjs/toolkit";
import extraReducers from "./functions";

export const openVacancies = createSlice({
  name: "openVacancies",
  initialState: {
    loading: false,
    value: [],
  },
  reducers: {
    clearOpenVacancies: (state) => {
      return {
        ...state,
        value: [],
      };
    },
  },

  extraReducers,
});

export const { clearOpenVacancies } = openVacancies.actions;

export default openVacancies.reducer;
