import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { setCandidate } from "../../../../candidate";
import { setInputs } from "../../../../../../utils/vacancy-form-inputs";

export const submitReserveCV = createAsyncThunk(
  "submitReserveCV",
  async (payload, thunkAPI) => {
    const dispatch = thunkAPI.dispatch;
    const formData = new FormData();
    formData.append("cv", payload.file);
    const uRL = `${payload.apiURL}anonymous/search-vacancies-for-cv`;

    const response = await axios.post(uRL, formData, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
      },
    });
    const candidateData = response.data.data.candidate;
    localStorage.setItem("candidate", JSON.stringify(candidateData));
    dispatch(setCandidate(candidateData));

    const vacancyFormData = {
      first_name: candidateData.first_name,
      last_name: candidateData.last_name,
      email: candidateData.email,
      phone: candidateData.phone,
    };
    setInputs(vacancyFormData);
    return response.data;
  }
);

const extraReducers = {
  [submitReserveCV.pending]: (state) => {
    return { ...state, loadingReserveCV: true };
  },
  [submitReserveCV.rejected]: (state, action) => {
    return { ...state, loadingReserveCV: false, message: action.error.message };
  },
  [submitReserveCV.fulfilled]: (state) => {
    return {
      ...state,
      loadingReserveCV: false,
    };
  },
};

export default extraReducers;
