import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { setCandidate } from "../../../../candidate";
import { setInputs } from "../../../../../../utils/vacancy-form-inputs";
import { setNotification } from "../../../../../../redux/slices/notifications";
import { removeCV } from "../../../../../../redux/slices/CV";
import { pushToDataLayer } from "../../../../../../utils/gtm";
import { logInfoToConsole } from "../../../../../../utils/debug";

export const fetchVacanciesForCV = createAsyncThunk(
  "fetchVacanciesForCV",
  async (payload, thunkAPI) => {
    const { errorCodesTranslated, apiURL } = payload;
    const cv = thunkAPI.getState().CV.value;
    const dispatch = thunkAPI.dispatch;
    const formData = new FormData();
    formData.append("cv", payload.file ? payload.file : cv);
    const uRL = `${apiURL}anonymous/search-vacancies-for-cv`;

    try {
      const response = await axios.post(uRL, formData, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
        },
      });
      const candidateData = response.data.data.candidate;
      localStorage.setItem("candidate", JSON.stringify(candidateData));
      dispatch(setCandidate(candidateData));

      const vacancyFormData = {
        first_name: candidateData.first_name,
        last_name: candidateData.last_name,
        email: candidateData.email,
        phone: candidateData.phone,
      };
      setInputs(vacancyFormData);
      pushToDataLayer({
        event: "cv",
        step: "cv uploaded success",
      });
      logInfoToConsole("dataLayer.push for upload CV");

      return response.data;
    } catch (err) {
      let message = errorCodesTranslated.find(
        (el) => el.code === "default"
      ).text;
      if (err.response && err.response.status) {
        const statusCode = err.response.status + ""; // convert to string
        const statusCodeElementMatched = errorCodesTranslated.find(
          (el) => el.code === statusCode
        );
        if (statusCodeElementMatched) {
          message = statusCodeElementMatched.text;
        }
      }

      dispatch(setNotification(message));
      dispatch(removeCV());

      return thunkAPI.rejectWithValue(err.response);
    }
  }
);

const extraReducers = {
  [fetchVacanciesForCV.pending]: (state) => {
    return { ...state, loading: true };
  },
  [fetchVacanciesForCV.rejected]: (state) => {
    return {
      ...state,
      loading: false,
    };
  },
  [fetchVacanciesForCV.fulfilled]: (state, action) => {
    return {
      ...state,
      loading: false,
      value: action.payload.data.vacancies.items,
    };
  },
};

export default extraReducers;
