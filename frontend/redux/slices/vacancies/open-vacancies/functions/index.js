import fetchVacanciesForCV from "./anonimus-search";
import fetchVacanciesForCandidate from "./search-for-candidate";
import submitReserveCV from "./reserve-cv";

const extraReducers = {
  ...fetchVacanciesForCV,
  ...fetchVacanciesForCandidate,
  ...submitReserveCV,
};

export default extraReducers;
