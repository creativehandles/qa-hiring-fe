import { createAsyncThunk } from "@reduxjs/toolkit";
import { appendQueryParams } from "../../../../../../utils/query-params";
import axios from "axios";

export const fetchVacanciesForCandidate = createAsyncThunk(
  "fetchVacanciesForCandidate",
  async (payload, thunkAPI) => {
    const { id } = thunkAPI.getState().candidate.value;
    const uRL = `${payload.apiURL}candidates/${id}/search-vacancies?with[]=city&with[]=country`;
    const uRLWithQueryParams = appendQueryParams(uRL);

    const response = await axios.get(uRLWithQueryParams, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
      },
    });
    return response.data.data.items;
  }
);

const extraReducers = {
  [fetchVacanciesForCandidate.pending]: (state) => {
    return { ...state, loading: true };
  },
  [fetchVacanciesForCandidate.rejected]: (state, action) => {
    return { ...state, message: action.error.message, loading: false };
  },
  [fetchVacanciesForCandidate.fulfilled]: (state, action) => {
    return {
      ...state,
      value: action.payload,
      loading: false,
    };
  },
};

export default extraReducers;
