import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchCurrentVacancy = createAsyncThunk(
  "vacancy",
  async (payload) => {
    const response = await axios.get(
      `${payload.apiURL}vacancies/${payload.id}?with[]=city&with[]=country`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
        },
      }
    );
    return response.data.data;
  }
);

const extraReducers = {
  [fetchCurrentVacancy.pending]: (state) => {
    return { ...state, message: "Loading" };
  },
  [fetchCurrentVacancy.rejected]: (state) => {
    return { ...state, message: "Error" };
  },
  [fetchCurrentVacancy.fulfilled]: (state, action) => {
    return {
      ...state,
      value: action.payload,
      curId: action.payload.id,
    };
  },
};

export default extraReducers;
