import { createSlice } from "@reduxjs/toolkit";
import extraReducers from "./functions";

export const currentVacancy = createSlice({
  name: "vacancy",
  initialState: {
    value: {},
    message: "",
    curId: null,
  },
  reducers: {
    clearVacancy: (state) => {
      return { ...state, value: {} };
    },
    clearMessage: (state) => {
      return { ...state, message: "", curId: null };
    },
    setCurId: (state, action) => {
      return { ...state, curId: action.payload };
    },
  },

  extraReducers,
});

export const { clearVacancy, clearMessage, setCurId } = currentVacancy.actions;

export default currentVacancy.reducer;
