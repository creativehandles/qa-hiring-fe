import { createSlice } from "@reduxjs/toolkit";

const allOpportunitiesSlice = createSlice({
  name: "allOpportunities",
  initialState: {
    seeAllOpportunities: false,
  },
  reducers: {
    setSeeAllOpportunities: (state, action) => {
      return { ...state, seeAllOpportunities: action.payload };
    },
  },
});

export const { setSeeAllOpportunities } = allOpportunitiesSlice.actions;

export default allOpportunitiesSlice.reducer;
