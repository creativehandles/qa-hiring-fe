import { createSlice } from "@reduxjs/toolkit";

export const querySlice = createSlice({
  name: "query",
  initialState: { query: "" },
  reducers: {
    setQuery: (state, action) => {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { setQuery } = querySlice.actions;

export default querySlice.reducer;
