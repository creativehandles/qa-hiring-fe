import { createSlice } from "@reduxjs/toolkit";
import ConfigStyles from "../../../utils/config-styles";
const styles = ConfigStyles();
export const globalStyles = createSlice({
  name: "globalStyles",
  initialState: {
    styles,
  },
  reducers: {
    uploadStyles: (state, action) => {
      return { ...state, styles: action.payload };
    },
  },
});
export const { uploadStyles } = globalStyles.actions;

export default globalStyles.reducer;
