import { createSlice } from "@reduxjs/toolkit";

export const notifications = createSlice({
  name: "notifications",
  initialState: {
    message: "",
    messageId: 0, // Keep  a track of which notification is current
  },
  reducers: {
    setNotification: (state, action) => {
      return {
        ...state,
        message: action.payload,
        messageId: state.messageId + 1,
      };
    },
    clearNotification: (state) => {
      return { ...state, message: "" };
    },
  },
});

export const { setNotification, clearNotification } = notifications.actions;

export default notifications.reducer;
