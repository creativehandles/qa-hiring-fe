import { createSlice } from "@reduxjs/toolkit";

const candidateSlice = createSlice({
  name: "candidate",
  initialState: {
    value: {},
  },
  reducers: {
    setCandidate: (state, action) => {
      return { ...state, value: action.payload };
    },
    clearCandidate: (state) => {
      return { ...state, value: {} };
    },
  },
});

export const { setCandidate, clearCandidate } = candidateSlice.actions;

export default candidateSlice.reducer;
