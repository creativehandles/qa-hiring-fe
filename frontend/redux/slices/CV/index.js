import { createSlice } from "@reduxjs/toolkit";
import { clearInputs } from "../../../utils/vacancy-form-inputs";

export const uploadCVSlice = createSlice({
  name: "CV",
  initialState: {
    value: "",
    reserveCV: "",
    uploadedThisSession: false,
  },
  reducers: {
    setCV: (state, action) => {
      return { ...state, value: action.payload };
    },
    setUploadedThisSession: (state, action) => {
      return { ...state, uploadedThisSession: action.payload };
    },
    removeCV: (state) => {
      localStorage.removeItem("candidate");
      clearInputs();
      return { ...state, value: "" };
    },
    setReserve: (state, action) => {
      return { ...state, reserveCV: action.payload };
    },
    removeReserve: (state) => {
      clearInputs();
      return { ...state, reserveCV: "" };
    },
  },
});

export const {
  removeCV,
  setCV,
  setUploadedThisSession,
  setReserve,
  removeReserve,
} = uploadCVSlice.actions;

export default uploadCVSlice.reducer;
