import { createSlice } from "@reduxjs/toolkit";

export const modalSlice = createSlice({
  name: "modal",
  initialState: {
    value: false,
  },
  reducers: {
    open: (state, action) => {
      return { ...state, value: action.payload };
    },
    close: (state) => {
      return { ...state, value: false };
    },
  },
});

export const { open, close } = modalSlice.actions;

export default modalSlice.reducer;
