import styled from "styled-components";
import { mobileBreakpoint } from "../../../constants/appConstants";

export const Container = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
`;

export const List = styled.div`
  display: grid;
  grid-gap: 10px;
  margin-bottom: 40px;
`;

export const ListItem = styled.div`
  border: ${(props) =>
    props.isCurrent
      ? props.theme.style.list.activeBorder
      : props.theme.style.list.border};
  background-color: ${(props) =>
    props.screenWidth < mobileBreakpoint
      ? props.theme.style.list.mobileBackground
      : props.theme.style.list.background};
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  cursor: pointer;
  border-radius: ${(props) => props.theme.style.list.borderRadius};
`;

export const JobTitle = styled.div`
  text-transform: uppercase;
  font-family: ${(props) => props.theme.style.list.titleFontFamily};
  font-weight: ${(props) => props.theme.style.list.titleWeight};
  font-size: ${(props) => props.theme.style.list.titleSize};
  color: ${(props) => props.theme.style.list.titleColor};
  line-height: ${(props) => props.theme.style.list.titleLineHeight};
  letter-spacing: ${(props) => props.theme.style.list.letterSpacing};
  display: flex;
  align-items: center;
`;

export const DescriptionContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 12px;
`;

export const Filter = styled.div`
  color: ${(props) =>
    props.filter ? props.theme.style.list.tagsColor : "#a63f3f"};
  background: ${(props) => props.theme.style.list.tagsBackground};
  line-height: ${(props) => props.theme.style.list.tagsLineHeight};
  font-weight: ${(props) => props.theme.style.list.tagsWeight};
  font-family: ${(props) => props.theme.style.list.tagsFontFamily};
  border-radius: ${(props) => props.theme.style.list.tagsBorderRadius};
  padding: 4px 7px;
  font-size: ${(props) => props.theme.style.list.tagsSize};
  margin-right: 10px;
`;

export const Payment = styled.div`
  background: ${(props) => props.theme.style.list.tagsBackground};
  line-height: ${(props) => props.theme.style.list.tagsLineHeight};
  font-weight: ${(props) => props.theme.style.list.tagsWeight};
  font-family: ${(props) => props.theme.style.list.tagsFontFamily};
  font-size: ${(props) => props.theme.style.list.tagsSize};
  color: ${(props) => props.theme.style.list.tagsColor};
  border-radius: ${(props) => props.theme.style.list.tagsBorderRadius};
  padding: 4px 7px;
  font-size: 12px;
`;

export const MissingContainer = styled.div`
  display: flex;
  align-items: center;
  & > span {
    margin-left: 5px;
  }
`;

export const Question = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-family: ${(props) => props.theme.style.list.questionFontFamily};
  text-align: center;
  margin-top: 10px;
`;

export const Percent = styled.div`
  color: ${(props) => props.theme.style.list.percentFontColor || "#3fa696"};
  display: flex;
  align-items: center;
  font-weight: 800;
  font-family: ${(props) => props.theme.style.list.percentFontFamily};
  font-size: ${(props) => props.theme.style.list.percentFontSize};
  &:after {
    content: "";
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background: #3f5aa6;
    display: inline-block;
    width: 3px;
    height: 3px;
    margin: 3px 5px;
  }
`;
