import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import {
  Container,
  ListItem,
  JobTitle,
  DescriptionContainer,
  Payment,
  List,
  Question,
  Percent,
} from "./style";
import prettyUrl from "../../../utils/pretty-url";
import { useSelector } from "react-redux";
import useTranslation from "../../../hooks/useTranslation";

export const ListOfOpportunities = ({
  data,
  withoutLoadButton = true,
  noRedirect,
}) => {
  const router = useRouter();
  const { showSalary } = useSelector(
    (state) => state.styles.styles.opportunitiesList
  );
  const { showPercent } = useSelector(
    (state) => state.styles.styles.opportunitiesList
  );
  const opportunities = data;
  const { t } = useTranslation();
  const file = useSelector((state) => state.CV.value);
  const [screenWidth, setScreenWidth] = useState();

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    const setScreenSize = (e) => {
      setScreenWidth(e.currentTarget.innerWidth);
    };

    window.addEventListener("resize", setScreenSize);
    return () => {
      window.removeEventListener("resize", setScreenSize);
    };
  }, []);

  const opportunitieItem = (item) => {
    const { id } = item;
    const onClickFunction = !noRedirect
      ? () => {
          router.push(
            {
              pathname: "/detail/[id]/[name]",
              query: { id, name: item.name },
            },
            `detail/${id}/${prettyUrl(item.slug)}`
          );
        }
      : () => {
          router.push(
            {
              pathname: "/opportunities/[id]/[name]",
              query: { id, name: item.name },
            },
            `/opportunities/${id}/${prettyUrl(item.slug)}`,
            { shallow: true }
          );
        };

    const percent = Math.ceil(item.mhub_relevance * 100) + "%";
    const hasPercent = file && <Percent>{percent}</Percent>;
    const salaryRate = `${Math.floor(item.salary_min / 1000)}${t(
      "k"
    )} - ${Math.floor(item.salary_max / 1000)}${t("kAYear")}`;

    return (
      <ListItem
        isCurrent={Number(router.query.id) === item.id}
        screenWidth={screenWidth}
        onClick={() => onClickFunction()}
        key={item.id}
      >
        <JobTitle>
          {showPercent && hasPercent}
          {item.name}
        </JobTitle>
        {showSalary && (
          <DescriptionContainer>
            <Payment>{salaryRate}</Payment>
          </DescriptionContainer>
        )}
      </ListItem>
    );
  };
  if (withoutLoadButton) {
    return (
      <Container>
        <List>{opportunities.map(opportunitieItem)}</List>
      </Container>
    );
  }
  return (
    <Container>
      <List>{opportunities.map(opportunitieItem)}</List>
      <Question>{t("didntFindWhatYoureLookingFor")}</Question>
      <Question>{t("opportunitiesListQuestion")}</Question>
    </Container>
  );
};
