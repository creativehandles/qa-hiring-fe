import React, { useState, createRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  UploadContainer,
  SvgContainer,
  Container,
  Title,
  Text,
  ViewButton,
  InnerContainer,
} from "./style";
import { setCV } from "../../redux/slices/CV";
import { ListOfOpportunities } from "../opportunities/list";
import { Upload } from "../svg-component";
import { setSeeAllOpportunities } from "../../redux/slices/all-opportunities";
import { NoOpenVacancies } from "../../functional-components/no-open-vacancies";
import useTranslation from "../../hooks/useTranslation";

export const DragAndDrop = ({ onlyCV }) => {
  const [isEnter, setIsEnter] = useState(false);
  const { t } = useTranslation();
  const { seeAllOpportunitiesOption } = useSelector(
    (state) => state.styles.styles.upload
  );
  const allOpportunitiesVisible = useSelector(
    (state) => state.allOpportunities.seeAllOpportunities
  );
  const dispatch = useDispatch();
  const defaultVacancies = useSelector((state) => state.defaultVacancies.value);
  const defaultVacanciesLoading = useSelector(
    (state) => state.defaultVacancies.loading
  );
  const opportunitiesRef = createRef();

  useEffect(() => {
    if (opportunitiesRef.current && allOpportunitiesVisible) {
      opportunitiesRef.current.scrollIntoView({
        behavior: "smooth",
      });
    }
  }, [opportunitiesRef, opportunitiesRef.current, allOpportunitiesVisible]);

  const shadowColor = (props) => {
    const color = !allOpportunitiesVisible
      ? props.theme.style.global.fontColor
      : props.theme.style.global.background;
    return color;
  };

  const uploadCV = (e) => {
    setIsEnter(false);
    e.stopPropagation();
    e.preventDefault();
    dispatch(setCV(e.dataTransfer.files[0]));
  };
  const onDragHandler = (e, over) => {
    e.stopPropagation();
    e.preventDefault();
    setIsEnter(over);
  };

  if (onlyCV) {
    return (
      <>
        <UploadContainer
          shadowColor={shadowColor}
          isEnter={isEnter}
          allVisible={allOpportunitiesVisible}
          onDragEnter={(e) => onDragHandler(e, true)}
          onDragOver={(e) => onDragHandler(e, true)}
          onDragLeave={(e) => onDragHandler(e, false)}
          onDrop={(e) => uploadCV(e)}
        >
          <span>{t("uploadButtonName")}</span>
          <SvgContainer allVisible={allOpportunitiesVisible}>
            <Upload />
          </SvgContainer>
          <input
            type="file"
            onChange={(e) => dispatch(setCV(e.target.files[0]))}
          />
        </UploadContainer>
      </>
    );
  }

  if (!defaultVacanciesLoading && !defaultVacancies.length) {
    return <NoOpenVacancies />;
  }

  return (
    <Container>
      <InnerContainer>
        <Title>
          {!allOpportunitiesVisible ? t("uploadTitle") : t("allOpportunities")}
        </Title>
        <UploadContainer
          shadowColor={shadowColor}
          isEnter={isEnter}
          allVisible={allOpportunitiesVisible}
          onDragEnter={(e) => onDragHandler(e, true)}
          onDragOver={(e) => onDragHandler(e, true)}
          onDragLeave={(e) => onDragHandler(e, false)}
          onDrop={(e) => uploadCV(e)}
        >
          <span>{t("uploadButtonName")}</span>
          <SvgContainer allVisible={allOpportunitiesVisible}>
            <Upload />
          </SvgContainer>
          <input
            type="file"
            onChange={(e) => dispatch(setCV(e.target.files[0]))}
          />
        </UploadContainer>
        {!allOpportunitiesVisible ? (
          <>
            <Text dangerouslySetInnerHTML={{ __html: t("uploadText") }} />
            {seeAllOpportunitiesOption === "true" && (
              <>
                <Text>{t("or")}</Text>
                <ViewButton
                  onClick={() => dispatch(setSeeAllOpportunities(true))}
                >
                  {t("uploadSeeAllOpportunitiesButtonName")}
                </ViewButton>
              </>
            )}
          </>
        ) : (
          <>
            <Text>{t("uploadTextMobile")}</Text>
            {!defaultVacanciesLoading && (
              <div ref={opportunitiesRef}>
                <ListOfOpportunities data={defaultVacancies} />
              </div>
            )}
          </>
        )}
      </InnerContainer>
    </Container>
  );
};
