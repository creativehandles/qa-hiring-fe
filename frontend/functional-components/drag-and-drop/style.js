import styled from "styled-components";

export const Container = styled.div`
  padding: 30px;
  width: 100%;
  background: ${(props) => props.theme.style.upload.background};
`;

export const InnerContainer = styled.div`
  margin: 0 auto;
  max-width: 376px;
`;

export const Title = styled.div`
  color: ${(props) => props.theme.style.upload.headerColorMobile};
  font-size: ${(props) => props.theme.style.upload.headerSizeMobile};
  font-weight: ${(props) => props.theme.style.upload.headerWeightMobile};
  font-family: ${(props) => props.theme.style.upload.headerFontFamily};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
  text-align: center;
  position: relative;

  &::after {
    content: "${(props) => props.theme.style.upload.afterClass}";
    color: #e00700;
    position: absolute;
    bottom: -8px;
    @media (max-width: 850px) {
      bottom: -5px;
    }
  }
`;

export const UploadContainer = styled.label`
  margin: 20px auto;
  cursor: pointer;
  padding: 20px;
  transition: 0.2s;
  align-items: center;
  display: flex;
  justify-content: space-around;
  flex-direction: ${(props) => (props.allVisible ? "column-reverse" : "row")};
  border-radius: ${(props) => props.theme.style.upload.uploadRadiusMobile};
  border: ${(props) =>
    !props.allVisible && props.theme.style.upload.uploadBorder};
  background-color: ${(props) =>
    props.allVisible
      ? props.theme.style.global.background
      : props.theme.style.upload.uploadBackground};
  & > span {
    font-family: ${(props) =>
      props.theme.style.upload.uploadDescriptionFontFamily};
    color: ${(props) => props.theme.style.upload.uploadDescriptionColor};
    font-size: ${(props) => props.theme.style.upload.uploadDescriptionSize};
    font-weight: ${(props) =>
      props.theme.style.upload.uploadDescriptionFontWeight};
    line-height: ${(props) =>
      props.theme.style.upload.uploadDescriptionLineHeight};
    text-transform: ${(props) =>
      props.theme.style.upload.textTransform || "inherit"};
    font-weight: ${(props) =>
      props.theme.style.upload.textTransform ? "bold" : "inherit"};
  }
  & > input {
    width: 0;
    height: 0;
  }
`;
export const SvgContainer = styled.div`
  border-radius: 55px;
  margin-left: ${(props) => (props.allVisible ? "0" : "40px")};
  margin-bottom: ${(props) => (props.allVisible ? "20px" : "0")};
  width: 80px;
  height: 80px;
  box-shadow: ${(props) => props.theme.style.upload.uploadSvgShadow};
  border: ${(props) => props.theme.style.upload.uploadSvgBorder};
  background-color: ${(props) => props.theme.style.upload.uploadSvgFill};
  stroke: ${(props) => props.theme.style.upload.uploadSvgColor};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.p`
  margin: 20px auto;
  font-family: ${(props) => props.theme.style.upload.fontFamily};
  color: ${(props) => props.theme.style.upload.fontColorMobile};
  font-weight: ${(props) => props.theme.style.upload.fontWeight};
  font-size: ${(props) => props.theme.style.upload.sizeMobile};
  text-align: left;
  line-height: ${(props) => props.theme.style.upload.lineHeightMobile};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
  text-align: center;
`;
export const ViewButton = styled.button`
  padding: 10px;
  border-radius: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonRadius
      : props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) =>
    props.theme.style.upload.uploadButtonBorder
      ? props.theme.style.upload.uploadButtonBorder
      : props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonBackground
      : props.theme.style.upload.seeAllButtonBackground};
  outline: none;
  margin: 0 auto;
  cursor: pointer;
  display: block;
  margin-top: 20px;
  color: ${(props) => props.theme.style.upload.seeAllButtonColor};
  transition: color 0.5s linear, background-color 0.5s linear;
  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverBgr
        ? props.theme.style.upload.seeAllButtonHoverBgr
        : ""};
    color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverColor
        ? props.theme.style.upload.seeAllButtonHoverColor
        : ""};
  }
`;
