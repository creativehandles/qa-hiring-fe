import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100%;
  background: ${(props) => props.theme.style.upload.background};
  max-width: 1120px;
  justify-content: space-between;
  align-items: center;
`;
export const OnlyCVContainer = styled(Container)`
  padding: 0px;
  border: ${(props) => props.theme.style.dragAndDrop.border};
  background: ${(props) => props.theme.style.dragAndDrop.background};
  border-radius: ${(props) => props.theme.style.dragAndDrop.borderRadius};
  justify-content: center;
`;

export const Title = styled.div`
  color: ${(props) => props.theme.style.upload.titleColor};
  font-family: ${(props) => props.theme.style.upload.titleFontFamily};
  font-weight: ${(props) => props.theme.style.upload.titleWeight};
  text-align: left;
  font-size: ${(props) => props.theme.style.upload.titleSize};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
  position: relative;

  &::after {
    content: "${(props) => props.theme.style.upload.afterClass}";
    color: #e00700;
    position: absolute;
    bottom: -8px;
    @media (max-width: 850px) {
      bottom: -5px;
    }
  }
`;

export const UploadContainer = styled.label`
  cursor: pointer;
  padding: 40px;
  width: ${(props) => (props.setWidth ? "315px" : "")};
  height: 315px;
  transition: 0.2s;
  align-items: center;
  display: flex;
  justify-content: space-between;
  flex-direction: column-reverse;
  box-shadow: ${(props) => props.theme.style.upload.uploadShadow};
  border-radius: ${(props) => props.theme.style.upload.uploadRadius};
  border: ${(props) =>
    props.allVisible ? "" : props.theme.style.upload.uploadBorder};
  background-color: ${(props) =>
    props.allVisible
      ? props.theme.style.global.background
      : props.theme.style.upload.uploadBackground};
  & > span {
    font-family: ${(props) =>
      props.theme.style.upload.uploadDescriptionFontFamily};
    color: ${(props) => props.theme.style.upload.uploadTitle};
    font-size: ${(props) => props.theme.style.upload.uploadDescriptionSize};
    font-weight: ${(props) =>
      props.theme.style.upload.uploadDescriptionFontWeight};
    line-height: ${(props) =>
      props.theme.style.upload.uploadDescriptionLineHeight};
  }
  & > input {
    width: 0;
    height: 0;
  }
`;

export const OnclyCVUploadContainer = styled(UploadContainer)`
  padding: 10px 20px;
  background: inherit;
  border: none;
  box-shadow: none;
  justify-content: space-between;
  & > span {
    font-family: ${(props) => props.theme.style.upload.titleFontFamily};
    color: ${(props) => props.theme.style.dragAndDrop.titleColor};
    font-size: ${(props) => props.theme.style.dragAndDrop.titleSize};
    font-weight: ${(props) => props.theme.style.dragAndDrop.titleWeight};
    line-height: ${(props) => props.theme.style.dragAndDrop.titleLineHeight};
  }
`;

export const SvgContainer = styled.div`
  border-radius: 72px;
  width: 121px;
  height: 121px;
  box-shadow: ${(props) => props.theme.style.upload.uploadSvgShadow};
  border: ${(props) => props.theme.style.upload.uploadSvgBorder};
  background-color: ${(props) => props.theme.style.upload.uploadSvgFill};
  stroke: ${(props) => props.theme.style.upload.uploadSvgColor};
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const OnlyCVSvgContainer = styled(SvgContainer)`
  width: 81px;
  height: 81px;
`;
export const Text = styled.p`
  margin: 20px auto;
  font-family: ${(props) => props.theme.style.upload.fontFamily};
  color: ${(props) => props.theme.style.upload.fontColor};
  font-weight: ${(props) => props.theme.style.upload.fontWeight};
  font-size: ${(props) => props.theme.style.upload.fontSize};
  text-align: left;
  line-height: ${(props) => props.theme.style.upload.lineHeight};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
`;
export const OnlyCVText = styled(Text)`
  font-family: ${(props) => props.theme.style.dragAndDrop.fontFamily};
  text-align: center;
  color: ${(props) => props.theme.style.dragAndDrop.fontColor};
  font-weight: ${(props) => props.theme.style.dragAndDrop.fontWeight};
  font-size: ${(props) => props.theme.style.dragAndDrop.fontSize};
  line-height: ${(props) => props.theme.style.dragAndDrop.lineHeight};
`;
export const ViewButton = styled.button`
  padding: 10px;
  border-radius: ${(props) => props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) =>
    props.theme.style.upload.seeAllOpportunitiesButtonBorder
      ? props.theme.style.upload.seeAllOpportunitiesButtonBorder
      : props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.theme.style.upload.seeAllButtonBackground};
  outline: none;
  cursor: pointer;
  display: block;
  font-family: ${(props) => props.theme.style.upload.seeAllButtonFontFamily};
  font-size: ${(props) => props.theme.style.upload.seeAllButtonSize};
  font-weight: ${(props) => props.theme.style.upload.seeAllButtonWeight};
  color: ${(props) => props.theme.style.upload.seeAllButtonColor};
  margin-right: 20px;

  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.hoverBackgroundColor};
    color: ${(props) => props.theme.style.upload.hoverTextCollor};
    transition: all 0.3s;
  }
`;

export const ViewButtonUpload = styled.label`
  padding: 10px;
  flex-basis: auto;
  border-radius: ${(props) => props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) =>
    props.theme.style.upload.uploadCVButtonBorder
      ? props.theme.style.upload.uploadCVButtonBorder
      : props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.theme.style.upload.highlightedButtonBackground};
  outline: none;
  cursor: pointer;
  display: flex;
  display: -webkit-inline;
  font-family: ${(props) => props.theme.style.upload.seeAllButtonFontFamily};
  font-size: ${(props) => props.theme.style.upload.seeAllButtonSize};
  font-weight: ${(props) => props.theme.style.upload.uploadButtonWeight};
  color: ${(props) => props.theme.style.upload.highlightedButtonColor};
  margin-right: 20px;
  transition: all 0.3s;
  & > input {
    width: 0;
    display: none;
    height: 0;
  }
  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.hoverBackgroundColor};
    color: ${(props) => props.theme.style.upload.hoverTextCollor};
  }
`;

export const DescriptionContianer = styled.div`
  width: 50%;
`;

export const ButtonContainer = styled.div`
  margin-top: 40px;
  display: flex;
`;
