import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  OnlyCVContainer,
  OnclyCVUploadContainer,
  OnlyCVText,
  Container,
  DescriptionContianer,
  Title,
  Text,
  UploadContainer,
  ViewButton,
  ViewButtonUpload,
  ButtonContainer,
  SvgContainer,
  OnlyCVSvgContainer,
} from "./style";
import { setCV, setUploadedThisSession } from "../../redux/slices/CV";
import useTranslation from "../../hooks/useTranslation";
import { Upload } from "../svg-component";

export const DragAndDropHome = ({
  onlyCV,
  setIsAccept = () => {},
  setNeedFetch,
}) => {
  const router = useRouter();
  const { seeAllOpportunitiesOption } = useSelector(
    (state) => state.styles.styles.upload
  );
  const [isEnter, setAllVisible] = useState(false);
  const [allVisible, setSeeAll] = useState(false);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const shadowColor = (props) => {
    const color = !allVisible
      ? props.theme.style.global.fontColor
      : props.theme.style.global.background;
    return color;
  };

  const uploadCV = (e, filed) => {
    setIsAccept(false);
    setAllVisible(false);
    e.stopPropagation();
    e.preventDefault();
    if (filed) dispatch(setCV(e.target.files[0]));
    else dispatch(setCV(e.dataTransfer.files[0]));
    if (setNeedFetch) {
      setNeedFetch(true);
    }
    dispatch(setUploadedThisSession(true));
  };
  const onDragHandler = (e, over) => {
    e.stopPropagation();
    e.preventDefault();
    setAllVisible(over);
  };

  if (onlyCV) {
    return (
      <OnlyCVContainer
        onDragEnter={(e) => onDragHandler(e, true)}
        onDragOver={(e) => onDragHandler(e, true)}
        onDragLeave={(e) => onDragHandler(e, false)}
        onDrop={(e) => uploadCV(e)}
      >
        <OnclyCVUploadContainer>
          <OnlyCVText
            dangerouslySetInnerHTML={{ __html: t("uploadOnlyCVText") }}
          ></OnlyCVText>
          <span>{t("uploadOnlyCVTitle")}</span>
          <OnlyCVSvgContainer allVisible={allVisible}>
            <Upload />
          </OnlyCVSvgContainer>
          <input type="file" onChange={(e) => uploadCV(e, true)} />
        </OnclyCVUploadContainer>
      </OnlyCVContainer>
    );
  }
  return (
    <Container>
      <DescriptionContianer>
        <Title>{t("uploadTitle")}</Title>
        <Text dangerouslySetInnerHTML={{ __html: t("uploadText") }} />
        <ButtonContainer>
          <ViewButtonUpload>
            {t("uploadButtonName")}
            <input type="file" onChange={(e) => uploadCV(e, true)} />
          </ViewButtonUpload>
          {seeAllOpportunitiesOption === "true" && (
            <ViewButton onClick={() => router.push("/opportunities")}>
              {t("uploadSeeAllOpportunitiesButtonName")}
            </ViewButton>
          )}
        </ButtonContainer>
      </DescriptionContianer>
      <UploadContainer
        setWidth
        shadowColor={shadowColor}
        isEnter={isEnter}
        allVisible={allVisible}
        onDragEnter={(e) => onDragHandler(e, true)}
        onDragOver={(e) => onDragHandler(e, true)}
        onDragLeave={(e) => onDragHandler(e, false)}
        onDrop={(e) => uploadCV(e)}
      >
        <span>{t("uploadButtonName")}</span>
        <SvgContainer allVisible={allVisible}>
          <Upload style={{ width: 44, height: 44 }} />
        </SvgContainer>
        <input type="file" onChange={(e) => uploadCV(e, true)} />
      </UploadContainer>
    </Container>
  );
};
