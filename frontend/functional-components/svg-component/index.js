import styled from "styled-components";

export const Check = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="33"
      height="24"
      viewBox="0 0 33 24"
    >
      <g>
        <g>
          <path
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="20"
            strokeWidth="3"
            d="M30.69 2.352v0L10.79 22.25v0l-9.044-9.045v0"
          />
        </g>
      </g>
    </svg>
  );
};

export const BoxDisagree = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="18"
      viewBox="0 0 19 19"
    >
      <g>
        <g>
          <path
            fill="none"
            strokeLinejoin="bevel"
            strokeMiterlimit="20"
            strokeWidth="2"
            d="M16.583 1H2.417C1.633 1 1 1.633 1 2.417v14.166C1 17.367 1.633 18 2.417 18h14.166c.784 0 1.417-.633 1.417-1.417V2.417C18 1.633 17.367 1 16.583 1z"
          />
        </g>
      </g>
    </svg>
  );
};

export const BoxAgree = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="18"
      viewBox="0 0 18 18"
    >
      <g>
        <g>
          <path d="M14.04 6.305L7.771 12.6h-.005c-.08.08-.295.258-.544.258-.178 0-.38-.099-.548-.267L4.05 9.966a.187.187 0 0 1 0-.268l.834-.834a.185.185 0 0 1 .132-.056c.046 0 .093.019.13.056l2.082 2.081 5.719-5.76a.185.185 0 0 1 .131-.057.17.17 0 0 1 .131.056l.82.849c.085.08.085.197.01.272zM16.5 0h-15C.67 0 0 .67 0 1.5v15c0 .83.67 1.5 1.5 1.5h15c.83 0 1.5-.67 1.5-1.5v-15c0-.83-.67-1.5-1.5-1.5z" />
        </g>
      </g>
    </svg>
  );
};

export const Heart = () => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="19"
      height="17"
      viewBox="0 0 19 17"
    >
      <g>
        <g>
          <path
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeLiterlimit="20"
            d="M17.189 2.416a4.473 4.473 0 0 0-6.327 0L10 3.278v0l-.862-.862A4.474 4.474 0 1 0 2.81 8.743l.862.862v0L10 15.932v0l6.327-6.327v0l.862-.862a4.473 4.473 0 0 0 0-6.327z"
          />
        </g>
      </g>
    </SVG>
  );
};

export const Back = ({ backColor }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="7.559"
      height="14"
      viewBox="0 0 7.559 14"
    >
      <path
        id="Icon_ionic-ios-arrow-back"
        data-name="Icon ionic-ios-arrow-back"
        d="M2.279,6.61l5-5A.945.945,0,0,0,5.944.276L.275,5.94a.943.943,0,0,0-.028,1.3l5.692,5.7a.945.945,0,0,0,1.338-1.334Z"
        transform="translate(0 0)"
        fill={backColor}
      />
    </svg>
  );
};

export const Grab = ({ className }) => {
  return (
    <SVG
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      width="17"
      height="18"
      viewBox="0 0 17 18"
    >
      <g>
        <g transform="rotate(-45 9 9)">
          <path d="M5.667 11.444h12.666v1.778H5.667zM23 7v1.778H1V7zm-12.778 8.889h3.556v1.778h-3.556z" />
        </g>
      </g>
    </SVG>
  );
};

export const Phone = () => {
  return (
    <SecondarySVG
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="13"
      viewBox="0 0 13 13"
    >
      <g>
        <g>
          <path d="M3.247 6.032a10.152 10.152 0 0 0 4.416 4.416l1.474-1.474a.666.666 0 0 1 .684-.161c.75.248 1.561.382 2.392.382.369 0 .67.302.67.67v2.339c0 .368-.301.67-.67.67-6.292 0-11.392-5.1-11.392-11.392 0-.369.301-.67.67-.67h2.345c.369 0 .67.301.67.67 0 .837.135 1.642.383 2.392a.673.673 0 0 1-.168.684z" />
        </g>
      </g>
    </SecondarySVG>
  );
};

export const Email = () => {
  return (
    <SecondarySVG
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="10"
      viewBox="0 0 14 10"
      stroke="white"
      strokeWidth="0px"
    >
      <g>
        <g>
          <path d="M9.198 4.9l4.467-3.832c.026.08.04.163.04.251v7.612c0 .08-.014.167-.04.264zM.608 9.71l4.506-4.32L6.86 6.802 8.603 5.39l4.506 4.322a.635.635 0 0 1-.224.04H.819a.599.599 0 0 1-.211-.04zM.595.54A.57.57 0 0 1 .819.5h12.066c.08 0 .158.013.238.04L8.63 4.384l-.595.476-1.176.964-1.176-.964-.595-.476zM0 8.93V1.32c0-.009.013-.093.04-.251L4.52 4.9.053 9.195A1.12 1.12 0 0 1 0 8.931z" />
        </g>
      </g>
    </SecondarySVG>
  );
};

export const Close = ({ className, ...props }) => {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      width="28"
      height="28"
      viewBox="0 0 34 34"
      {...props}
    >
      <g>
        <g>
          <g>
            <path
              strokeWidth="1.5"
              fill="none"
              strokeLinecap="round"
              strokeMiterlimit="20"
              d="M10 10l8 8"
            />
          </g>
          <g>
            <path
              strokeWidth="1.5"
              fill="none"
              strokeLinecap="round"
              strokeMiterlimit="20"
              d="M18 10l-8 8"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const CloseCV = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="10"
      viewBox="0 0 10 10"
      style={{ transform: "rotate(-585deg)" }}
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="1.2"
              d="M5-.575v11.151"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="1.2"
              d="M-.576 5h11.152"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const Upload = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
      {...props}
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="2"
              d="M30.358 20.692v6.455a3.227 3.227 0 0 1-3.228 3.228H4.54a3.227 3.227 0 0 1-3.228-3.228v-6.455 0"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="2"
              d="M23.903 9.397v0l-8.068-8.069v0L7.766 9.397v0"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="2"
              d="M15.835 1.328v19.364"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const MissingFilter = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="15"
      viewBox="0 0 14 15"
      {...props}
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M13 7.054a6 6 0 1 1-12 0 6 6 0 0 1 12 0z"
              {...props}
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M7 4.583v2.59"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M7 9.454v0"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const Warning = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="15"
      viewBox="0 0 14 15"
      {...props}
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M13 7.054a6 6 0 1 1-12 0 6 6 0 0 1 12 0z"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M7 4.583v2.59"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M7 9.454v0"
            />
          </g>
        </g>
      </g>
    </SVG>
  );
};

export const PaperClip = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="28"
      height="28"
      viewBox="0 0 29 35"
      {...props}
    >
      <g>
        <g transform="rotate(-45 14.5 17.5)">
          <path
            fill="none"
            strokeMiterlimit="20"
            d="M20.126 16.727v0l-5.594 5.594a3.654 3.654 0 0 1-5.167-5.168l5.593-5.593a2.436 2.436 0 1 1 3.445 3.445l-5.6 5.593a1.218 1.218 0 0 1-1.722-1.722l5.167-5.162v0"
          />
        </g>
      </g>
    </svg>
  );
};
export const BigPaperClip = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="22"
      height="22"
      viewBox="12 5 10 20"
      {...props}
    >
      <g>
        <g transform="rotate(-45 14.5 17.5)">
          <path
            fill="none"
            strokeMiterlimit="20"
            d="M20.126 16.727v0l-5.594 5.594a3.654 3.654 0 0 1-5.167-5.168l5.593-5.593a2.436 2.436 0 1 1 3.445 3.445l-5.6 5.593a1.218 1.218 0 0 1-1.722-1.722l5.167-5.162v0"
          />
        </g>
      </g>
    </SVG>
  );
};

export const PaperClipXXL = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="28"
      viewBox="10 8 10 20"
      {...props}
    >
      <g>
        <g transform="rotate(-45 14.5 17.5)">
          <path
            fill="none"
            strokeMiterlimit="20"
            d="M20.126 16.727v0l-5.594 5.594a3.654 3.654 0 0 1-5.167-5.168l5.593-5.593a2.436 2.436 0 1 1 3.445 3.445l-5.6 5.593a1.218 1.218 0 0 1-1.722-1.722l5.167-5.162v0"
          />
        </g>
      </g>
    </SVG>
  );
};

export const Ring = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="26"
      height="31"
      viewBox="0 0 26 31"
      {...props}
    >
      <g>
        <g>
          <path d="M13 30.563c-1.65 0-3-1.356-3-3.013h5.998c0 1.657-1.35 3.013-2.999 3.013zM13 .438c1.274 0 2.248.978 2.248 2.259V3.75c4.274.98 7.498 4.896 7.498 9.49v8.284l2.999 3.013v1.506H.254v-1.506l2.999-3.013v-8.284c0-4.594 3.223-8.51 7.497-9.49V2.697c0-1.28.975-2.26 2.25-2.26zm-.073 5.248c-.425 0-1.732.324-1.732.324-3.301.757-5.625 3.798-5.625 7.23v9.242l-.675.678-.564.567h17.336l-.564-.567-.675-.678v-9.241c0-3.434-2.323-6.474-5.625-7.231 0 0-1.218-.324-1.731-.324z" />
        </g>
      </g>
    </SVG>
  );
};

export const Drop = () => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="7"
      viewBox="0 0 12 7"
    >
      <g>
        <g>
          <path d="M0 .834l6 6 6-6z" />
        </g>
      </g>
    </SVG>
  );
};

export const DeleteValue = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="18"
      viewBox="0 -1 22 22"
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              stroke="#fff"
              strokeLinecap="round"
              strokeMiterlimit="20"
              d="M10.343 10.099l6.802 6.802"
            />
          </g>
          <g>
            <path
              fill="none"
              stroke="#fff"
              strokeLinecap="round"
              strokeMiterlimit="20"
              d="M17.145 10.099L10.343 16.9"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const Calendar = () => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      viewBox="38 40 25 25"
    >
      <defs>
        <clipPath id="qsura">
          <path
            fill="#fff"
            d="M38 62.722V39.278h23v23.444zm16.79-19.514v-1.917a.64.64 0 0 0-.638-.639h-1.278a.64.64 0 0 0-.64.64v1.916h-5.11v-1.917a.64.64 0 0 0-.64-.639h-1.277a.64.64 0 0 0-.64.64v1.916h-1.916a1.917 1.917 0 0 0-1.917 1.917v1.917h17.89v-1.917a1.917 1.917 0 0 0-1.917-1.917zm-11.5 13.258a.48.48 0 0 1 .48-.48h1.597a.48.48 0 0 1 .479.48v1.597a.48.48 0 0 1-.48.479H43.77a.48.48 0 0 1-.479-.48zm0-5.112a.48.48 0 0 1 .48-.479h1.597a.48.48 0 0 1 .479.48v1.596a.48.48 0 0 1-.48.48H43.77a.48.48 0 0 1-.479-.48zm5.111 5.112a.48.48 0 0 1 .48-.48h1.597a.48.48 0 0 1 .479.48v1.597a.48.48 0 0 1-.48.479h-1.596a.48.48 0 0 1-.48-.48zm0-5.112a.48.48 0 0 1 .48-.479h1.597a.48.48 0 0 1 .479.48v1.596a.48.48 0 0 1-.48.48h-1.596a.48.48 0 0 1-.48-.48zm5.112 5.112a.48.48 0 0 1 .479-.48h1.597a.48.48 0 0 1 .48.48v1.597a.48.48 0 0 1-.48.479h-1.597a.48.48 0 0 1-.48-.48zm0-5.112a.48.48 0 0 1 .479-.479h1.597a.48.48 0 0 1 .48.48v1.596a.48.48 0 0 1-.48.48h-1.597a.48.48 0 0 1-.48-.48zm-12.779 7.827c0 1.058.859 1.917 1.917 1.917h14.056a1.917 1.917 0 0 0 1.917-1.917V48.319h-17.89z"
          />
        </clipPath>
      </defs>
      <g>
        <g>
          <path
            fill="none"
            strokeMiterlimit="20"
            strokeWidth="4"
            d="M54.79 43.208v-1.917a.64.64 0 0 0-.638-.639h-1.278a.64.64 0 0 0-.64.64v1.916h-5.11v-1.917a.64.64 0 0 0-.64-.639h-1.277a.64.64 0 0 0-.64.64v1.916h-1.916a1.917 1.917 0 0 0-1.917 1.917v1.917h17.89v-1.917a1.917 1.917 0 0 0-1.917-1.917zm-11.5 13.258a.48.48 0 0 1 .48-.48h1.597a.48.48 0 0 1 .479.48v1.597a.48.48 0 0 1-.48.479H43.77a.48.48 0 0 1-.479-.48zm0-5.112a.48.48 0 0 1 .48-.479h1.597a.48.48 0 0 1 .479.48v1.596a.48.48 0 0 1-.48.48H43.77a.48.48 0 0 1-.479-.48zm5.111 5.112a.48.48 0 0 1 .48-.48h1.597a.48.48 0 0 1 .479.48v1.597a.48.48 0 0 1-.48.479h-1.596a.48.48 0 0 1-.48-.48zm0-5.112a.48.48 0 0 1 .48-.479h1.597a.48.48 0 0 1 .479.48v1.596a.48.48 0 0 1-.48.48h-1.596a.48.48 0 0 1-.48-.48zm5.112 5.112a.48.48 0 0 1 .479-.48h1.597a.48.48 0 0 1 .48.48v1.597a.48.48 0 0 1-.48.479h-1.597a.48.48 0 0 1-.48-.48zm0-5.112a.48.48 0 0 1 .479-.479h1.597a.48.48 0 0 1 .48.48v1.596a.48.48 0 0 1-.48.48h-1.597a.48.48 0 0 1-.48-.48zm-12.779 7.827c0 1.058.859 1.917 1.917 1.917h14.056a1.917 1.917 0 0 0 1.917-1.917V48.319h-17.89z"
            clipPath='url("#qsura")'
          />
        </g>
      </g>
    </SVG>
  );
};

export const Power = () => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="22"
      height="22"
      viewBox="0 0 22 22"
    >
      <g>
        <g>
          <path d="M21.858 10.937c0 5.833-4.729 10.562-10.562 10.562-5.832 0-10.562-4.73-10.562-10.562 0-3.216 1.444-6.079 3.72-8.015L6.11 4.576a8.209 8.209 0 0 0 5.187 14.576 8.209 8.209 0 0 0 8.215-8.215 8.121 8.121 0 0 0-3.04-6.349l1.667-1.666c2.277 1.936 3.72 4.8 3.72 8.015zm-9.388 1.174h-2.347V.375h2.347z" />
        </g>
      </g>
    </SVG>
  );
};

export const ArrowDropdown = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="7"
      viewBox="0 0 12 7"
      {...props}
    >
      <g>
        <g>
          <path d="M-.006.834l6 6 6-6z" />
        </g>
      </g>
    </SVG>
  );
};

export const DeleteCV = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="12"
      viewBox="0 0 10 12"
    >
      <g>
        <g>
          <path
            fill="none"
            stroke="#a63f3f"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="20"
            d="M3.04 2.203a.98.98 0 0 1 .98-.98h1.96a.98.98 0 0 1 .98.98v.98H3.04zm-1.469.98v6.857c0 .54.439.98.98.98h4.898a.98.98 0 0 0 .98-.98V3.182z"
          />
        </g>
      </g>
    </svg>
  );
};

export const Download = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="13"
      viewBox="0 0 13 13"
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              stroke="#3fa696"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M11.952 7.957v2.245c0 .62-.503 1.122-1.123 1.122H2.972c-.62 0-1.122-.502-1.122-1.122V7.957v0"
            />
          </g>
          <g>
            <path
              fill="none"
              stroke="#3fa696"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M4.095 5.151v0L6.9 7.957v0l2.806-2.806v0"
            />
          </g>
          <g>
            <path
              fill="none"
              stroke="#3fa696"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              d="M6.9 7.957V1.223"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const Next = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="8"
      height="15"
      viewBox="0 0 8 15"
      style={{ transform: "rotate(180deg)" }}
    >
      <g>
        <g>
          <path
            fill="#fff"
            d="M2.748 7.499l5.003-5a.94.94 0 0 0 0-1.334.95.95 0 0 0-1.339 0L.744 6.829a.943.943 0 0 0-.028 1.303l5.692 5.704c.185.185.43.276.67.276a.94.94 0 0 0 .67-1.61z"
          />
        </g>
      </g>
    </svg>
  );
};

export const Prev = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="8"
      height="15"
      viewBox="0 0 8 15"
    >
      <g>
        <g>
          <path
            fill="#fff"
            d="M2.748 7.499l5.003-5a.94.94 0 0 0 0-1.334.95.95 0 0 0-1.339 0L.744 6.829a.943.943 0 0 0-.028 1.303l5.692 5.704c.185.185.43.276.67.276a.94.94 0 0 0 .67-1.61z"
          />
        </g>
      </g>
    </svg>
  );
};

export const ArrowBack = (props) => {
  return (
    <SVG
      id="SVGDoc"
      width="8"
      height="14"
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      viewBox="0 0 8 14"
      {...props}
    >
      <defs></defs>
      <g>
        <g>
          <path
            d="M2.49954,6.74854v0l5.00306,-4.99913c0.37001,-0.37001 0.37001,-0.96834 0,-1.33441c-0.37001,-0.37001 -0.96833,-0.36608 -1.33835,0l-5.6683,5.66437c-0.35821,0.3582 -0.36608,0.93291 -0.02756,1.30292l5.69192,5.70373c0.18501,0.18501 0.42906,0.27554 0.66917,0.27554c0.24012,0 0.48417,-0.09053 0.66918,-0.27554c0.37001,-0.37001 0.37001,-0.96833 0,-1.33441z"
            fillOpacity="1"
          ></path>
        </g>
      </g>
    </SVG>
  );
};

export const DatePickerCallendar = () => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="14"
      viewBox="0 0 12 14"
    >
      <g>
        <g>
          <path d="M10.486 2.105H9.262V.882a.41.41 0 0 0-.407-.408h-.816a.41.41 0 0 0-.408.408v1.223H4.368V.882A.41.41 0 0 0 3.96.474h-.816a.41.41 0 0 0-.408.408v1.223H1.513c-.676 0-1.224.548-1.224 1.224v1.223h11.42V3.33c0-.676-.547-1.224-1.223-1.224zM1.921 10.57c0-.169.137-.306.305-.306h1.02c.168 0 .306.137.306.306v1.02a.307.307 0 0 1-.306.305h-1.02a.307.307 0 0 1-.305-.306zm0-3.263c0-.169.137-.306.305-.306h1.02c.168 0 .306.137.306.306v1.02a.307.307 0 0 1-.306.305h-1.02a.307.307 0 0 1-.305-.306zm3.263 3.263c0-.169.137-.306.306-.306h1.02c.167 0 .305.137.305.306v1.02a.307.307 0 0 1-.306.305H5.49a.307.307 0 0 1-.305-.306zm0-3.263c0-.169.137-.306.306-.306h1.02c.167 0 .305.137.305.306v1.02a.307.307 0 0 1-.306.305H5.49a.307.307 0 0 1-.305-.306zm3.263 3.263c0-.169.137-.306.306-.306h1.02c.167 0 .305.137.305.306v1.02a.307.307 0 0 1-.306.305h-1.02a.307.307 0 0 1-.305-.306zm0-3.263c0-.169.137-.306.306-.306h1.02c.167 0 .305.137.305.306v1.02a.307.307 0 0 1-.306.305h-1.02a.307.307 0 0 1-.305-.306zM.289 12.302c0 .676.548 1.224 1.224 1.224h8.973c.676 0 1.224-.548 1.224-1.224V5.368H.289z" />
        </g>
      </g>
    </SVG>
  );
};

export const Plus = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="13"
      viewBox="0 0 13 13"
      {...props}
    >
      <g>
        <g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="1.5"
              d="M6.241 1.35v10.483"
            />
          </g>
          <g>
            <path
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="1.5"
              d="M1 6.592h10.483"
            />
          </g>
        </g>
      </g>
    </SVG>
  );
};

export const ApplicationSent = (props) => {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="14"
      viewBox="0 0 18 14"
      {...props}
    >
      <g>
        <g>
          <path
            fill="none"
            stroke="#fff"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="20"
            strokeWidth="2"
            d="M16.27 1.898v0l-10.4 10.4v0L1.144 7.571v0"
          />
        </g>
      </g>
    </SVG>
  );
};

export const Dots = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="2"
      height="12"
      viewBox="0 0 2 12"
    >
      <defs>
        <clipPath id="aqp5a">
          <path fill="#fff" d="M1.9 6.121a.9.9 0 1 1-1.8 0 .9.9 0 0 1 1.8 0z" />
        </clipPath>
        <clipPath id="aqp5b">
          <path fill="#fff" d="M1.9 1.818a.9.9 0 1 1-1.8 0 .9.9 0 0 1 1.8 0z" />
        </clipPath>
        <clipPath id="aqp5c">
          <path
            fill="#fff"
            d="M1.9 10.424a.9.9 0 1 1-1.8 0 .9.9 0 0 1 1.8 0z"
          />
        </clipPath>
      </defs>
      <g>
        <g>
          <g>
            <path
              fill="none"
              stroke="#3f5aa6"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="4"
              d="M1.9 6.121a.9.9 0 1 1-1.8 0 .9.9 0 0 1 1.8 0z"
              clipPath='url("#aqp5a")'
            />
          </g>
          <g>
            <path
              fill="none"
              stroke="#3f5aa6"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="4"
              d="M1.9 1.818a.9.9 0 1 1-1.8 0 .9.9 0 0 1 1.8 0z"
              clipPath='url("#aqp5b")'
            />
          </g>
          <g>
            <path
              fill="none"
              stroke="#3f5aa6"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="20"
              strokeWidth="4"
              d="M1.9 10.424a.9.9 0 1 1-1.8 0 .9.9 0 0 1 1.8 0z"
              clipPath='url("#aqp5c")'
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export const Search = ({ color }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="15"
      viewBox="0 0 16 15"
    >
      <g>
        <g>
          <path
            fill={color || "#3f5aa6"}
            d="M6.578 10.612a4.64 4.64 0 0 1-3.302-1.367A4.637 4.637 0 0 1 1.91 5.947c0-1.247.485-2.42 1.367-3.298a4.64 4.64 0 0 1 3.302-1.367A4.64 4.64 0 0 1 9.88 2.649a4.637 4.637 0 0 1 1.367 3.298c0 1.247-.485 2.42-1.367 3.298a4.64 4.64 0 0 1-3.302 1.367zm8.795 3.228l-4.149-4.187A5.944 5.944 0 0 0 6.578 0 5.947 5.947 0 0 0 .631 5.947a5.944 5.944 0 0 0 5.947 5.943 5.926 5.926 0 0 0 3.749-1.328l4.121 4.16a.636.636 0 0 0 .901.024.643.643 0 0 0 .024-.906z"
          />
        </g>
      </g>
    </svg>
  );
};

const SVG = styled.svg`
  fill: ${(props) =>
    props.theme.style.global.svgColor
      ? props.theme.style.global.svgColor
      : props.theme.style.global.fontColor};
  stroke: ${(props) =>
    props.theme.style.global.svgColor
      ? props.theme.style.global.svgColor
      : props.theme.style.global.fontColor};
`;
const SecondarySVG = styled.svg`
  fill: ${(props) =>
    props.theme.style.global.svgColor
      ? props.theme.style.global.svgColor
      : props.theme.style.global.fontColor};
  stroke: ${(props) =>
    props.theme.style.global.svgColor
      ? props.theme.style.global.svgColor
      : props.theme.style.global.fontColor};
`;
