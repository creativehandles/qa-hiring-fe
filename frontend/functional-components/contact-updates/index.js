import React, { useState } from "react";
import { Container, ContactSection, NotifyButton, Title } from "./style";

import { GdrAgreement } from "../../components/common/gdr-agreement";
import { Input } from "../../components/common/input";

export const ContactUpdates = () => {
  const [isChecked, setIsChecked] = useState(false);
  return (
    <Container>
      <ContactSection>
        <Title>SELECT A START DATE</Title>
        <Input />
      </ContactSection>
      <ContactSection>
        <Title>YOUR EMAIL</Title>
        <Input />
      </ContactSection>
      <GdrAgreement
        isChecked={isChecked}
        setIsChecked={setIsChecked}
        center
        label="Send me updates about LOGO"
      />
      <NotifyButton title="NOTIFY ME VIA EMAIL" />
    </Container>
  );
};
