import styled from "styled-components";
import { Button } from "../../components/common/button";

export const Container = styled.div`
  display: grid;
  grid-gap: 25px;
`;

export const Title = styled.div`
  font-size: 12px;
  font-weight: 800;
  text-align: left;
  margin-bottom: 10px;
  color: ${(props) => props.theme.style.global.fontColor};
`;

export const NotifyButton = styled(Button)`
  padding: 14px 65px;
`;

export const ContactSection = styled.div``;
