import styled from "styled-components";

export const Container = styled.div`
  padding: 30px;
  width: 100%;
  margin: 0 auto;
  background: ${(props) => props.theme.style.upload.background};
`;
export const ContainerCV = styled.div`
  display: flex;
  fill: ${(props) => props.theme.style.cvField.color};
  stroke: ${(props) => props.theme.style.cvField.color};
  box-shadow: ${(props) => props.theme.style.cvField.boxShadow};
  border-radius: ${(props) => props.theme.style.cvField.borderRadius};
  border: ${(props) => props.theme.style.cvField.border};
  background-color: ${(props) => props.theme.style.cvField.background};
  justify-content: space-between;
  margin-bottom: 10px;
  font-family: ${(props) => props.theme.style.cvField.fontFamily};
  color: ${(props) => props.theme.style.cvField.color};
`;

export const Text = styled.div`
  width: 82%;
  align-self: center;
`;
export const Title = styled.div`
  text-transform: uppercase;
  color: ${(props) => props.theme.style.upload.headerColorMobile};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
  font-family: ${(props) => props.theme.style.upload.titleFontFamily};
  text-align: center;
  margin-bottom: 20px;
  font-size: ${(props) => props.theme.style.upload.titleSizeMobile};
  font-weight: ${(props) => props.theme.style.upload.headerWeightMobile};
`;

export const DescriprionText = styled(Text)`
  text-align: center;
  margin-bottom: 22px;
  width: 100%;
  color: ${(props) => props.theme.style.upload.fontColorMobile};
`;

export const DeleteFile = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px 12px;
  border-left: 1px solid ${(props) => props.theme.style.global.background};
`;
