import React, { useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Container,
  ContainerCV,
  Text,
  DescriprionText,
  Title,
  DeleteFile,
} from "./style";
import { removeCV } from "../../redux/slices/CV";
import { ListOfOpportunities } from "../opportunities/list";
import { Close, PaperClip } from "../svg-component";
import { NoOpenVacancies } from "../../functional-components/no-open-vacancies";
import useTranslation from "../../hooks/useTranslation";

export const CVfiled = ({ file }) => {
  const listItemRef = useRef();
  const { t } = useTranslation();

  useEffect(() => {
    if (listItemRef.current) {
      listItemRef.current.scrollIntoView({
        behavior: "smooth",
      });
    }
  }, [listItemRef]);

  const dispatch = useDispatch();
  const openVacancies = useSelector((state) => state.openVacancies.value);

  if (!openVacancies.length) {
    return <NoOpenVacancies hasDefaultVacancies />;
  }

  return (
    <Container>
      <Title>{t("suitablePositions")}</Title>
      <DescriprionText>{t("resultsNowOptmized")}</DescriprionText>
      <ContainerCV>
        <PaperClip style={{ marginLeft: 10 }} />
        <Text>{file.name}</Text>
        <DeleteFile>
          <Close
            width="20"
            height="20"
            viewBox="4 4 20 20"
            onClick={() => {
              dispatch(removeCV());
            }}
          />
        </DeleteFile>
      </ContainerCV>
      <div ref={listItemRef}>
        <ListOfOpportunities data={openVacancies} withoutLoadButton={false} />
      </div>
    </Container>
  );
};
