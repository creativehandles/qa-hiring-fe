import React, { useRef } from "react";
import styled from "styled-components";
import forEach from "lodash/forEach";
import LazyLoad from "react-lazy-load";
import { UnderImageText, UnderImageTextBold } from "../text-section/style";

export const Gallery = ({
  images,
  childrens,
  screenWidth,
  iframe,
  autoHeight,
  height,
}) => {
  const target = useRef();
  const columns = images?.columns;
  const rows = images?.rows;
  const isImageDescription = images?.isImageDescription;

  // Gets the grid layout in css for container
  const gridTemplate = () => {
    let templateColumns = "";

    if (columns && columns.length !== 0) {
      templateColumns = columns.map((col) => `${col.width} `);
    } else if (rows && rows.length !== 0) {
      throw new Error("Not implemented yet");
    } else {
      throw new Error("Rows or Columns must be specified for images object");
    }

    return {
      columns: templateColumns,
    };
  };

  // gets the total no of rows for the images array
  const getTotalRows = () => {
    let totalrows = -1;

    if (columns && columns.length !== 0) {
      // had to use lodash for nested objects
      forEach(columns, (col) => {
        const colImagesLength = col.columnImages.length;
        if (colImagesLength > totalrows) {
          totalrows = colImagesLength;
        }
      });
    }
    return totalrows;
  };

  const renderImages = () => {
    const totalrows = getTotalRows();
    const imagesRendered = [];

    if (columns && columns.length !== 0) {
      // had to use lodash for nested objects
      forEach(columns, (col, colIndex) => {
        const totalrowsInCol = col.columnImages.length;
        forEach(col.columnImages, (colImage, colImageIndex) => {
          const columnNo = colIndex + 1;
          let rowNo = -1;
          if (totalrowsInCol === totalrows) {
            rowNo = colImageIndex + 1;
          } else {
            // css spec says if first out of 2 rows, then specify as 1 / 3
            rowNo = `${colImageIndex + 1} / ${totalrows + 1}`;
          }
          imagesRendered.push(
            <ImageContainer key={colImage} template={{ columnNo, rowNo }}>
              <LazyLoad debounce={false}>
                <Image src={colImage} draggable={false} />
              </LazyLoad>
              {isImageDescription ? (
                <>
                  <UnderImageTextBold>{col.textBold}</UnderImageTextBold>
                  <UnderImageText>{col.text}</UnderImageText>
                </>
              ) : (
                ""
              )}
            </ImageContainer>
          );
        });
      });
    } else if (rows && rows.length !== 0) {
      throw new Error("Not implemented yet");
    }

    return imagesRendered.length !== 0 ? imagesRendered : null;
  };

  if (iframe) {
    return (
      <IframeContaner
        height={height}
        screenWidth={screenWidth}
        childrens={childrens}
        autoHeight={autoHeight}
        ref={target}
        dangerouslySetInnerHTML={{ __html: iframe }}
      />
    );
  }

  return (
    <GalleryContainer
      height={height}
      screenWidth={screenWidth}
      childrens={childrens}
      ref={target}
      template={gridTemplate()}
    >
      {renderImages()}
    </GalleryContainer>
  );
};

const GalleryContainer = styled.div`
  width: auto;
  margin: 0 auto;
  display: grid;
  grid-gap: ${(props) =>
    props.theme.style.global.galleryContainerGap
      ? props.theme.style.global.galleryContainerGap
      : "4% 6%"};
  grid-template-columns: ${(props) => props.template.columns};
  place-items: start;
  @media (max-width: 850px) {
    grid-gap: 4% 6%;
  }
  * {
    width: 100%;
  }
  img::before {
    content: "";
    display: inline-block;
    width: 1px;
    height: 0;
    padding-bottom: 100%;
  }
`;

const ImageContainer = styled.div`
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  grid-column: ${(props) => props.template.columnNo};
  grid-row: ${(props) => props.template.rowNo};
`;

const Image = styled.img`
  object-fit: cover;
`;

const IframeContaner = styled.div`
  width: 100%;
  height: ${(props) => (props.autoHeight ? "auto" : props.height)};
  & > iframe {
    background: black;
    width: 100%;
    height: 100%;
  }
`;
