import React, { useState, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { open } from "../../redux/slices/modals";
import { GdrAgreement } from "../../components/common/gdr-agreement";
import ConfigStyles from "../../utils/config-styles";
import { getAPIURL } from "../../utils/api-url/index";
import {
  Container,
  Title,
  AddButton,
  AreaContainer,
  TextArea,
  FormContainer,
  InputFiled,
  InputLabel,
  SendButton,
  ContainerCV,
  DeleteCV,
  DesktopContainer,
  DesktopAddButton,
  FeildsContainer,
  FieldItem,
  AgreementDesktopContainer,
  DesktopSendButton,
  Miniloader,
  Notice,
  GdrAgreementContainerMobile,
} from "./style";
import {
  ApplicationSent,
  BigPaperClip,
  CloseCV,
  MissingFilter,
  Plus,
} from "../svg-component";
import { getGDPR, setGDPR as setGDPRUtil } from "../../utils/gdpr";
import axios from "axios";
import { removeCV, removeReserve, setReserve } from "../../redux/slices/CV";
import { submitReserveCV } from "../../redux/slices/vacancies/open-vacancies/functions/reserve-cv";
import Spinner from "../../components/common/spinner";
import { setInputs, getInputs } from "../../utils/vacancy-form-inputs";
import useTranslation from "../../hooks/useTranslation";
import { pushToDataLayer } from "../../utils/gtm";
import { logInfoToConsole } from "../../utils/debug";

export const VacancyForm = ({ mobile, target, mhub_id }) => {
  const [cvNotPresent, setCVNotPresent] = useState(false);
  const addButton = useRef();
  const localStyles = ConfigStyles();
  const vacancy = useSelector((state) => state.vacancy.value);
  const candidate = useSelector((state) => state.candidate?.value);
  const [otherDocuments, setOtherDocuments] = useState([]);
  const [userData, setUserData] = useState(getInputs());
  const [isSubmited, setIsSubmited] = useState(false);
  const [submissionError, setSubmissionError] = useState(false);
  const [submissionInProgress, setSubmissionInProgress] = useState(false);
  const cv = useSelector((state) => state.CV.value);
  const reserveCV = useSelector((state) => state.CV.reserveCV);
  const reserveCVUploading = useSelector(
    (state) => state.openVacancies.loadingReserveCV
  );
  const agreement = getGDPR();
  const [isChecked, setIsCheked] = useState(agreement);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  useEffect(() => {
    setIsSubmited(false);
    if (candidate) {
      setUserData(getInputs());
    }
  }, [candidate, vacancy]);

  const checkboxLabel = (() => {
    return (
      <div>
        {t("resultsText")}
        <span
          style={{ textDecoration: "underline", fontWeight: 600 }}
          onClick={(e) => {
            e.stopPropagation();
            dispatch(open("gdpr"));
          }}
        >
          {t("resultsGdprText")}
        </span>
      </div>
    );
  })();

  const uploadCV = (e) => {
    dispatch(setReserve(e.target.files[0]));
    dispatch(submitReserveCV({ file: e.target.files[0], apiURL: getAPIURL() }));
    e.target.value = null;
  };

  const attachOtherDocument = (e) => {
    setOtherDocuments([...otherDocuments, ...e.target.files]);
    e.target.value = null;
  };
  const changeUserData = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
  };

  const filterOtherDocuments = (el) => {
    setOtherDocuments(otherDocuments.filter((file) => file !== el));
  };

  const returnOtherDocuments = (desktop) => {
    return (
      <>
        {otherDocuments?.map((el, index) => {
          return (
            <ContainerCV key={"key" + index}>
              <BigPaperClip style={{ marginBottom: 3 }} />
              {el.name}
              <DeleteCV onClick={() => filterOtherDocuments(el)}>
                <CloseCV />
              </DeleteCV>
            </ContainerCV>
          );
        })}
        {otherDocuments?.length < 2 ? (
          desktop ? (
            <DesktopAddButton>
              <Plus style={{ marginRight: "10px" }} />
              {t("addAnotherDocument")}
              <input type="file" multiple onChange={attachOtherDocument} />
            </DesktopAddButton>
          ) : (
            <AddButton>
              <Plus style={{ marginRight: "10px" }} />
              {t("addAnotherDocument")}
              <input type="file" multiple onChange={attachOtherDocument} />
            </AddButton>
          )
        ) : null}
      </>
    );
  };

  const returnCV = (isDesktop) => {
    if (cv || reserveCV)
      return (
        <>
          <ContainerCV>
            <BigPaperClip style={{ marginBottom: 3 }} />
            {cv ? cv.name : reserveCV.name}
            {!(submissionInProgress || reserveCVUploading) && (
              <DeleteCV
                onClick={() => {
                  setUserData({
                    first_name: null,
                    last_name: null,
                    email: null,
                    phone: null,
                  });
                  dispatch(removeCV());
                  dispatch(removeReserve());
                }}
              >
                <CloseCV />
              </DeleteCV>
            )}
            {reserveCVUploading && (
              <Miniloader>
                <Spinner />
              </Miniloader>
            )}
          </ContainerCV>
        </>
      );
    else
      return (
        <>
          {isDesktop ? (
            <DesktopAddButton warning={cvNotPresent} ref={addButton}>
              <Plus style={{ marginRight: "10px" }} />
              {t("addCV")}
              <input type="file" onChange={uploadCV} />
            </DesktopAddButton>
          ) : (
            <AddButton warning={cvNotPresent} ref={addButton}>
              <Plus style={{ marginRight: "10px" }} />
              {t("addCV")}
              <input type="file" onChange={uploadCV} />
            </AddButton>
          )}
          <Notice open={cvNotPresent}>
            {" "}
            <MissingFilter style={{ marginRight: "10px" }} /> {t("cVRequired")}
          </Notice>
        </>
      );
  };

  const applyToVacancy = async () => {
    const formData = new FormData();
    formData.append("first_name", userData.first_name);
    formData.append("last_name", userData.last_name);
    formData.append("email", userData.email);
    formData.append("phone", userData.phone);
    userData.cover_letter &&
      formData.append("cover_letter", userData.cover_letter);
    formData.append("gdpr_agreed", isChecked ? "1" : "0");
    formData.append("mhub_vacancy_id", mhub_id);
    otherDocuments.length &&
      otherDocuments.forEach((el) => formData.append("documents[]", el));
    try {
      setSubmissionError(false);
      setSubmissionInProgress(true);
      const response = await axios.post(
        `${getAPIURL()}candidates/${candidate.id}/apply-to-vacancy`,
        formData,
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            "Accept-Language": process.env.NEXT_PUBLIC_LANGUAGE,
          },
        }
      );
      setIsSubmited(true);
      pushToDataLayer({
        event: "cv",
        step: "application success",
      });
      logInfoToConsole("dataLayer.push for submitted application");

      return response;
    } catch (error) {
      setSubmissionError(true);
      console.log(error);
    } finally {
      setSubmissionInProgress(false);
      setInputs(userData);
    }
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    if (!cv && !reserveCV) {
      setCVNotPresent(true);
      addButton.current.scrollIntoView({
        behavior: "smooth",
      });
      const smooth = setTimeout(() => {
        setCVNotPresent(false);
      }, 3000);
      return () => clearTimeout(smooth);
    } else {
      setGDPRUtil(isChecked);
      applyToVacancy();
    }
  };

  if (!mobile) {
    return (
      <DesktopContainer
        onChange={() => setIsSubmited(false)}
        enctype="multipart/form-data"
        ref={target}
        onSubmit={onFormSubmit}
      >
        <Title>{t("applyForJobText")}</Title>
        {returnCV(true)}
        {returnOtherDocuments(true)}
        <AreaContainer>
          <TextArea
            disabled={reserveCVUploading || submissionInProgress}
            name="cover_letter"
            onChange={changeUserData}
            placeholder={t("writeCoverLetter")}
          />
        </AreaContainer>
        <FormContainer>
          <FeildsContainer>
            <FieldItem>
              {" "}
              <InputLabel>{t("name")}</InputLabel>
              <InputFiled
                disabled={reserveCVUploading || submissionInProgress}
                value={userData.first_name || ""}
                required
                placeholder={t("name")}
                name="first_name"
                onChange={changeUserData}
              />
            </FieldItem>
            <FieldItem>
              <InputLabel>{t("surname")}</InputLabel>
              <InputFiled
                disabled={reserveCVUploading || submissionInProgress}
                required
                value={userData.last_name || ""}
                placeholder={t("surname")}
                name="last_name"
                onChange={changeUserData}
              />
            </FieldItem>
            <FieldItem>
              <InputLabel>{t("email")}</InputLabel>
              <InputFiled
                disabled={reserveCVUploading || submissionInProgress}
                required
                value={userData.email || ""}
                placeholder={t("email")}
                name="email"
                onChange={changeUserData}
              />
            </FieldItem>
            <FieldItem>
              <InputLabel>{t("phone")}</InputLabel>
              <InputFiled
                disabled={reserveCVUploading || submissionInProgress}
                required
                value={userData.phone || ""}
                placeholder={t("phone")}
                name="phone"
                onChange={changeUserData}
              />
            </FieldItem>
          </FeildsContainer>
          <AgreementDesktopContainer>
            {localStyles.agreement.home === "false" ? (
              <GdrAgreement
                isChecked={isChecked}
                handleCheckboxChange={setIsCheked}
                label={checkboxLabel}
                isOpportunitiesPage
              />
            ) : (
              localStyles.agreement.vacancy === "true" && (
                <GdrAgreement
                  isChecked={isChecked}
                  handleCheckboxChange={setIsCheked}
                  label={checkboxLabel}
                  isOpportunitiesPage
                />
              )
            )}
            <DesktopSendButton
              disabled={reserveCVUploading || submissionInProgress}
              type="submit"
              title={isSubmited ? t("applicationSent") : t("sendApplication")}
            >
              {isSubmited && <ApplicationSent style={{ marginLeft: "10px" }} />}
            </DesktopSendButton>
            <Notice open={submissionError}>
              {" "}
              <MissingFilter style={{ marginRight: "10px" }} />
              {t("errorSubmitApplication")}
            </Notice>
          </AgreementDesktopContainer>
        </FormContainer>
      </DesktopContainer>
    );
  }
  return (
    <Container
      onChange={() => setIsSubmited(false)}
      enctype="multipart/form-data"
      ref={target}
      onSubmit={onFormSubmit}
    >
      <Title>{t("applyForJobText")}</Title>
      {returnCV()}
      {returnOtherDocuments()}
      <AreaContainer>
        <TextArea
          disabled={reserveCVUploading || submissionInProgress}
          name="cover_letter"
          onChange={changeUserData}
          placeholder={t("writeCoverLetter")}
        />
      </AreaContainer>
      <FormContainer>
        <InputLabel>{t("name")}</InputLabel>
        <InputFiled
          disabled={reserveCVUploading || submissionInProgress}
          value={userData.first_name || ""}
          required
          placeholder={t("name")}
          name="first_name"
          onChange={changeUserData}
        />
        <InputFiled
          disabled={reserveCVUploading || submissionInProgress}
          required
          value={userData.last_name || ""}
          placeholder={t("surname")}
          name="last_name"
          onChange={changeUserData}
        />
        <InputLabel>{t("email")}</InputLabel>
        <InputFiled
          disabled={reserveCVUploading || submissionInProgress}
          required
          value={userData.email || ""}
          placeholder={t("email")}
          name="email"
          onChange={changeUserData}
        />
        <InputLabel>{t("phone")}</InputLabel>
        <InputFiled
          disabled={reserveCVUploading || submissionInProgress}
          required
          value={userData.phone || ""}
          placeholder={t("phone")}
          name="phone"
          onChange={changeUserData}
        />
        <GdrAgreementContainerMobile>
          {localStyles.agreement.home === "false" ? (
            <GdrAgreement
              isChecked={isChecked}
              handleCheckboxChange={setIsCheked}
              label={checkboxLabel}
              mobile
            />
          ) : (
            localStyles.agreement.vacancy === "true" && (
              <GdrAgreement
                isChecked={isChecked}
                handleCheckboxChange={setIsCheked}
                label={checkboxLabel}
                mobile
              />
            )
          )}
        </GdrAgreementContainerMobile>
        <SendButton
          disabled={reserveCVUploading || submissionInProgress}
          type="submit"
          title={isSubmited ? t("applicationSent") : t("sendApplication")}
        >
          {isSubmited && <ApplicationSent style={{ marginLeft: "10px" }} />}
        </SendButton>
      </FormContainer>
    </Container>
  );
};
