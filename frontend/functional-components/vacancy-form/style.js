import styled from "styled-components";
import { Button } from "../../components/common/button";
import { Input } from "../../components/common/input";

export const Container = styled.form`
  display: flex;
  max-width: 316px;
  margin: 0 auto;
  flex-direction: column;
  justify-content: center;
  position: relative;
`;

export const Title = styled.div`
  font-family: ${(props) => props.theme.style.apply.titleFontFamily};
  color: ${(props) => props.theme.style.apply.titleColor};
  font-size: ${(props) => props.theme.style.apply.titleSize};
  font-weight: ${(props) => props.theme.style.apply.titleWeight};
  line-height: ${(props) => props.theme.style.apply.titleLineHeight};
  text-align: center;
  margin-bottom: 15px;
  letter-spacing: normal;
  text-transform: uppercase;

  @media (max-width: 850px) {
    color: ${(props) =>
      props.theme.style.apply.titleColorMobile
        ? props.theme.style.apply.titleColorMobile
        : props.theme.style.apply.titleColor};
  }
`;

export const AddButton = styled.label`
  display: flex;
  flex-direction: row;
  width: 316px;
  font-family: ${(props) => props.theme.style.apply.addButtonFontFamily};
  box-shadow: ${(props) => (props.warning ? "0px 0px 4px 0px #a63f3f " : "")};
  transition: 0.4s;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  border-radius: ${(props) => props.theme.style.apply.addButtonRadius};
  padding: 15px 30px;
  border: ${(props) => props.theme.style.apply.addButtonBorder};
  background-color: ${(props) => props.theme.style.apply.addButtonBackground};
  outline: none;
  margin: 0px auto;
  margin-top: 5px;
  margin-bottom: 5px;
  color: ${(props) => props.theme.style.apply.addButtonColor};
  font-size: ${(props) => props.theme.style.apply.addButtonSize};
  font-weight: ${(props) => props.theme.style.apply.addButtonWeight};
  & > input {
    visibility: hidden;
    height: 0;
    width: 0;
  }
`;

export const TextArea = styled.textarea`
  z-index: 1;
  width: 100%;
  min-height: 197px;
  outline: none;
  padding: 20px;
  font-family: ${(props) => props.theme.style.apply.textAreaFontFamily};
  box-shadow: ${(props) => props.theme.style.apply.textAreaShadow};
  border-radius: ${(props) => props.theme.style.apply.textAreaRadius};
  border: ${(props) => props.theme.style.apply.textAreaBorder};
  background-color: ${(props) => props.theme.style.apply.textAreaBackground};
  color: ${(props) => props.theme.style.apply.textAreaColor};
  font-weight: ${(props) => props.theme.style.apply.textAreaWeight};
  font-size: ${(props) => props.theme.style.apply.textAreaSize};
  line-height: ${(props) => props.theme.style.apply.textAreaLineHeight};
  display: block;
  resize: vertical;
`;

export const AreaContainer = styled.div`
  width: 100%;
  margin-bottom: 20px;
  margin-top: 5px;
`;

export const InputLabel = styled.label`
  width: 100%;
  font-family: ${(props) => props.theme.style.apply.inputFontFamily};
  color: ${(props) => props.theme.style.apply.inputColor};
  font-size: ${(props) => props.theme.style.apply.inputSize};
  font-weight: ${(props) => props.theme.style.apply.inputWeight};
  line-height: ${(props) => props.theme.style.apply.inputLineHeight};
  text-align: left;
  text-transform: uppercase;
  letter-spacing: normal;
  margin-top: 30px;

  @media (max-width: 850px) {
    color: ${(props) =>
      props.theme.style.apply.titleColorMobile
        ? props.theme.style.apply.titleColorMobile
        : props.theme.style.apply.inputColor};
  }
`;

export const FormContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const InputFiled = styled(Input)`
  margin-top: 10px;
`;

export const SendButton = styled(Button)`
  width: 100%;
  height: 50px;
  justify-content: center;
  margin-top: 40px;
  display: flex;
  font-weight: ${(props) =>
    props.theme.style.apply.sendButtonFontWeight ??
    props.theme.style.button.fontWeight};
  transition: 0.5s all linear;
  &:hover {
    background: ${(props) =>
      props.theme.style.apply.sendButtonHoverBgr
        ? props.theme.style.apply.sendButtonHoverBgr
        : ""};
    color: ${(props) =>
      props.theme.style.apply.sendButtonHoverColor
        ? props.theme.style.apply.sendButtonHoverColor
        : ""};
  }
`;
export const ContainerCV = styled.div`
  border-radius: ${(props) => props.theme.style.apply.cvRadius};
  border: ${(props) => props.theme.style.apply.cvBorder};
  background-color: ${(props) => props.theme.style.apply.cvBackground};
  font-size: ${(props) => props.theme.style.apply.cvSize};
  font-weight: ${(props) => props.theme.style.apply.cvWeight};
  font-family: ${(props) => props.theme.style.apply.cvFontFamily};
  padding: 10px;
  margin-top: 10px;
  color: ${(props) => props.theme.style.apply.cvColor};
  display: flex;
  align-items: center;
`;

export const DeleteCV = styled.button`
  outline: none;
  border-radius: 5px;
  border: ${(props) => props.theme.style.apply.cvBorder};
  width: 24px;
  height: 24px;
  background: inherit;
  margin-left: auto;
  cursor: pointer;
  svg {
    stroke: ${(props) => props.theme.style.apply.addButtonColorStroke};
    fill: ${(props) => props.theme.style.apply.addButtonColorStroke};
  }
`;

export const DesktopContainer = styled(Container)`
  max-width: 100%;
  width: 100%;
  margin-bottom: 60px;
  padding: 50px;
  background-color: ${(props) => props.theme.style.apply.background};
  border: ${(props) => props.theme.style.apply.border};
  border-radius: ${(props) => props.theme.style.apply.borderRadius};
  @media (max-width: 1080px) {
    padding: 50px 30px;
  }
  @media (max-width: 700px) {
    padding: 50px 10px;
  }
`;

export const DesktopAddButton = styled(AddButton)`
  width: 100%;
  font-weight: ${(props) => props.theme.style.apply.addButtonWeightMobile};
  border-radius: ${(props) => props.theme.style.apply.addButtonRadius};
`;

export const FeildsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
`;

export const FieldItem = styled.div`
  margin-bottom: 10px;
`;
export const AgreementDesktopContainer = styled.div`
  width: 70%;
  margin: 0 auto;
  margin-top: 30px;
`;

export const DesktopSendButton = styled(SendButton)`
  margin-top: 30px;
  @media (max-width: 1080px) {
    padding: 15px 30px;
    text-transform: lowercase;
  }
  @media (max-width: 750px) {
    padding: 15px 10px;
    text-transform: lowercase;
  }
`;

export const Miniloader = styled.div`
  margin-left: auto;
`;

export const Notice = styled.div`
  color: ${(props) => props.theme.style.apply.errorMessageColor};
  font-size: 11px;
  display: flex;
  justify-content: center;
  opacity: ${(props) => (props.open ? 1 : 0)};
  align-items: center;
  max-height: ${(props) => (props.open ? "40px" : 0)};
  padding: ${(props) => (props.open ? "8px" : 0)};
  transition: 0.5s;
  svg {
    stroke: ${(props) => props.theme.style.apply.errorMessageColor};
    fill: ${(props) => props.theme.style.apply.errorMessageColor};
  }
`;

export const GdrAgreementContainerMobile = styled.div`
  padding-top: 30px;
`;
