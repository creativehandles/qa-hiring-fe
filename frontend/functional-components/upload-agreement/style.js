import styled from "styled-components";
import { Button } from "../../components/common/button";

export const Container = styled.div`
  margin: 0 auto;
`;

export const LoaderDescription = styled.div`
  padding: 30px 0 0 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  background: ${(props) =>
    !props.isOpportunitiesPage && props.mobile
      ? props.theme.style.upload.background
      : null};
  & > p {
    color: ${(props) =>
      props.isOpportunitiesPage
        ? props.theme.style.loading.titleLoadingColorOpportunities
        : props.theme.style.loading.titleColorHome};
    font-family: ${(props) => props.theme.style.loading.titleFontFamily};
    font-size: ${(props) => props.theme.style.loading.titleSize};
    font-weight: ${(props) => props.theme.style.loading.titleWeight};
    letter-spacing: ${(props) => props.theme.style.loading.letterSpacing};
    text-align: center;
    margin-bottom: 15px;
  }
  & > div {
    line-height: ${(props) => props.theme.style.loading.lineHeight};
    color: ${(props) =>
      props.isOpportunitiesPage
        ? props.theme.style.loading.fontColorOpportunities
        : props.theme.style.loading.fontColorHome};
    font-family: ${(props) => props.theme.style.loading.fontFamily};
    font-size: ${(props) => props.theme.style.loading.fontSize};
    font-weight: ${(props) => props.theme.style.loading.fontWeight};
    letter-spacing: ${(props) => props.theme.style.loading.letterSpacing};
    text-align: left;
    margin-bottom: 10px;
  }
`;

export const AlreadyLoaded = styled.div`
  display: flex;
  text-overflow: ellipsis;
  flex-direction: column;
  align-items: flex-start;
  & > p {
    color: ${(props) =>
      props.isOpportunitiesPage
        ? props.theme.style.loading.titleLoadedColorOpportunities
        : props.theme.style.loading.titleColorHome};
    font-size: ${(props) => props.theme.style.results.titleSize};
    font-weight: ${(props) => props.theme.style.results.titleWeight};
    font-family: ${(props) => props.theme.style.results.titleFontFamily};
    letter-spacing: ${(props) => props.theme.style.results.titleLetterSpacing};
    text-align: center;
    margin-bottom: 15px;
    white-space: nowrap;
    text-transform: ${(props) =>
      props.theme.style.loading.textTransform || "inherit"};
  }
`;

export const ContinueButton = styled(Button)`
  margin-top: 50px;
  background: ${(props) =>
    !props.isChecked
      ? props.theme.style.button.colorDisabled
      : props.theme.style.button.background};
  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.hoverBackgroundColor};
    color: ${(props) => props.theme.style.upload.hoverTextCollor};
    transition: all 0.3s;
  }
`;

export const MobileLoaderDescription = styled(LoaderDescription)`
  padding: 0px 43px;
  & > p {
    font-size: ${(props) => props.theme.style.loading.headerSizeMobile};
    font-weight: ${(props) => props.theme.style.loading.headerWeightMobile};
    color: ${(props) =>
      props.isOpportunitiesPage
        ? props.theme.style.loading.titleLoadingColorOpportunities
        : props.theme.style.loading.headerColorMobile};
    width: 100%;
  }
  & > div {
    font-size: ${(props) => props.theme.style.loading.sizeMobile};
    line-height: ${(props) => props.theme.style.loading.lineHeightMobile};
    text-align: center;
    margin: 0 auto;
    margin-bottom: 40px;
  }
`;

export const MobileAlreadyLoaded = styled(AlreadyLoaded)`
  align-items: center;
  margin: 0 auto;
  background: ${(props) =>
    props.isOpportunitiesPage ? null : props.theme.style.upload.background};
  & > p {
    font-size: ${(props) => props.theme.style.results.titleSizeMobile};
    font-weight: ${(props) => props.theme.style.results.titleWeightMobile};
    color: inherit;
  }
`;

export const MobileAgreementContainer = styled.div`
  width: 260px;
`;

export const MobileContinueButton = styled(ContinueButton)`
  margin-bottom: 40px;
  margin-top: 10px;
`;

export const GDPRContainer = styled.div`
  max-width: 420px;
`;

export const DescriptionContianer = styled.div``;

export const ButtonContainer = styled.div`
  display: flex;
`;

export const Text = styled.p`
  margin: 50px auto;
  font-family: ${(props) => props.theme.style.upload.fontFamily};
  color: ${(props) => props.theme.style.upload.fontColor};
  font-weight: ${(props) => props.theme.style.upload.fontWeight};
  font-size: ${(props) => props.theme.style.upload.fontSize};
  text-align: left;
  line-height: ${(props) => props.theme.style.upload.lineHeight};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
`;

export const Title = styled.div`
  color: ${(props) => props.theme.style.upload.titleColor};
  font-family: ${(props) => props.theme.style.upload.titleFontFamily};
  font-weight: ${(props) => props.theme.style.upload.titleWeight};
  text-align: left;
  font-size: ${(props) => props.theme.style.upload.titleSize};
  letter-spacing: ${(props) => props.theme.style.upload.letterSpacing};
  text-transform: ${(props) =>
    props.theme.style.upload.textTransform || "inherit"};
  position: relative;

  &::after {
    content: "${(props) => props.theme.style.upload.afterClass}";
    color: #e00700;
    position: absolute;
    bottom: -10px;
    @media (max-width: 850px) {
      bottom: -5px;
    }
  }
`;

export const ViewButton = styled.button`
  padding: 10px;
  border-radius: ${(props) => props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) => props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.theme.style.upload.highlightedButtonBackground};
  outline: none;
  cursor: pointer;
  display: block;
  font-family: ${(props) => props.theme.style.upload.seeAllButtonFontFamily};
  font-size: ${(props) => props.theme.style.upload.seeAllButtonSize};
  font-weight: ${(props) => props.theme.style.upload.seeAllButtonWeight};
  color: ${(props) => props.theme.style.upload.highlightedButtonColor};
  margin-right: 20px;
  transition: color 0.5s linear, background-color 0.5s linear;
  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverBgr
        ? props.theme.style.upload.seeAllButtonHoverBgr
        : ""};
    color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverColor
        ? props.theme.style.upload.seeAllButtonHoverColor
        : ""};
  }
`;

export const ViewButtonUpload = styled.label`
  padding: 10px;
  flex-basis: auto;
  border-radius: ${(props) => props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) =>
    props.theme.style.upload.anotherCVButtonBorder
      ? props.theme.style.upload.anotherCVButtonBorder
      : props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.theme.style.upload.seeAllButtonBackground};
  outline: none;
  cursor: pointer;
  display: flex;
  display: -webkit-inline;
  font-family: ${(props) => props.theme.style.upload.seeAllButtonFontFamily};
  font-size: ${(props) => props.theme.style.upload.seeAllButtonSize};
  font-weight: ${(props) => props.theme.style.upload.seeAllButtonWeight};
  color: ${(props) => props.theme.style.upload.seeAllButtonColor};
  margin-right: 20px;
  & > input {
    width: 0;
    display: none;
    height: 0;
  }

  transition: color 0.5s linear, background-color 0.5s linear;
  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverBgr
        ? props.theme.style.upload.seeAllButtonHoverBgr
        : ""};
    color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverColor
        ? props.theme.style.upload.seeAllButtonHoverColor
        : ""};
  }
`;
