import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GdrAgreement } from "../../components/common/gdr-agreement";
import { open } from "../../redux/slices/modals";
import ConfigStyles from "../../utils/config-styles";
import { getAPIURL } from "../../utils/api-url/index";
import { setGDPR as setGDPRUtil, getGDPR } from "../../utils/gdpr";
import {
  Container,
  LoaderDescription,
  AlreadyLoaded,
  ContinueButton,
  MobileAlreadyLoaded,
  MobileLoaderDescription,
  MobileAgreementContainer,
  MobileContinueButton,
  GDPRContainer,
  DescriptionContianer,
  Title,
  Text,
  ButtonContainer,
  ViewButtonUpload,
  ViewButton,
} from "./style";
import { useRouter } from "next/router";
import { setGDPR } from "../../redux/slices/agreement/functions";
import { setCV, setUploadedThisSession } from "../../redux/slices/CV";
import useTranslation from "../../hooks/useTranslation";

export const UploadAgreement = ({
  isLoaded,
  setIsAccept,
  mobile,
  setIsLoaded,
  setNeedFetch,
}) => {
  const agreement = getGDPR();
  const [isChecked, setIsChecked] = useState(agreement);
  const candidate = useSelector((state) => state.candidate.value);
  const dispatch = useDispatch();
  const localStyles = ConfigStyles();
  const { results } = useSelector((state) => state.styles.styles);
  const router = useRouter();
  const isOpportunitiesPage = router.pathname.includes("/opportunities");
  const cVuploadedThisSession = useSelector(
    (state) => state.CV.uploadedThisSession
  );
  const { t } = useTranslation();

  const checkboxLabel = (() => {
    return (
      <div>
        {t("resultsText")}
        <span
          style={{ textDecoration: "underline", fontWeight: 600 }}
          onClick={(e) => {
            e.stopPropagation();
            dispatch(open("gdpr"));
          }}
        >
          {t("resultsGdprText")}
        </span>
      </div>
    );
  })();

  const onHandleContinue = () => {
    setIsAccept(true);
    setGDPRUtil(isChecked);
    if (candidate.id) {
      dispatch(setGDPR(getAPIURL()));
    }
  };

  const handleCheckboxChange = (value) => {
    setIsChecked(value);
  };

  const uploadNewCV = (e) => {
    if (setNeedFetch) setNeedFetch(true);
    if (setIsLoaded) setIsLoaded(false);
    e.stopPropagation();
    e.preventDefault();
    dispatch(setCV(e.target.files[0]));
    dispatch(setUploadedThisSession(true));
  };

  const renderLoading = () => {
    return (
      <LoaderDescription
        isOpportunitiesPage={isOpportunitiesPage}
        mobile={mobile}
      >
        <p>{t("loadingTitle")}</p>
        <div>{t("loadingText")}</div>
      </LoaderDescription>
    );
  };

  const renderLoadedFirstTimeUpload = () => {
    return (
      <AlreadyLoaded isOpportunitiesPage={isOpportunitiesPage}>
        <p>{t("resultsTitle")}</p>
        {localStyles.agreement.home === "true" && (
          <GDPRContainer>
            <GdrAgreement
              center={true}
              isChecked={isChecked}
              handleCheckboxChange={handleCheckboxChange}
              label={checkboxLabel}
            />
          </GDPRContainer>
        )}
        <ContinueButton
          isChecked={isChecked}
          onClick={() => onHandleContinue()}
          title={t("resultsButtonName")}
        />
      </AlreadyLoaded>
    );
  };

  const renderLoadedPreviouslyUploaded = () => {
    return (
      <DescriptionContianer>
        <Title>{t("resultsTitleBackToResults")}</Title>
        <Text
          dangerouslySetInnerHTML={{ __html: t("resultsTextBackToResults") }}
        />
        <ButtonContainer>
          <ViewButton onClick={() => router.push("/opportunities")}>
            {t("resultsBackToOpportunitites")}
          </ViewButton>
          <ViewButtonUpload>
            {t("resultsButtonUploadNewCV")}
            <input type="file" onChange={(e) => uploadNewCV(e, true)} />
          </ViewButtonUpload>
        </ButtonContainer>
      </DescriptionContianer>
    );
  };

  if (mobile) {
    return (
      <Container>
        {!isLoaded ? (
          <MobileLoaderDescription
            isOpportunitiesPage={isOpportunitiesPage}
            mobile={mobile}
          >
            <p>{t("loadingTitle")}</p>
            <div>{t("loadingText")}</div>
          </MobileLoaderDescription>
        ) : (
          <MobileAlreadyLoaded isOpportunitiesPage={isOpportunitiesPage}>
            <p>{t("resultsTitle")}</p>
            <MobileAgreementContainer>
              {localStyles.agreement.home === "true" && (
                <GdrAgreement
                  isOpportunitiesPage={isOpportunitiesPage}
                  center={true}
                  isChecked={isChecked}
                  handleCheckboxChange={handleCheckboxChange}
                  label={checkboxLabel}
                  mobile
                />
              )}
            </MobileAgreementContainer>

            <MobileContinueButton
              isChecked={isChecked}
              onClick={() => onHandleContinue()}
              title={t("resultsButtonName")}
            />
          </MobileAlreadyLoaded>
        )}
      </Container>
    );
  }

  return (
    <Container>
      {!isLoaded && renderLoading()}
      {isLoaded && cVuploadedThisSession && renderLoadedFirstTimeUpload()}
      {isLoaded && !cVuploadedThisSession && renderLoadedPreviouslyUploaded()}
    </Container>
  );
};
