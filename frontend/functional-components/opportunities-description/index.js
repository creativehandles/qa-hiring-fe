import React, { useRef } from "react";
import {
  Container,
  JobContainer,
  JobTitle,
  ApplyContainer,
  ShortText,
  ApplyButton,
  DescriptionContainer,
  Description,
} from "./style";
import { VacancyForm } from "../vacancy-form";
import { useSelector } from "react-redux";
import useTranslation from "../../hooks/useTranslation";

export const OpportunitiesDescription = ({ job }) => {
  const { t } = useTranslation();
  const { showLanguage } = useSelector(
    (state) => state.styles.styles.jobDescription
  );
  const target = useRef();
  const scrollToForm = () => {
    target.current.scrollIntoView({
      behavior: "smooth",
    });
  };

  return (
    <Container>
      <JobContainer>
        <JobTitle>{job.name}</JobTitle>
        <ApplyContainer>
          <div>
            {job.company_name && <ShortText>{job.company_name}</ShortText>}
            {job.city?.name && (
              <ShortText>
                {job.city?.name}
                {showLanguage && ", "}
              </ShortText>
            )}
            {showLanguage && <ShortText>{job.language}</ShortText>}
          </div>
          <ApplyButton onClick={scrollToForm}>{t("apply")}</ApplyButton>
        </ApplyContainer>
      </JobContainer>
      <DescriptionContainer>
        <Description
          dangerouslySetInnerHTML={{ __html: job.description }}
        ></Description>
      </DescriptionContainer>
      <VacancyForm mhub_id={job.mhub_id} target={target} />
    </Container>
  );
};
