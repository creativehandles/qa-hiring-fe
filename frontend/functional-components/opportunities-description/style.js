import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.style.job.background};
  border: ${(props) => props.theme.style.job.border};
  padding: 50px;
  border-radius: ${(props) => props.theme.style.job.borderRadius};
  width: 100%;
  @media (max-width: 1080px) {
    padding: 50px 30px;
  }
  @media (max-width: 700px) {
    padding: 50px 10px;
  }
`;

export const JobContainer = styled.div`
  font-family: ${(props) =>
    props.theme.style.mainOpportunities.jobContainerFontFamily};
  margin-bottom: 60px;
`;

export const DescriptionContainer = styled.div`
  margin-bottom: 60px;
  & > span {
    display: block;
    margin: 0 auto;
    display: flex;
    justify-content: center;
  }
`;

export const JobTitle = styled.div`
  font-family: ${(props) => props.theme.style.job.titleFontFamily};
  color: ${(props) => props.theme.style.job.titleColor};
  font-size: ${(props) => props.theme.style.job.titleSize};
  font-weight: ${(props) => props.theme.style.job.titleWeight};
  letter-spacing: ${(props) => props.theme.style.job.letterSpacing};
`;
export const ShortText = styled.span`
  font-family: ${(props) => props.theme.style.job.tagsFontFamily};
  color: ${(props) => props.theme.style.job.tagsColor};
  font-size: ${(props) => props.theme.style.job.tagsSize};
  line-height: ${(props) => props.theme.style.job.tagsLineHeight};
  font-weight: ${(props) => props.theme.style.job.tagsWeight};
  letter-spacing: 0.7px;
  &:first-child {
    &:after {
      content: "";
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background: ${(props) => props.theme.style.job.tagsColor};
      display: inline-block;
      width: 3px;
      height: 3px;
      margin: 3px 5px;
    }
  }
`;

export const ApplyButton = styled.button`
  padding: 8px 25px;
  font-family: ${(props) => props.theme.style.job.buttonFontFamily};
  border-radius: ${(props) => props.theme.style.job.buttonRadius};
  background-color: ${(props) => props.theme.style.job.buttonBackground};
  outline: none;
  letter-spacing: 0.6px;
  border: none;
  color: ${(props) => props.theme.style.job.buttonColor};
  font-size: ${(props) => props.theme.style.job.buttonSize};
  font-weight: ${(props) => props.theme.style.job.buttonWeight};
  text-transform: uppercase;
  cursor: pointer;
  transition: 0.5s;
  &:hover {
    opacity: ${(props) =>
      props.theme.style.apply.sendButtonHoverOpacity
        ? props.theme.style.apply.sendButtonHoverOpacity
        : "0.7"};

    background: ${(props) =>
      props.theme.style.apply.sendButtonHoverBgr
        ? props.theme.style.apply.sendButtonHoverBgr
        : ""};
    color: ${(props) =>
      props.theme.style.apply.sendButtonHoverColor
        ? props.theme.style.apply.sendButtonHoverColor
        : ""};
  }
`;

export const DescriptionTitle = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 13px;
  font-weight: 300;
  margin-bottom: 17px;
  &:first-child {
    font-weight: 800;
  }
`;

export const Description = styled.div`
  color: ${(props) => props.theme.style.job.descriptionColor};
  font-size: ${(props) => props.theme.style.job.descriptionSize};
  font-weight: ${(props) => props.theme.style.job.descriptionWeight};
  line-height: ${(props) => props.theme.style.job.descriptionLineHeight};
  text-align: left;
  margin-bottom: 56px;
  margin-right: 30px;
  & ul {
    list-style-type: disc;
    padding: 7px 0 7px 16px;
  }
  & p,
  li {
    padding: 7px 0 7px 0;
  }
  & img {
    max-width: 100%;
  }
  & > * {
    font-family: ${(props) => props.theme.style.job.descriptionFontFamily};
  }
`;
export const ApplyContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 15px;
`;
export const Contact = styled.div`
  display: inline;
  margin-right: 60px;
  color: ${(props) => props.theme.style.global.fontColor};
  & > span {
    margin-left: 15px;
    font-size: 14px;
    font-weight: 300;
  }
`;
export const ContactContainer = styled.div`
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  border: 1px solid #ebeef7;
  padding: 15px;
`;
export const TextSectionContainer = styled.div``;
