import styled, { keyframes } from "styled-components";

const slideDownAnimation = keyframes`
 0% { right: -150px; }
 50% { right: -75px; }
 100% { right: 0px; }
`;

export const FloatingButton = styled.div`
  background-color: ${(props) =>
    props.theme.style.contactFloatingForm.background};
  color: ${(props) => props.theme.style.contactFloatingForm.color};
  width: 50px;
  height: 160px;
  border-radius: 16px 0 0 16px;
  position: absolute;
  top: ${(props) =>
    props.theme.style.contactFloatingForm.positionTopOffset || "130px"};
  right: 0;
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  animation-name: ${slideDownAnimation};
  animation-duration: 0.4s;
  animation-timing-function: linear;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const IconArrowDown = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    width: 20px;
    height: 20px;
    fill: ${(props) => props.theme.style.contactFloatingForm.color};
    stroke: ${(props) => props.theme.style.contactFloatingForm.background};
  }
`;

export const Container = styled.div`
  background-color: ${(props) =>
    props.theme.style.contactFloatingForm.background};
  color: ${(props) => props.theme.style.contactFloatingForm.color};
  width: 375px;
  position: absolute;
  top: ${(props) =>
    props.theme.style.contactFloatingForm.positionTopOffset || "130px"};
  right: 0;
  border-radius: 16px 0 0 16px;
  padding: 40px 30px 30px;
  display: ${(props) => (props.isOpened ? "block" : "none")};
  animation-name: ${slideDownAnimation};
  animation-duration: 0.4s;
  animation-timing-function: linear;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
  z-index: 10;
`;

export const ContainerDialog = styled(Container)`
  top: ${(props) =>
    props.theme.style.contactFloatingForm.positionTopOffset || "130px"};
  height: max-content;
  display: block;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const Title = styled.p`
  text-align: center;
  font-size: 16px;
  margin-bottom: 20px;
  font-weight: ${(props) =>
    props.theme.style.contactFloatingForm.fontWeight || "700"};
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const InputContainer = styled.div`
  margin-bottom: 20px;
  label {
    color: ${(props) =>
      props.theme.style.contactFloatingForm.labelColor || "#fff"};
    text-transform: uppercase;
    display: block;
    font-size: 12px;
    font-weight: ${(props) =>
      props.theme.style.contactFloatingForm.fontWeight || "700"};
    padding-left: 10px;
    font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
    letter-spacing: ${(props) =>
      props.theme.style.contactFloatingForm.letterSpacing || 0};
  }
`;

export const Input = styled.input`
  border: ${(props) =>
    props.theme.style.contactFloatingForm.inputBorder || "none"};
  border-radius: 2px;
  color: ${(props) =>
    props.theme.style.contactFloatingForm.inputColor || "#3f5aa6"};
  height: 40px;
  width: 100%;
  padding-left: 10px;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
  font-size: ${(props) =>
    props.theme.style.contactFloatingForm.inputFontSize || "14px"};
`;

export const Button = styled.button`
  border: none;
  background-color: ${(props) =>
    props.theme.style.contactFloatingForm.buttonBackground};
  border-radius: 2px;
  color: ${(props) => props.theme.style.contactFloatingForm.buttonColor};
  width: auto;
  padding: 8px 20px;
  text-transform: uppercase;
  font-weight: 600;
  margin: auto;
  display: block;
  cursor: pointer;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
  transition: color 0.5s linear, background-color 0.5s linear;
  & :hover {
    background-color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverBgr
        ? props.theme.style.upload.seeAllButtonHoverBgr
        : ""};
    color: ${(props) =>
      props.theme.style.upload.seeAllButtonHoverColor
        ? props.theme.style.upload.seeAllButtonHoverColor
        : ""};
  }
`;

export const IconClose = styled.div`
  cursor: pointer;
  width: 24px;
  top: 15px;
  right: 15px;
  height: 24px;
  position: absolute;
  stroke: ${(props) => props.theme.style.contactFloatingForm.color};
  svg {
    width: 40px;
    height: 40px;
  }
`;

export const CustomCheckContainer = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  margin: 10px 0px;
  width: 100%;
  justify-content: ${(props) => (props.center ? "center" : "flex-start")};
`;

export const Label = styled.div`
  border: 1px solid rgba(0, 0, 0, 0);
  color: ${(props) => props.theme.style.contactFloatingForm.color};
  font-size: ${(props) =>
    props.mobile || props.isOpportunitiesPage
      ? props.theme.style.checkbox.fontSizeMobile
      : props.theme.style.checkbox.fontSizeDesktop};
  line-height: ${(props) => props.theme.style.checkbox.lineHeight};
  font-weight: ${(props) => props.theme.style.checkbox.fontWeight};
  font-family: ${(props) => props.theme.style.checkbox.fontFamily};
  letter-spacing: ${(props) => props.theme.style.checkbox.letterSpacing};
  text-align: left;
  margin-left: 18px;
  cursor: pointer;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
  small {
    font-size: 14px;
  }
`;

export const CustomCheckbox = styled.input`
  position: absolute;
  left: 0;
  z-index: 0;
  opacity: 0;
  width: 20px;
  height: 20px;
  cursor: pointer;
`;

export const CheckContainer = styled.div`
  fill: ${(props) => props.theme.style.contactFloatingForm.color};
  stroke: ${(props) =>
    props.isChecked
      ? props.theme.style.contactFloatingForm.color
      : props.theme.style.contactFloatingForm.color};
`;
