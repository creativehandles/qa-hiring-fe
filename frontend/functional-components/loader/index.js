import React, { useEffect, useMemo, useState } from "react";
import {
  MainContainer,
  Loader,
  LoaderContainer,
  LoaderStatus,
  LoaderCircle,
  SpinnerWrapper,
} from "./style";
import { Check } from "../svg-component";
import { useDispatch, useSelector } from "react-redux";
import { fetchVacanciesForCV } from "../../redux/slices/vacancies/open-vacancies/functions/anonimus-search";
import { getAPIURL } from "../../utils/api-url/index";
import { useRouter } from "next/router";
import { errorStatusCodesUploadCV } from "../../constants/errorCodes";
import useTranslation from "../../hooks/useTranslation";

export const LoaderOpportuniies = ({
  isLoaded,
  setIsLoaded,
  mobile,
  stillLoaded,
  alreadyLoaded,
  needFetch = true,
}) => {
  const [percent, setPercent] = useState(400);
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();
  const isOpportunitiesPage = router.pathname.includes("/opportunities");
  const { t } = useTranslation();

  useEffect(() => {
    if (!needFetch) return null;
    if (state.CV.value) {
      const errorCodesTranslated = errorStatusCodesUploadCV.map((el) => ({
        code: el.code,
        text: t(el.key),
      }));
      dispatch(
        fetchVacanciesForCV({ apiURL: getAPIURL(), errorCodesTranslated })
      );
    }
  }, [state.CV.value]);

  useEffect(() => {
    let fetchingData;
    if (!isLoaded) {
      fetchingData = setInterval(() => {
        setPercent((percent) => {
          if (percent - 10 > 40) {
            return percent - 5;
          } else {
            return 40;
          }
        });
      }, 100);
    }

    return () => {
      if (fetchingData) {
        clearInterval(fetchingData);
      }
    };
  }, [isLoaded]);

  useEffect(() => {
    if (
      percent < 360 &&
      !state.openVacancies.loading &&
      !state.openVacancies.message
    ) {
      setPercent(400);
      setIsLoaded(true);
    }
  }, [percent, state.openVacancies.loading, state.openVacancies.message]);

  const showProcent = useMemo(() => {
    const supplement = percent === 40 ? 5 : 0;
    const currentProcent = Math.ceil((400 - percent) / 4) + supplement;

    return currentProcent;
  }, [percent]);

  const renderLoader = () => {
    /* PURE CSS Loader with Microsoft style
      here each div represents a dot in spinning trail */
    return (
      <SpinnerWrapper>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </SpinnerWrapper>
    );
  };

  return (
    <MainContainer forMobile={mobile} isOpportunitiesPage={isOpportunitiesPage}>
      <LoaderContainer>
        {!isLoaded ? (
          <>
            {renderLoader()}
            <LoaderStatus forMobile={mobile}>{stillLoaded}</LoaderStatus>
          </>
        ) : (
          <>
            <Loader
              percent="0"
              forMobile={mobile}
              isOpportunitiesPage={isOpportunitiesPage}
            >
              <LoaderCircle cx="85" cy="73" r="60"></LoaderCircle>
              <circle cx="85" cy="73" r="60"></circle>
            </Loader>
            <span>
              <Check />
            </span>
            <LoaderStatus forMobile={mobile}>{alreadyLoaded}</LoaderStatus>
          </>
        )}
      </LoaderContainer>
    </MainContainer>
  );
};
