import styled, { keyframes } from "styled-components";

export const MainContainer = styled.div`
  padding: 20px 20px 0px 20px;
  margin: 0 auto;
  background: ${(props) =>
    props.forMobile
      ? props.theme.style.upload.background
      : props.isOpportunitiesPage
      ? props.theme.style.loader.loaderSvgFillOpportunities
      : props.theme.style.loader.loaderSvgFill};
  flex-direction: column;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const Loader = styled.svg`
  width: 150px;
  position: relative;
  transition: 0.5s;
  filter: drop-shadow(${(props) => props.theme.style.loader.uploadSvgShadow});
  stroke: ${(props) => props.theme.style.loader.uploadSvgLoadingColor};
  stroke-width: ${(props) => props.theme.style.loader.uploadSvgStroke};
  stroke-linecap: round;
  stroke-dasharray: 400;
  fill: ${(props) =>
    props.forMobile
      ? props.theme.style.loader.loaderSvgFill
      : props.isOpportunitiesPage
      ? props.theme.style.loader.loaderSvgFillOpportunities
      : props.theme.style.loader.loaderSvgFill};
  transform: rotateZ(-90deg);
  stroke-dashoffset: ${(props) => props.percent};
  & circle {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
  }
  & ::last-child {
    fill: none;
    color: red;
  }
`;
export const LoaderCircle = styled.circle`
  stroke: ${(props) => props.theme.style.loader.uploadSvgColor};
  stroke-width: ${(props) => props.theme.style.loader.uploadSvgStroke};
  position: absolute;
  stroke-dasharray: 400;
  stroke-dashoffset: 0;
`;

export const LoaderContainer = styled.div`
  max-width: 376px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  position: relative;
  align-items: center;
  & > span {
    position: absolute;
    top: 55px;
    font-family: ${(props) => props.theme.style.loader.uploadPercentfontFamily};
    stroke: ${(props) => props.theme.style.loader.uploadSvgCompleteColor};
    font-size: ${(props) => props.theme.style.loader.uploadPercentSize};
    color: ${(props) => props.theme.style.loader.uploadPercentColor};
    font-weight: ${(props) => props.theme.style.loader.uploadPercentWeight};
  }
`;
export const LoaderStatus = styled.p`
  font-family: ${(props) =>
    props.theme.style.loading.uploadDescriptionFontFamily};
  color: ${(props) =>
    props.forMobile
      ? props.theme.style.loading.uploadDescriptionColorMobile
      : props.theme.style.loading.uploadDescriptionColorDesktop};
  font-weight: ${(props) =>
    props.forMobile
      ? props.theme.style.loading.titleWeightMobile
      : props.theme.style.loading.uploadDescriptionWeight};
  font-size: ${(props) =>
    props.forMobile
      ? props.theme.style.loading.titleSizeMobile
      : props.theme.style.loading.uploadDescriptionSize};
  text-align: center;
  margin-bottom: 10px;
`;

const ldsRoller = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const SpinnerWrapper = styled.div`
  display: inline-block;
  position: relative;
  width: 80px;
  height: 80px;
  margin-bottom: 20px;
  div {
    animation: ${ldsRoller} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    transform-origin: 40px 40px;
    &::after {
      content: " ";
      display: block;
      position: absolute;
      width: 7px;
      height: 7px;
      border-radius: 50%;
      background: ${(props) => props.theme.style.loader.uploadSvgLoadingColor};
      margin: -4px 0 0 -4px;
    }
    &:nth-child(1) {
      animation-delay: -0.036s;
      &::after {
        top: 63px;
        left: 63px;
      }
    }
    &:nth-child(2) {
      animation-delay: -0.072s;
      &::after {
        top: 68px;
        left: 56px;
      }
    }
    &:nth-child(3) {
      animation-delay: -0.108s;
      &:after {
        top: 71px;
        left: 48px;
      }
    }
    &:nth-child(4) {
      animation-delay: -0.144s;
      &::after {
        top: 72px;
        left: 40px;
      }
    }
    &:nth-child(5) {
      animation-delay: -0.18s;
      &::after {
        top: 71px;
        left: 32px;
      }
    }
    &:nth-child(6) {
      animation-delay: -0.216s;
      &::after {
        top: 68px;
        left: 24px;
      }
    }
    &:nth-child(7) {
      animation-delay: -0.252s;
      &::after {
        top: 63px;
        left: 17px;
      }
    }
    &:nth-child(8) {
      animation-delay: -0.288s;
      &::after {
        top: 56px;
        left: 12px;
      }
    }
  }
`;
