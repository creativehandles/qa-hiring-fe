import styled from "styled-components";

export const Container = styled.div`
  padding: 20px;
  background: #ffffff;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  max-width: 350px;
  border: 1px solid #ebeef7;
`;

export const Title = styled.div`
  text-transform: uppercase;
  font-weight: 800;
  font-size: 12px;
  color: ${(props) => props.theme.style.global.fontColor};
  margin-bottom: 20px;
`;

export const Button = styled.button`
    cursor:pointer;
    border-radius: 4px;
    border: 1px solid ${(props) =>
      props.isActive
        ? props.theme.style.global.background
        : props.theme.style.global.fontColor};;
    background-color: ${(props) =>
      props.isActive ? "#fff" : props.theme.style.global.fontColor};
    padding: 8px 12px;
    margin-right: 20px;
    color: ${(props) =>
      props.isActive ? props.theme.style.global.fontColor : "#fff"};
    font-size: 12px;
    outline: none;
}

`;

export const SalaryContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 20px;
  padding-top: 20px;
  &::first-child {
    border: 1px solid green;
  }
`;

export const Description = styled.div`
  display: flex;
  background: #008076;
  align-items: center;
  padding: 10px 10px;
  color: #828899;
  background-color: ${(props) => props.theme.style.global.background};
  font-size: 12px;
`;
export const FirstSalaryContainer = styled(SalaryContainer)`
  border-bottom: 1px solid #ebeef7;
`;

export const Text = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 13px;
`;
export const ChekcBoxContainer = styled.div`
  justify-items: flex-end;
  display: grid;
  grid-template-columns: 1fr 1fr;
  width: 80%;
  margin-left: auto;
`;
