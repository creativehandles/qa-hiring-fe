import React, { useState } from "react";
import {
  Container,
  Title,
  FirstSalaryContainer,
  SalaryContainer,
  Button,
  ChekcBoxContainer,
  Text,
  Description,
} from "./style";
import { Checkbox } from "../../components/common/checkbox";
import MultiRange from "../../components/common/multi-ranger";
import { Warning } from "../svg-component";

export const SalaryFilter = () => {
  const [current, setCurrent] = useState("contractor");

  const onHandleSetCurrent = (target) => {
    setCurrent(target);
  };

  return (
    <Container>
      <Title>Salary</Title>
      <FirstSalaryContainer>
        <Button
          onClick={({ target }) => onHandleSetCurrent(target.id)}
          isActive={current !== "employee"}
          id="employee"
        >
          Employee
        </Button>
        <MultiRange
          disabled={current !== "employee"}
          style={{ width: "50%", height: 10, marginRight: 40 }}
        />
      </FirstSalaryContainer>
      <SalaryContainer>
        <Button
          onClick={({ target }) => onHandleSetCurrent(target.id)}
          isActive={current !== "contractor"}
          id="contractor"
        >
          Contractor
        </Button>
        <MultiRange
          disabled={current !== "contractor"}
          style={{ width: "50%", height: 10, marginRight: 40 }}
        />
      </SalaryContainer>
      <ChekcBoxContainer>
        <Text>Daly rate on</Text>
        <Checkbox />
      </ChekcBoxContainer>
      <Description>
        <Warning width="8%" style={{marginRight:10}} />
        These Sliders represent a full-time salary. For path-time positions,
        adjust accordingly
      </Description>
    </Container>
  );
};
