import styled from "styled-components";

export const Container = styled.div`
  max-width: 800px;
  margin: 0 auto;
`;

export const Title = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-family: ${(props) => props.theme.style.upload.titleFontFamily};
  font-size: 30px;
  text-align: center;
  margin-bottom: 60px;
`;

export const MobileContainer = styled(Container)`
  max-width: 376px;
  padding: 40px 30px;
  background: ${(props) => props.theme.style.upload.background};
`;

export const TitleMobile = styled(Title)`
  font-size: 18px;
  font-weight: normal;
  margin-bottom: 0px;
  color: ${(props) => props.theme.style.upload.headerColorMobile};
`;

export const SeeAllContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-weight: 600;
  margin-top: 40px;
  color: ${(props) => props.theme.style.global.fontColor};
  & > span {
    margin-top: 40px;
  }
`;

export const SeeAllButton = styled.button`
  margin-top: 40px;
  padding: 8px 25px;
  font-family: ${(props) => props.theme.style.job.buttonFontFamily};
  border-radius: ${(props) => props.theme.style.job.buttonRadius};
  background-color: ${(props) => props.theme.style.job.buttonBackground};
  outline: none;
  border: none;
  letter-spacing: 0.6px;
  color: ${(props) => props.theme.style.job.buttonColor};
  font-size: ${(props) => props.theme.style.job.buttonSize};
  font-weight: ${(props) => props.theme.style.job.buttonWeight};
  cursor: pointer;
  transition: 0.5s;
  &:hover {
    opacity: 0.7;
  }
`;

export const SeeAllButtonMobile = styled.button`
  padding: 10px;
  border-radius: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonRadius
      : props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonBorder
      : props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonBackground
      : props.theme.style.upload.seeAllButtonBackground};
  outline: none;
  margin: 0 auto;
  cursor: pointer;
  display: block;
  margin-top: 20px;
  color: ${(props) => props.theme.style.upload.seeAllButtonColor};
`;
