import React from "react";
import {
  MobileContainer,
  TitleMobile,
  Title,
  Container,
  SeeAllContainer,
  SeeAllButton,
  SeeAllButtonMobile,
} from "./style";
import { setSeeAllOpportunities } from "../../redux/slices/all-opportunities";
import { removeCV } from "../../redux/slices/CV";
import useTranslation from "../../hooks/useTranslation";
import { useDispatch } from "react-redux";

export const NoOpenVacancies = ({
  opportunities,
  setSeeDafault,
  hasDefaultVacancies,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const seeAllButtonMobileClicked = () => {
    dispatch(setSeeAllOpportunities(true));
    dispatch(removeCV());
  };

  const seeAllButtonOpportunitiesClicked = () => {
    setSeeDafault(true);
    dispatch(removeCV());
  };

  if (opportunities) {
    return (
      <Container>
        <Title>{t("noVacanciesAtTheMoment")}</Title>
        {hasDefaultVacancies && (
          <SeeAllContainer>
            <span>{t("orUppercase")}</span>
            <SeeAllButton onClick={seeAllButtonOpportunitiesClicked}>
              {t("uploadSeeAllOpportunitiesButtonName")}
            </SeeAllButton>
          </SeeAllContainer>
        )}
      </Container>
    );
  }

  return (
    <MobileContainer>
      <TitleMobile>{t("noVacanciesAtTheMoment")}</TitleMobile>
      {hasDefaultVacancies && (
        <SeeAllButtonMobile onClick={seeAllButtonMobileClicked}>
          {t("uploadSeeAllOpportunitiesButtonName")}
        </SeeAllButtonMobile>
      )}
    </MobileContainer>
  );
};
