import styled from "styled-components";

export const Container = styled.div`
  background: #ffffff;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  width: 100%;
  border: 1px solid #ebeef7;
`;

export const Title = styled.div`
  text-transform: uppercase;
  font-weight: 800;
  font-size: 12px;
  color: ${(props) => props.theme.style.global.fontColor};
  width: 50%;
  cursor: pointer;
  text-align: center;
  padding: 20px 0px;
  & :first-child {
    background: ${(props) =>
      props.currentTab ? "inherit" : props.theme.style.global.background};
    border-right: ${(props) => (props.currentTab ? "" : "1px solid #ebeef7")};
    border-bottom: ${(props) => (props.currentTab ? "" : "1px solid #ebeef7")};
  }
  & :last-child {
    background: ${(props) =>
      !props.currentTab ? "inherit" : props.theme.style.global.background};
    border-left: ${(props) => (!props.currentTab ? "" : "1px solid #ebeef7")};
    border-bottom: ${(props) => (!props.currentTab ? "" : "1px solid #ebeef7")};
  }
`;
export const DistanceContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const TitleContainer = styled.div`
  display: flex;
`;

export const ContentContainer = styled.div`
  padding: 20px;
`;
export const AddButton = styled.button`
  cursor: pointer;
  border: none;
  background: inherit;
  outline: none;
  color: ${(props) => props.theme.style.global.fontColor};
  display: grid;
  grid-auto-flow: column;
  grid-gap: 10px;
  align-items: center;
  padding: 10px 10px 10px 0;
  font-size: 14px;
`;
