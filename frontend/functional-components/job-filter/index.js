import React, { useState } from "react";
import {
  Container,
  DistanceContainer,
  TitleContainer,
  Title,
  ContentContainer,
  AddButton,
} from "./style";
import { Select } from "../../components/common/select";
import { SelectAdress } from "../../components/common/select-adress";
import { SelectDistance } from "../../components/common/select-distance";
import { Plus } from "../svg-component";

const adress = [
  "Praga",
  "Taganrog",
  "Rostov",
  "Novocherkasck",
  "Novgorod",
  "Berlin",
  "LA",
];
const km = ["+10km", "+20km", "+30km", "+40km", "+50km", "+60km", "+70km"];
const location = ["Remote work", "In-office-work", "Mixed"];

export const JobFilter = ({ location, distance, towns }) => {
  const [currentTab, setCurrentTab] = useState(true);
  const [count, setCount] = useState([1]);
  const getContent = () => {
    if (currentTab) {
      return (
        <>
          <Select data={location} />
          {count.map((el) => {
            return (
              <DistanceContainer key={el + "key"}>
                <SelectAdress data={adress} />
                <SelectDistance data={km} />
              </DistanceContainer>
            );
          })}
          <AddButton onClick={() => setCount([...count, 1])}>
            <Plus />
            Add a second address
          </AddButton>
        </>
      );
    } else {
      return <></>;
    }
  };

  return (
    <Container>
      <TitleContainer>
        <Title onClick={() => setCurrentTab(true)} currentTab={currentTab}>
          Job location
        </Title>
        <Title onClick={() => setCurrentTab(false)} currentTab={currentTab}>
          Travel Distance
        </Title>
      </TitleContainer>
      <ContentContainer>{getContent()}</ContentContainer>
    </Container>
  );
};
