import {
  Container,
  Title,
  InputContainer,
  Input,
  CustomCheckContainer,
  CheckContainer,
  CustomCheckbox,
  Label,
  Button,
  IconClose,
  TopBar,
  IconArrowDown,
  ContainerDialog,
} from "./style";
import { useDispatch, useSelector } from "react-redux";
import useTranslation from "../../hooks/useTranslation";
import { useState } from "react";
import { getGDPR } from "../../utils/gdpr";
import { BoxAgree, BoxDisagree, Close, ArrowBack } from "../svg-component";
import { submitContactInfo } from "../../redux/slices/contact/functions";
import { getAPIURL } from "../../utils/api-url";
import {
  setContactFormVisibility,
  setSuccessDialog,
} from "../../redux/slices/contact";
import {
  emailRegex,
  linkedInRegex,
  telephoneRegex,
} from "../../constants/appConstants";
import { setSeeAllOpportunities } from "../../redux/slices/all-opportunities";
import { open } from "../../redux/slices/modals";

export const ContactFloatingMobile = () => {
  const { t } = useTranslation();
  const {
    title,
    email,
    linkedin,
    telephone,
    inputPlaceholder,
    nameAndSurname,
  } = useSelector((state) => state.styles.styles.contactFloatingForm);
  const { showSuccessDialog, loading, showContactForm } = useSelector(
    (state) => state.contactForm
  );
  const dispatch = useDispatch();
  const agreement = getGDPR();
  const [isOpened, setIsOpened] = useState(false);
  const [formData, setFormData] = useState({
    email: "",
    phone: "",
    linkedin: "",
    gdpr_agreed: agreement || false,
  });

  const showIcon = () => {
    const icon = formData.gdpr_agreed ? <BoxAgree /> : <BoxDisagree />;
    return icon;
  };
  const handleInputChange = ({ currentTarget: input }) => {
    const updatedData = { ...formData };
    updatedData[input.name] = input.value;
    setFormData(updatedData);
  };

  const handleCheckboxChange = (value) => {
    setFormData((prevState) => {
      return {
        ...prevState,
        gdpr_agreed: value,
      };
    });
  };
  const checkboxLabel = (() => {
    return (
      <small>
        {t("resultsText")}
        <span
          style={{ textDecoration: "underline", fontWeight: 600 }}
          onClick={(e) => {
            e.stopPropagation();
            dispatch(open("gdpr"));
          }}
        >
          {t("resultsGdprText")}
        </span>
      </small>
    );
  })();

  const handleSubmit = () => {
    if (formData.email && !emailRegex.test(formData.email)) {
      alert(t("invalidEmail"));
      return;
    }
    if (formData.linkedin && !linkedInRegex.test(formData.linkedin)) {
      alert(t("invalidLinkedin"));
      return;
    }
    if (formData.phone && !telephoneRegex.test(formData.phone)) {
      alert(t("invalidPhone"));
      return;
    }
    if (
      formData.email === "" &&
      formData.linkedin === "" &&
      formData.phone === ""
    ) {
      alert(t("emptyFormSubmitError"));
      return;
    }
    dispatch(
      submitContactInfo({
        apiURL: getAPIURL(),
        data: formData,
      })
    );
  };

  const onButtonClick = () => {
    // Do a toggle to false and then true in order to trigger the scrollIntoView
    dispatch(setSeeAllOpportunities(false));
    dispatch(setSeeAllOpportunities(true));
  };

  const handleSuccessDialogClose = () => {
    dispatch(setSuccessDialog(false));
    dispatch(setContactFormVisibility(false));
    setIsOpened(false);
  };

  const renderSuccessDialog = () => {
    return (
      <ContainerDialog>
        <Title>{t("contactFormThankyouText")}</Title>
        <IconClose onClick={() => handleSuccessDialogClose()}>
          <Close />
        </IconClose>
        <Button onClick={onButtonClick}>{t("seeAllOpenPositions")}</Button>
      </ContainerDialog>
    );
  };

  return showContactForm ? (
    !loading && showSuccessDialog ? (
      renderSuccessDialog()
    ) : !isOpened ? (
      <TopBar onClick={() => setIsOpened(true)}>
        <IconArrowDown>
          <ArrowBack />
        </IconArrowDown>
      </TopBar>
    ) : (
      <Container isOpened={isOpened}>
        <Title dangerouslySetInnerHTML={{ __html: t(title) }} />
        <IconClose onClick={() => setIsOpened(false)}>
          <Close />
        </IconClose>
        {email && (
          <InputContainer>
            <label>{t("emailShort")}</label>
            <Input
              name="email"
              value={formData.email}
                  onChange={handleInputChange}
                  placeholder={
                    inputPlaceholder ? "jan@novak.cz" : ""
                  }
            />
          </InputContainer>
        )}
        {linkedin && (
          <InputContainer>
            <label>LinkedIn</label>
            <Input
              name="linkedin"
              value={formData.linkedin}
              onChange={handleInputChange}
              placeholder={
                inputPlaceholder ? "https://www.linkedin.com/in/novak " : ""
              }
            />
          </InputContainer>
        )}
        {nameAndSurname && (
          <InputContainer>
            <label>{t("nameAndSurname")}</label>
            <Input
              name="nameAndSurname"
              value={formData.nameAndSurname}
              onChange={handleInputChange}
              placeholder={inputPlaceholder ? "Jan Novák" : ""}
            />
          </InputContainer>
        )}
        {telephone && (
          <InputContainer>
            <label>{t("phone")}</label>
            <Input
              name="phone"
              value={formData.phone}
              onChange={handleInputChange}
              placeholder={inputPlaceholder ? "+420100100100" : ""}
            />
          </InputContainer>
        )}
        <CustomCheckContainer>
          <CustomCheckbox
            value={formData.gdpr_agreed}
            onChange={() => handleCheckboxChange(!formData.gdpr_agreed)}
            type="checkbox"
          />
          <CheckContainer isChecked={formData.gdpr_agreed}>
            {showIcon()}
          </CheckContainer>
          <Label onClick={() => handleCheckboxChange(!formData.gdpr_agreed)}>
            {checkboxLabel}
          </Label>
        </CustomCheckContainer>
        <Button onClick={handleSubmit}>{t("send")}</Button>
      </Container>
    )
  ) : null;
};
