import styled from "styled-components";

export const TopBar = styled.div`
  background-color: ${(props) =>
    props.theme.style.contactFloatingForm.backgroundMobile};
  color: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
  width: 100%;
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const IconArrowDown = styled.div`
  cursor: pointer;
  transform: rotate(270deg);
  svg {
    width: 16px;
    height: 16px;
    fill: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
    stroke: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
  }
`;

export const Container = styled.div`
  background-color: ${(props) =>
    props.theme.style.contactFloatingForm.backgroundMobile};
  color: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
  width: 100%;
  position: relative;
  left: 0;
  padding: 40px 30px 30px;
  display: ${(props) => (props.isOpened ? "block" : "none")};
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const ContainerDialog = styled(Container)`
  display: block;
`;

export const Title = styled.p`
  text-align: center;
  font-size: 16px;
  margin-bottom: 20px;
  font-weight: ${(props) =>
    props.theme.style.contactFloatingForm.fontWeight || "700"};
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const InputContainer = styled.div`
  margin-bottom: 20px;
  label {
    color: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
    text-transform: uppercase;
    display: block;
    font-size: 12px;
    font-weight: ${(props) =>
      props.theme.style.contactFloatingForm.fontWeight || "700"};
    padding-left: 10px;
    font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
    letter-spacing: ${(props) =>
      props.theme.style.contactFloatingForm.letterSpacing
        ? props.theme.style.contactFloatingForm.letterSpacing
        : "0"};
  }
`;

export const Input = styled.input`
  border: ${(props) =>
    props.theme.style.contactFloatingForm.inputBorder
      ? props.theme.style.contactFloatingForm.inputBorder
      : "none"};
  border-radius: 2px;
  color: ${(props) =>
    props.theme.style.contactFloatingForm.inputColor || "#3f5aa6"};
  height: 40px;
  width: 100%;
  padding-left: 10px;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
  font-size: ${(props) =>
    props.theme.style.contactFloatingForm.inputFontSize || "14px"};
`;

export const Button = styled.button`
  border: none;
  background-color: ${(props) =>
    props.theme.style.contactFloatingForm.buttonBackgroundMobile};
  border-radius: 2px;
  color: ${(props) => props.theme.style.contactFloatingForm.buttonColorMobile};
  width: auto;
  padding: 8px 20px;
  text-transform: uppercase;
  font-weight: 600;
  margin: auto;
  display: block;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
`;

export const IconClose = styled.div`
  cursor: pointer;
  width: 24px;
  top: 15px;
  right: 15px;
  height: 24px;
  position: absolute;
  stroke: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
  svg {
    width: 40px;
    height: 40px;
  }
`;

export const CustomCheckContainer = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  margin: 10px 0px;
  width: 100%;
  justify-content: ${(props) => (props.center ? "center" : "flex-start")};
`;

export const Label = styled.div`
  border: 1px solid rgba(0, 0, 0, 0);
  color: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
  font-size: ${(props) =>
    props.mobile || props.isOpportunitiesPage
      ? props.theme.style.checkbox.fontSizeMobile
      : props.theme.style.checkbox.fontSizeDesktop};
  line-height: ${(props) => props.theme.style.checkbox.lineHeight};
  font-weight: ${(props) => props.theme.style.checkbox.fontWeight};
  font-family: ${(props) => props.theme.style.checkbox.fontFamily};
  letter-spacing: ${(props) => props.theme.style.checkbox.letterSpacing};
  text-align: left;
  margin-left: 18px;
  cursor: pointer;
  font-family: ${(props) => props.theme.style.contactFloatingForm.fontFamily};
  small {
    font-size: 14px;
  }
`;

export const CustomCheckbox = styled.input`
  position: absolute;
  left: 0;
  z-index: 0;
  opacity: 0;
  width: 20px;
  height: 20px;
  cursor: pointer;
`;

export const CheckContainer = styled.div`
  fill: ${(props) => props.theme.style.contactFloatingForm.colorMobile};
  stroke: ${(props) =>
    props.isChecked
      ? props.theme.style.contactFloatingForm.colorMobile
      : props.theme.style.contactFloatingForm.colorMobile};
`;
