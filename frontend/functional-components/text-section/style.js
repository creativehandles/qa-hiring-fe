import styled from "styled-components";
import { mobileBreakpoint } from "../../constants/appConstants";

export const ButtonDefaultContainer = styled.div`
  margin: ${(props) =>
    props.marginButton ? props.marginButton : "0px auto 100px"};
  padding: ${(props) => (props.paddingButton ? props.paddingButton : 0)};
  display: flex;
`;

export const Container = styled.div`
  background-color: ${(props) => props.background};
`;
export const Header = styled.h4`
  color: ${(props) => props.theme.style.textComponent.titleColor};
  font-family: ${(props) => props.theme.style.textComponent.titleFontFamily};
  margin-bottom: 25px;
  font-weight: ${(props) => props.theme.style.textComponent.titleWeight};
  font-size: ${(props) =>
    props.screenWidth > mobileBreakpoint
      ? props.theme.style.textComponent.titleSize
      : props.theme.style.textComponent.titleFontSizeMobile};
  text-align: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "left" : "center"};
  text-transform: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "none" : "uppercase"};
  letter-spacing: ${(props) => props.theme.style.textComponent.letterSpacing};
  position: relative;

  &::after {
    content: "${(props) => props.afterClass}";
    color: #e00700;
    position: absolute;
    bottom: ${(props) => (props.afterClass ? "-8px" : "6px")};

    @media (max-width: 850px) {
      bottom: ${(props) => (props.afterClass ? "-5.5px" : "3px")};
    }
  }
`;
export const TextContainer = styled.div`
  max-width: 100%;
  order: ${(props) => (props.reverse === "true" ? "-1" : "")};
`;
export const Text = styled.div`
  font-family: ${(props) => props.theme.style.textComponent.fontFamily};
  color: ${(props) =>
    props.screenWidth < mobileBreakpoint
      ? props.theme.style.textComponent.fontColorMobile
      : props.theme.style.textComponent.fontColor};
  font-size: ${(props) =>
    props.screenWidth > mobileBreakpoint
      ? props.theme.style.textComponent.fontSize
      : props.theme.style.textComponent.fontSizeMobile};
  line-height: ${(props) =>
    props.screenWidth > mobileBreakpoint
      ? props.theme.style.textComponent.lineHeight
      : props.theme.style.textComponent.lineHeightMobile};
  text-align: left;
  letter-spacing: ${(props) => props.theme.style.textComponent.letterSpacing};
  display: flex;
  flex-wrap: wrap;
  white-space: pre-line;

  @media (max-width: 850px) {
    text-align: ${(props) =>
      props.theme.style.textComponent.textAlign
        ? props.theme.style.textComponent.textAlign
        : "center"};
  }

  b {
    font-family: ${(props) => props.theme.style.textComponent.titleFontFamily};
  }

  ul {
    li {
      font-family: ${(props) => props.theme.style.textComponent.fontFamilyList};
      text-align: left;
    }
  }
`;

export const UnderImageText = styled(Text)`
  font-size: 20px;
  line-height: 30px;
  text-align: center;
  display: block;
  @media (max-width: 850px) {
    font-size: 16px;
  }
`;

export const UnderImageTextBold = styled(Text)`
  font-weight: 700;
  font-size: 20px;
  line-height: 30px;
  text-align: center;
  display: block;
  @media (max-width: 850px) {
    font-size: 16px;
  }
`;

export const ContentContainer = styled.div`
  align-items: center;
  padding: 80px 20px;
  grid-gap: ${(props) =>
    props.screenWidth < mobileBreakpoint ? "0px 50px" : "0px 100px"};
  margin: 0 auto;
  max-width: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "1160px" : "376px"};
  min-height: ${(props) =>
    props.screenWidth < mobileBreakpoint ? "220px" : "350px"};
  @media (max-width: 850px) {
    margin: ${(props) =>
      props.reverse === "true" ? "40px auto 0 auto" : "0 auto"};
    padding: ${(props) =>
      props.containerContentFullWidth ? "20px 0px" : "20px 50px"};
  }
  @media (max-width: 850px) and (min-width: 500px) {
    max-width: 700px;
  }
  @media (max-width: 1400px) and (min-width: 850px) {
    padding: 80px 65px;
  }

  display: ${(props) => (props.display ? props.display : "grid")};
  justify-content: space-between;
  grid-template-columns: ${(props) =>
    props.screenWidth > mobileBreakpoint ? props.ratio : "1fr"};
  grid-template-rows: ${(props) =>
    props.screenWidth > mobileBreakpoint ? " 1fr " : props.ratio};
`;
export const OpportunitiesContainer = styled.div`
  padding: 0;
  display: grid;
  max-width: 60%;
  margin: 0 auto;
  padding: 20px 0px;
  margin-top: 80px;
  grid-auto-flow: row;
`;

export const OpportunitiesText = styled(Text)`
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 16px;
  line-height: 30px;
  text-align: center;
  display: flex;
  flex-wrap: wrap;
`;

export const RevertOppotrunitiesContainer = styled(OpportunitiesContainer)`
  max-width: 100%;
  padding: 0;
  margin-top: 0;
  border-radius: 5px;
  margin-top: 20px;
  padding: 50px 40px;
  grid-template-columns: 45% 50%;
  gap: 32px;
  background: ${(props) =>
    props.theme.style.mainOpportunities.textSectionBackground};
  border-radius: ${(props) =>
    props.theme.style.mainOpportunities.textSectionBorderRadius};
  border: ${(props) => props.theme.style.mainOpportunities.textSectionBorder};
`;

export const DescriptionsContainer = styled.div`
  margin: 0 auto;
  max-width: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "1160px" : "376px"};
  min-height: ${(props) =>
    props.screenWidth < mobileBreakpoint ? "220px" : "350px"};
  padding: ${(props) =>
    props.screenWidth > mobileBreakpoint
      ? "40px 20px"
      : props.textOnly
      ? "40px 30px"
      : "40px 50px"};
  @media (max-width: 850px) and (min-width: 500px) {
    max-width: 700px;
  }

  @media (max-width: 1400px) and (min-width: 850px) {
    padding: 40px 65px;
  }
  display: flex;
  flex-direction: column;
  justify-content: ${(props) => (props.textOnly ? "center" : "inherit")};
`;
export const DescriptionItemsContainer = styled.div`
  display: grid;
  padding: ${(props) =>
    props.descriptionContainerPadding
      ? props.descriptionContainerPadding
      : "40px 0"};
  grid-template-columns: ${(props) => props.gridTemplateColumns};
  grid-gap: ${(props) => props.gridGap};
  @media (max-width: 850px) {
    grid-template-columns: 1fr;
  }
`;
export const DescriptionItemContainer = styled.div`
  display: flex;
  justify-content: center;
`;
export const DescriptionItem = styled.div`
  display: flex;
  width: ${(props) => props.itemWidth};
  align-items: center;
  @media (max-width: 850px) {
    width: 100%;
  }
`;

export const DescriptionSubHeader = styled.div`
  color: ${(props) => props.theme.style.textComponent.fontColor};
  font-family: ${(props) =>
    props.theme.style.textComponent.descriptionfontFamily};
  display: flex;
  align-items: center;
  margin: 0 auto;
  font-size: ${(props) =>
    props.screenWidth > mobileBreakpoint
      ? props.theme.style.textComponent.descriptionfontSize
      : "14px"};
  line-height: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "24px" : "24px"};
  padding-top: 80px;
  padding-bottom: 40px;

  @media (max-width: 850px) {
    width: 100%;
    padding-top: 40px;
  }
`;

export const DescriptionHeaderD = styled(Header)`
  color: ${(props) =>
    props.theme.style.textComponent.colorDiffText
        ? props.theme.style.textComponent.colorDiffText
        : props.theme.style.textComponent.titleColor};
  text-align: ${(props) =>
    props.textOnly
        ? "left"
        : props.theme.style.textComponent.descriptionHeaderAlign
        ? props.theme.style.textComponent.descriptionHeaderAlign
        : "center"};
  margin-bottom: ${(props) =>
    props.textOnly
        ? "20px"
        : props.theme.style.textComponent.descriptionHeaderMarginBottom
        ? props.theme.style.textComponent.descriptionHeaderMarginBottom
        : "57px"};
  line-height: 1.2;
  font-size: ${(props) =>
    props.textOnly
        ? "32px"
        : props.screenWidth > mobileBreakpoint
        ? props.theme.style.textComponent.titleSize
        : props.theme.style.textComponent.titleFontSizeMobile};
  position: relative;
  padding-top: ${(props) => (props.headerOnly ? "50px" : "")};
  padding-left: ${(props) => (props.headerOnly ? "50px" : "")};
  padding-right: ${(props) => (props.headerOnly ? "50px" : "")};
  @media (max-width: 850px) {
    margin-bottom: ${(props) =>
    props.theme.style.textComponent.descriptionHeaderMarginBottomMobile
        ? props.theme.style.textComponent.descriptionHeaderMarginBottomMobile
        : "50px"};
    font-size: ${(props) => (props.textOnly ? "20px" : "18px")};
  }
`;
export const DescriptionTextD = styled.div`
  color: ${(props) => props.theme.style.textComponent.colorDiffText};
  font-family: ${(props) =>
    props.theme.style.textComponent.descriptionfontFamily};
  width: 100%;
  font-size: ${(props) =>
    props.screenWidth > mobileBreakpoint
        ? props.theme.style.textComponent.descriptionfontSize
        : "14px"};
  line-height: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "30px" : "24px"};
  text-align: left;
`;

export const DescriptionHeader = styled(Header)`
  color: ${(props) =>
    props.theme.style.textComponent.descriptionTitleColor
      ? props.theme.style.textComponent.descriptionTitleColor
      : props.theme.style.textComponent.titleColor};
  text-align: ${(props) =>
    props.textOnly
      ? "left"
      : props.theme.style.textComponent.descriptionHeaderAlign
      ? props.theme.style.textComponent.descriptionHeaderAlign
      : "center"};
  margin-bottom: ${(props) =>
    props.textOnly
      ? "20px"
      : props.theme.style.textComponent.descriptionHeaderMarginBottom
      ? props.theme.style.textComponent.descriptionHeaderMarginBottom
      : "57px"};
  line-height: 1.2;
  font-size: ${(props) =>
    props.textOnly
      ? "32px"
      : props.screenWidth > mobileBreakpoint
      ? props.theme.style.textComponent.titleSize
      : props.theme.style.textComponent.titleFontSizeMobile};
  position: relative;
  padding-top: ${(props) => (props.headerOnly ? "50px" : "")};
  padding-left: ${(props) => (props.headerOnly ? "50px" : "")};
  padding-right: ${(props) => (props.headerOnly ? "50px" : "")};
  @media (max-width: 850px) {
    margin-bottom: ${(props) =>
      props.theme.style.textComponent.descriptionHeaderMarginBottomMobile
        ? props.theme.style.textComponent.descriptionHeaderMarginBottomMobile
        : "50px"};
    font-size: ${(props) => (props.textOnly ? "20px" : "18px")};
  }
`;
export const DescriptionText = styled.div`
  color: ${(props) => props.theme.style.textComponent.fontColor};
  font-family: ${(props) =>
    props.theme.style.textComponent.descriptionfontFamily};
  width: 100%;
  font-size: ${(props) =>
    props.screenWidth > mobileBreakpoint
      ? props.theme.style.textComponent.descriptionfontSize
      : "14px"};
  line-height: ${(props) =>
    props.screenWidth > mobileBreakpoint ? "30px" : "24px"};
  text-align: left;
`;
export const DescriptionLogo = styled.img`
  margin-right: 20px;
  width: auto;
  height: auto;
`;

export const ColumnsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  margin: 0 auto;
  align-items: flex-start;
  grid-gap: ${(props) =>
    props.theme.style.textComponent.columnsContainerGridGap || "80px"};
  width: 100%;
  width: 100%;
  min-height: ${(props) =>
    props.screenWidth < mobileBreakpoint ? "220px" : "350px"};

  @media (max-width: 850px) {
    grid-template-rows: 1fr 1fr 1fr;
    grid-template-columns: 1fr;
  }
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ColumnHeader = styled(Header)`
  font-family: ${(props) =>
    props.theme.style.textComponent.columnHeaderFontFamily};
  letter-spacing: ${(props) => props.theme.style.textComponent.letterSpacing};
  line-height: ${(props) => props.theme.style.textComponent.titleLineHeight};
  margin-bottom: 25px;
  font-size: ${(props) =>
    props.theme.style.textComponent.columnHeaderFontSize || "30px"};
  color: ${(props) =>
    props.theme.style.textComponent.columnHeaderFontColor || "inherit"};
  text-align: inherit;
  text-transform: inherit;
  @media (max-width: 850px) {
    font-size: ${(props) =>
      props.theme.style.textComponent.columnHeaderFontSizeMobile || "18px"};
  }
`;
export const MainImageContainer = styled(ContentContainer)`
  grid-gap: 0px 50px;
  align-items: center;
  justify-items: center;
  padding: ${(props) =>
    props.theme.style.textComponent.mainImageContainerPadding
      ? props.theme.style.textComponent.mainImageContainerPadding
      : ""};
  min-height: ${(props) =>
    props.theme.style.textComponent.mainImageContainerMinHeight
      ? props.theme.style.textComponent.mainImageContainerMinHeight
      : ""};
`;
export const MainImageHeader = styled(DescriptionHeader)`
  font-size: ${(props) => props.theme.style.textComponent.mainImageTextSize};
  font-family: ${(props) =>
    props.theme.style.textComponent.mainImageFontFamily};
  text-align: ${(props) =>
    props.theme.style.textComponent.mainImageHeaderTextAlign
      ? props.theme.style.textComponent.mainImageHeaderTextAlign
      : ""};
  font-weight: ${(props) =>
    props.theme.style.textComponent.mainImageTextFontWeight};
  @media (max-width: 850px) {
    font-size: 28px;
  }
  margin-top: 25px;
  display: flex;
  flex-direction: row-reverse;
  align-items: flex-end;
  justify-content: flex-end;

  @media (min-width: 850px) {
    &::before {
      content: "${(props) => props.theme.style.upload.afterClass}";
      color: #e00700;
      margin-bottom: -10px;
    }
  }
`;
export const MainImage = styled.img`
  width: ${(props) =>
    props.theme.style.textComponent.mainImageWidth
      ? props.theme.style.textComponent.mainImageWidth
      : "100px"};
  height: 100px;
`;
