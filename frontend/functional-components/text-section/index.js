import React, { useEffect, useRef, useState } from "react";
import { Gallery } from "../gallery";
import {
  Container,
  TextContainer,
  ButtonDefaultContainer,
  Header,
  Text,
  ContentContainer,
  OpportunitiesContainer,
  OpportunitiesText,
  RevertOppotrunitiesContainer,
  DescriptionsContainer,
  DescriptionItem,
  DescriptionText,
  DescriptionTextD,
  DescriptionItemsContainer,
  DescriptionItemContainer,
  DescriptionHeader,
  DescriptionHeaderD,
  DescriptionLogo,
  Column,
  ColumnsContainer,
  ColumnHeader,
  MainImageHeader,
  MainImage,
  MainImageContainer,
  DescriptionSubHeader
} from "./style";
import { DefaultOpportunitiesButton } from "../default-opportunities-button";
import useTranslation from "../../hooks/useTranslation";

// Note, for header and text, we take the translatable key from the .config file
export const TextSection = ({
  header,
  text,
  height,
  background,
  images,
  screenWidth,
  reverse,
  opportunities,
  descriptions,
  ratio,
  columns,
  mainImage,
  buttonDefaultText,
  gridTemplateColumns,
  gridGap,
  marginButton,
  paddingButton,
  itemWidth,
  textOnly,
  textDiff,
  headerOnly,
  headerIcon,
  columnsMainTitle,
  isSubHeader,
  subheader,
  display,
  descriptionContainerPadding,
  afterClass,
  ...props
}) => {
  const target = useRef({});
  const [childrens, setChildrens] = useState();
  const [currentHtml, setCurrentHtml] = useState();
  const [iframe, setIframe] = useState();
  const { t } = useTranslation();

  const __html = `${currentHtml}`;
  useEffect(() => {
    setChildrens(target.current.children.length);
    if (DOMParser) {
      const newDom = new DOMParser()
        .parseFromString(t(text), "text/html")
        .querySelector("body");
      const iframe = newDom.querySelector("iframe");
      if (iframe) setIframe(iframe);
      if (iframe) {
        const parent = iframe.parentNode;
        parent.removeChild(iframe);
        setCurrentHtml(newDom.outerHTML);
      } else setCurrentHtml(newDom.outerHTML);
    }
  }, []);

  if (buttonDefaultText) {
    return (
      <Container background={background} ref={target}>
        <ButtonDefaultContainer
          marginButton={marginButton}
          paddingButton={paddingButton}
        >
          <DefaultOpportunitiesButton screenWidth={screenWidth} />
        </ButtonDefaultContainer>
      </Container>
    );
  }
  if (mainImage) {
    return (
      <Container background={background} ref={target}>
        <MainImageContainer screenWidth={screenWidth}>
          <MainImage src={mainImage} />
          <MainImageHeader
            afterClass={afterClass}
            childrens={childrens}
            screenWidth={screenWidth}
          >
            {t(header)}
          </MainImageHeader>
        </MainImageContainer>
      </Container>
    );
  }
  if (columns) {
    return (
      <Container background={background} ref={target}>
        <DescriptionsContainer screenWidth={screenWidth} textOnly>
          {columnsMainTitle && (
            <DescriptionHeader screenWidth={screenWidth} textOnly>
              {columnsMainTitle}
            </DescriptionHeader>
          )}
          <ColumnsContainer screenWidth={screenWidth}>
            {columns.map((el, index) => {
              return (
                <Column key={index}>
                  <ColumnHeader childrens={childrens} screenWidth={screenWidth}>
                    {t(el.header)}
                  </ColumnHeader>
                  <Text
                    screenWidth={screenWidth}
                    dangerouslySetInnerHTML={{ __html: t(el.text) }}
                  />
                </Column>
              );
            })}
          </ColumnsContainer>
        </DescriptionsContainer>
      </Container>
    );
  }
  if (textOnly) {
    return (
      <Container background={background} ref={target}>
        <DescriptionsContainer textOnly screenWidth={screenWidth}>
          <DescriptionHeader screenWidth={screenWidth} textOnly>
            {header}
          </DescriptionHeader>
          <DescriptionText screenWidth={screenWidth} textOnly>
            {text}
          </DescriptionText>
        </DescriptionsContainer>
      </Container>
    );
  }

  if (textDiff) {
    return (
        <Container background={background} ref={target}>
          <DescriptionsContainer textOnly screenWidth={screenWidth}>
            <DescriptionHeaderD screenWidth={screenWidth} textOnly>
              {header}
            </DescriptionHeaderD>
            <DescriptionTextD screenWidth={screenWidth} textOnly>
              {text}
            </DescriptionTextD>
          </DescriptionsContainer>
        </Container>
    );
  }

  if (headerOnly) {
    return (
      <Container background={background} ref={target}>
        <DescriptionHeader screenWidth={screenWidth} headerOnly>
          {header}
        </DescriptionHeader>
      </Container>
    );
  }
  if (descriptions) {
    return (
      <Container background={background} ref={target}>
        <DescriptionsContainer screenWidth={screenWidth}>
          <DescriptionHeader childrens={childrens} screenWidth={screenWidth}>
            {t(header)}
          </DescriptionHeader>
          {isSubHeader && (
            <DescriptionSubHeader
              childrens={childrens}
              screenWidth={screenWidth}
            >
              {headerIcon !== "" ? <DescriptionLogo src={headerIcon} /> : ""}
              <span>{t(subheader)}</span>
            </DescriptionSubHeader>
          )}
          <DescriptionItemsContainer
            screenWidth={screenWidth}
            gridTemplateColumns={gridTemplateColumns}
            gridGap={gridGap}
            descriptionContainerPadding={descriptionContainerPadding}
          >
            {descriptions.map((el) => {
              return (
                <DescriptionItemContainer key={t(el.text)}>
                  <DescriptionItem itemWidth={itemWidth}>
                    <DescriptionLogo src={el.icon} />
                    <DescriptionText
                      screenWidth={screenWidth}
                      dangerouslySetInnerHTML={{ __html: t(el.text) }}
                    />
                  </DescriptionItem>
                </DescriptionItemContainer>
              );
            })}
          </DescriptionItemsContainer>
        </DescriptionsContainer>
      </Container>
    );
  }
  if (opportunities && reverse) {
    return (
      <RevertOppotrunitiesContainer ref={target}>
        <TextContainer>
          <Header
            childrens={childrens}
            screenWidth={screenWidth}
            afterClass={afterClass}
          >
            {t(header)}
          </Header>
          <OpportunitiesText
            dangerouslySetInnerHTML={{ __html }}
          ></OpportunitiesText>
        </TextContainer>
        {iframe && (
          <Gallery
            autoHeight
            screenWidth={screenWidth}
            childrens={childrens}
            iframe={iframe.outerHTML}
          />
        )}
        {images && (
          <Gallery
            autoHeight
            childrens={childrens}
            images={images}
            screenWidth={screenWidth}
          />
        )}
      </RevertOppotrunitiesContainer>
    );
  }

  if (opportunities) {
    return (
      <Container background={background} ref={target}>
        <OpportunitiesContainer>
          {iframe && (
            <Gallery
              autoHeight
              screenWidth={screenWidth}
              childrens={childrens}
              iframe={iframe.outerHTML}
            />
          )}
          {images && (
            <Gallery
              autoHeight
              childrens={childrens}
              images={images}
              screenWidth={screenWidth}
            />
          )}
          <TextContainer>
            <Header
              childrens={childrens}
              screenWidth={screenWidth}
              afterClass={afterClass}
            >
              {t(header)}
            </Header>
            <OpportunitiesText
              dangerouslySetInnerHTML={{ __html }}
            ></OpportunitiesText>
          </TextContainer>
        </OpportunitiesContainer>
      </Container>
    );
  }

  return (
    <Container {...props} background={background}>
      <ContentContainer
        {...props}
        reverse={reverse}
        childrens={childrens}
        ref={target}
        ratio={ratio}
        screenWidth={screenWidth}
        textOnly
        display={display}
      >
        {iframe && (
          <Gallery
            height={height}
            screenWidth={screenWidth}
            childrens={childrens}
            iframe={iframe.outerHTML}
          />
        )}
        {images && (
          <Gallery
            height={height}
            childrens={childrens}
            images={images}
            screenWidth={screenWidth}
          />
        )}
        {text && (
          <TextContainer
            reverse={reverse}
            childrens={childrens}
            screenWidth={screenWidth}
          >
            <Header
              childrens={childrens}
              screenWidth={screenWidth}
              afterClass={afterClass}
            >
              {t(header)}
            </Header>
            <Text
              childrens={childrens}
              screenWidth={screenWidth}
              dangerouslySetInnerHTML={{ __html }}
            />
          </TextContainer>
        )}
      </ContentContainer>
    </Container>
  );
};
