import React, { useState } from "react";
import { Container, Title, GDPR } from "./style";
import { Button } from "../../components/common/button";
import { GdrAgreement } from "../../components/common/gdr-agreement";
import { Ring } from "../svg-component";

export const ContactEmail = () => {
  const [isCheked, setIsChecked] = useState(true);

  return (
    <Container>
      <Ring style={{ color: "#ebb274", stroke: "#ebb274", fill: "#ebb274" }} />
      <Title>
        Můžeme vám poslat e -mail, když se nabídka shoduje s vaším životopisem a
        vybranými filtry.
      </Title>
      <Button title="VYTVOŘTE UPOZORNĚNÍ NA PRÁCI" />
      <GDPR>
        <GdrAgreement
          label=" Zasílejte mi novinky o LOGU"
          isChecked={isCheked}
          setIsChecked={setIsChecked}
        />
      </GDPR>
    </Container>
  );
};
