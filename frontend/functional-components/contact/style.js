import styled from "styled-components";

export const Container = styled.div`
  border: 1px solid #ebeef7;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  padding: 30px 20px;
  max-width: 100%;
  margin: 0 auto;
  justify-content: center;
  align-items: center;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
`;
export const Title = styled.div`
  text-align: center;
  margin: 30px 0px;
  color: ${(props) => props.theme.style.global.fontColor};
`;

export const GDPR = styled.div`
  text-align: center;
  width: 80%;
  margin: 0px auto;
  margin-top: 30px;
`;
