import React, { useState } from "react";
import styled from "styled-components";
import { Search } from "../../../functional-components/svg-component";
import { useDispatch, useSelector } from "react-redux";
import { setQuery } from "../../../redux/slices/query";
import { fetchVacanciesForCandidate } from "../../../redux/slices/vacancies/open-vacancies/functions/search-for-candidate";
import { getAPIURL } from "../../../utils/api-url/index";
import { fetchDefaultVacancies } from "../../../redux/slices/vacancies/default/functions";
import ConfigStyles from "../../../utils/config-styles";

export default function Query() {
  const dispatch = useDispatch();
  const queryState = useSelector((state) => state.query);
  const candidate = useSelector((state) => state.candidate.value);
  const [queryInput, setQueryInput] = useState(queryState.query);
  const localStyles = ConfigStyles()
  const onQueryInputChange = (e) => {
    setQueryInput(e.target.value);
  };

  const onExecuteSearch = () => {
    dispatch(setQuery({ query: queryInput }));
    dispatch(fetchDefaultVacancies({ apiURL: getAPIURL() }));
    if (candidate.id) {
      dispatch(fetchVacanciesForCandidate({ apiURL: getAPIURL() }));
    }
  };

  return (
    <QueryContainer>
      <QueryInput value={queryInput} onChange={onQueryInputChange} />
      <SearchIconContainer onClick={onExecuteSearch}>
        <Search color={localStyles.styles.global.SearchIconColor} />
      </SearchIconContainer>
    </QueryContainer>
  );
}

const QueryContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 234px;
  height: 35px;
  box-shadow: 0 10px 20px rgba(27, 48, 110, 0.05);
  border-radius: 25px;
  border: 1px solid #ebeef7;
  background-color: #ffffff;
`;

const QueryInput = styled.input`
  flex: 1 0px;
  margin-left: 13px;
  border: none;
  height: 32px;
  outline: none;
`;

const SearchIconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 5px;
  width: 30px;

  &:hover {
    cursor: pointer;
  }
`;
