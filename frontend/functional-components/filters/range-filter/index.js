import React from "react";
import styled from "styled-components";
import MultiRange from "../../../components/common/multi-ranger";
import MultiRangeFixedValues from "../../../components/common/multi-ranger-fixed-values";

export default function RangeFilter({
  title,
  onChange,
  data,
  isFixedValues,
  fixedValues,
  addMargin,
}) {
  if (isFixedValues && !fixedValues) {
    throw new Error("Fixed Values must be defined when isFixedValues = true");
  }

  return (
    <Container key={title} addMargin={addMargin}>
      <Title>{title}</Title>
      <RangeContainer>
        {!isFixedValues && <MultiRange data={data} onChange={onChange} />}
        {isFixedValues && (
          <MultiRangeFixedValues
            data={data}
            onChange={onChange}
            fixedValues={fixedValues}
          />
        )}
      </RangeContainer>
    </Container>
  );
}

const Container = styled.div`
  width: 340px;
  margin-bottom: 15px;
  border: ${(props) =>
    props.isCurrent
      ? props.theme.style.list.activeBorder
      : props.theme.style.list.border};
  margin-right: ${(props) => (props.addMargin ? "20px" : "0")};
  background-color: ${(props) =>
    props.theme.style.filters.rangeFilter.background};
  padding: 20px 20px 15px 20px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  cursor: pointer;
  border-radius: ${(props) => props.theme.style.list.borderRadius};
`;

const Title = styled.div`
  text-transform: uppercase;
  font-family: ${(props) => props.theme.style.list.titleFontFamily};
  font-weight: ${(props) => props.theme.style.list.titleWeight};
  font-size: ${(props) => props.theme.style.list.titleSize};
  color: ${(props) => props.theme.style.list.titleColor};
  line-height: ${(props) => props.theme.style.list.titleLineHeight};
  display: flex;
  align-items: center;
`;

const RangeContainer = styled.div`
  width: 100%;
  margin-top: 30px;
`;
