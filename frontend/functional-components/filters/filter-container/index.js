import React, { useReducer, useState } from "react";
import styled from "styled-components";
import { ArrowDropdown } from "../../svg-component";
import RangeFilter from "../range-filter";
import { useDispatch } from "react-redux";
import { setFilters } from "../../../redux/slices/filters";
import { useSelector } from "react-redux";
import { postingAges } from "../../../constants/postingAges";
import Query from "../query";
import { SalaryFilter } from "../../salary-filter";
import { JobFilter } from "../../job-filter";
import ConfigStyles from "../../../utils/config-styles";

// Note: this will be used inside of the main store's redux slice as initial state
export const initialFilterState = {
  scheduleMin: 0,
  scheduleMax: 100,
  postingAgeMin: postingAges[0].label,
  postingAgeMax: postingAges[postingAges.length - 1].label,
};

function reducer(state, action) {
  switch (action.type) {
    case "setSchedule":
      return {
        ...state,
        scheduleMin: action.payload.scheduleMin,
        scheduleMax: action.payload.scheduleMax,
      };
    case "setPostingAge":
      return {
        ...state,
        postingAgeMin: action.payload.postingAgeMin,
        postingAgeMax: action.payload.postingAgeMax,
      };
    default:
      throw new Error("Invalid action type");
  }
}

export default function FilterContainer() {
  const filterState = useSelector((state) => state.filters);
  const [state, dispatch] = useReducer(reducer, filterState);
  const dispatchToRedux = useDispatch();
  const [showFilters, setShowFilters] = useState(false);

  const onChangeSchedule = ([min, max]) => {
    dispatch({
      type: "setSchedule",
      payload: { scheduleMin: min, scheduleMax: max },
    });
  };

  const onChangePostingAge = ([min, max]) => {
    dispatch({
      type: "setPostingAge",
      payload: { postingAgeMin: min, postingAgeMax: max },
    });
  };
  // Once Apply Filters button is pressed, the filters are saved into the main
  // redux store
  const renderFilters = () => {
    return (
      <FilterContentsContainer visible={showFilters}>
        <FilterContents>
          <JobSalaryContainer>
          <SalaryFilter />
          <JobFilter />
          </JobSalaryContainer>
          <RangeFilterContainer>
          <RangeFilter
            data={[state.scheduleMin, state.scheduleMax]}
            title="SCHEDULE"
            onChange={onChangeSchedule}
            isFixedValues={false}
            addMargin
          />
          <RangeFilter
            data={[state.postingAgeMin, state.postingAgeMax]}
            title="POSTING AGE"
            onChange={onChangePostingAge}
            isFixedValues
            fixedValues={postingAges.map((item) => item.label)}
          />
          </RangeFilterContainer>
        </FilterContents>
        <FilterFooter>
          <ViewButton onClick={() => dispatchToRedux(setFilters(state))}>
            Apply Filters
          </ViewButton>
        </FilterFooter>
      </FilterContentsContainer>
    );
  };

  return (
    <OuterContainer>
      <Container>
        <Header>
          <Title>
            Manual Filters
            <DropdownButton onClick={() => setShowFilters(!showFilters)}>
              <Dropdown invert={!!showFilters}>
                <ArrowDropdown></ArrowDropdown>
              </Dropdown>
            </DropdownButton>
          </Title>
          <Query />
        </Header>
        {renderFilters()}
      </Container>
    </OuterContainer>
  );
}

const OuterContainer = styled.div`
  width: 100%;
  display: flex;
  flex-shrink: 0;
  justify-content: center;
  align-items: center;
  background: ${(props) => props.theme.style.list.background};
  
`;
const JobSalaryContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  
`;
const RangeFilterContainer = styled.div`
display: ${(p) => p.theme.style.filters.rangeContainer.display || 'flex'};
justify-item: flex-end;
`;

const Container = styled.div`
  width: 100%;
  max-width: 1160px;
  padding: 0 20px 0px 20px;
  @media (max-width: 1400px) and (min-width: 850px) {
    padding: 0 60px 0px 60px;
  }
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 0 20px 0;
`;

const Title = styled.div`
  font-size: ${(props) => props.theme.style.filters.filterContainer.fontSize};
  font-family: ${(props) =>
    props.theme.style.filters.filterContainer.fontFamily};
  font-weight: ${(props) =>
    props.theme.style.filters.filterContainer.fontWeight};
  color: ${(props) => props.theme.style.filters.filterContainer.fontColor};
  border: ${(props) => props.theme.style.filters.filterContainer.borderButtonFilter};
  padding:  ${(props) => props.theme.style.filters.filterContainer.paddingButtonFilter};
  border-radius: 5px;
  box-shadow: ${(props) => props.theme.style.global.headerBoxShadow};
`;

const DropdownButton = styled.button`
  height: 44px;
  width: 40px;
  background: none;
  border: none;
`;

export const Dropdown = styled.span`
  svg {
    stroke: ${(props) => props.theme.style.filters.filterContainer.fontColor};
    fill: ${(props) => props.theme.style.filters.filterContainer.fontColor};
  }
  margin: 0 0 0 5px;
  svg {
    transform: ${(props) => (props.invert ? "rotate(180deg)" : "rotate(0deg)")};
    transition: transform 0.5s linear;
  }
`;

const FilterContents = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  grid-column-gap: 20px;
  margin-bottom: 20px;
`;

const FilterContentsContainer = styled.div`
  height: ${(p) =>
    p.visible ? p.theme.style.filters.fullHeightfilter ? "420px": "210px" : "0"}; /* Need to set height manually */
  overflow: hidden;
  transition: all 0.5s ease-in-out;
`;

const FilterFooter = styled.div`
  margin-bottom: 20px;
`;

export const ViewButton = styled.button`
  padding: 10px;
  border-radius: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonRadius
      : props.theme.style.upload.seeAllButtonRadius};
  border: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonBorder
      : props.theme.style.upload.seeAllButtonBorder};
  background-color: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonBackground
      : props.theme.style.upload.seeAllButtonBackground};
  outline: none;
  cursor: pointer;
  display: block;
  font-family: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonFontFamily
      : props.theme.style.upload.seeAllButtonFontFamily};
  font-size: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonSize
      : props.theme.style.upload.seeAllButtonSize};
  font-weight: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonWeight
      : props.theme.style.upload.seeAllButtonWeight};
  color: ${(props) =>
    props.fill
      ? props.theme.style.upload.uploadButtonColor
      : props.theme.style.upload.seeAllButtonColor};
  margin-right: 20px;
  & :hover {
    background-color: ${(props) =>props.theme.style.upload.hoverBackgroundColor};
    color: ${(props) =>props.theme.style.upload.hoverTextCollor};
    transition: all 0.3s;
  }
`;
