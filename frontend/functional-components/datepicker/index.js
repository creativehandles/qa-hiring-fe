import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Next, Prev } from "../svg-component";

const date = new Date();
const CURRENT_YEAR = date.getFullYear();
const CURRENT_MONTH = date.getMonth();
const CURRENT_DAY = date.getDate();
const MONTH_NAMES = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const WEEKS = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];

export const DatePicker = ({ setUserDate, userDate }) => {
  const [year, setYear] = useState(
    userDate ? userDate.getFullYear() ?? CURRENT_YEAR : CURRENT_YEAR
  );
  const [month, setMonth] = useState(
    userDate ? userDate.getMonth() ?? CURRENT_MONTH : CURRENT_MONTH
  );
  const [days, setDays] = useState([]);
  const [previousMonthDays, setpreviousMontDays] = useState([]);
  const [nextMonthDays, setNextMontDays] = useState([]);
  const [day, setDay] = useState(
    userDate ? userDate.getDate() ?? CURRENT_DAY : CURRENT_DAY
  );
  const [value, setValue] = useState();

  const getDays = () => {
    const prev = new Date(year, month, 0).getDate();
    const next = new Date(year, month + 2, 0).getDate();
    const currentMonthDays = new Date(year, month + 1, 0).getDate();
    setDays(currentMonthDays);
    setpreviousMontDays(prev);
    setNextMontDays(next);
  };

  const returnDays = () => {
    const firstDayWeek = new Date(year, month, 1).getDay();
    const lastDayWeek = new Date(year, month, days).getDay();

    const newDays = [];
    let prevDays = [];
    let nextDays = [];
    for (let i = 1; i <= previousMonthDays; i++) {
      prevDays.push(i);
    }
    for (let i = 1; i <= nextMonthDays; i++) {
      nextDays.push(i);
    }

    if (firstDayWeek !== 0) {
      prevDays = prevDays.slice(
        prevDays.length - firstDayWeek,
        prevDays.length
      );
    } else {
      prevDays = [];
    }

    if (lastDayWeek !== 6) {
      nextDays = nextDays.slice(0, 7 - lastDayWeek - 1);
    } else {
      nextDays = [];
    }

    for (let i = 1; i <= days; i++) {
      newDays.push(i);
    }

    const unactiveFirstDays = prevDays.map((el, index) => (
      <UnactiveDayes key={"prev" + index}>{el}</UnactiveDayes>
    ));
    const unactiveNextDays = nextDays.map((el, index) => (
      <UnactiveDayes key={"next" + index}>{el}</UnactiveDayes>
    ));
    const activeDays = newDays.map((el, index) => {
      let currentDate = false;
      if (
        el === value.getDate() &&
        year === value.getFullYear() &&
        month === value.getMonth()
      ) {
        currentDate = true;
      }
      return (
        <DayItem
          key={"current" + index}
          current={currentDate}
          onClick={() => setDay(el)}
        >
          {el}
        </DayItem>
      );
    });
    const Calendar = [...unactiveFirstDays, ...activeDays, ...unactiveNextDays];
    return Calendar;
    // return newDays.map((el) => {
    //   let currentDate=false
    //   if(el===value.getDate()&&year===value.getFullYear()&&month===value.getMonth()){
    //     currentDate=true
    //   }
    //   return <DayItem current={currentDate} onClick={() => setDay(el)}>{el}</DayItem>;
    // });
  };

  const setNextMonth = () => {
    if (month === 11) {
      setMonth(0);
      setYear((state) => state + 1);
    } else {
      setMonth((state) => state + 1);
    }
  };
  const setPrevMonth = () => {
    if (month === 0) {
      setMonth(11);
      setYear((state) => state - 1);
    } else {
      setMonth((state) => state - 1);
    }
  };

  const setNextYear = () => {
    setYear((state) => state + 1);
  };
  const setPrevYear = () => {
    setYear((state) => state - 1);
  };
  //   const setCurrentMonth = (action) => {
  //     switch (action) {
  //       case "next":
  //         {
  //           if (month.month > 11) {
  //             setMonth({ ...month, month: 0, monthNmae: MONTH_NAMES[0] });
  //             setYear((state) => state + 1);
  //           } else {
  //             setMonth({
  //               ...month,
  //               month: month.month + 1,
  //               monthNmae: MONTH_NAMES[month.month + 2],
  //             });
  //           }
  //         }
  //         break;
  //       case "prev":
  //         {
  //           if (month.month < 0) {
  //             setMonth({ ...month, month: 11, monthNmae: MONTH_NAMES[11] });
  //             setYear((state) => state - 1);
  //           } else {
  //             setMonth({
  //               ...month,
  //               month: month.month + 1,
  //               monthNmae: MONTH_NAMES[month.month + 2],
  //             });
  //           }
  //         }
  //         break;
  //       default:
  //         return () => {};
  //     }
  //   };

  //   const setCurrentYear = (action) => {
  //     switch (action) {
  //       case "next":
  //         setYear((state) => state + 1);
  //         break;
  //       case "prev":
  //         setYear((state) => state - 1);
  //         break;
  //       default:
  //         return null;
  //     }
  //   };

  // const setCurrentDate = (action, target) => {
  //   if (target === "year") {
  //     if (action === "next") setYear((state) => state + 1);
  //     if (action === "prev") setYear((state) => state - 1);
  //   }
  //   if (target === "month") {
  //     if (action === "next")
  //       setMonth({
  //         ...month,
  //         month: month.month + 1,
  //         monthNmae: MONTH_NAMES[month.month + 2],
  //       });
  //     if (action === "prev")
  //       setMonth({
  //         ...month,
  //         month: month.month - 1,
  //         monthNmae: MONTH_NAMES[month.month - 2],
  //       });
  //   }
  // };

  useEffect(() => {
    getDays();
  }, [year, month]);

  useEffect(() => {
    setValue(new Date(year, month, day));
  }, [day]);

  useEffect(() => {
    setUserDate(value);
  }, [value]);

  // useEffect(()=>{
  //   setValue(userDate);
  // },[]);

  return (
    <>
      <Container>
        <TitleForYear>
          <Count onClick={() => setPrevYear()}>
            <Prev />
          </Count>
          {year}
          <Count onClick={() => setNextYear()}>
            <Next />
          </Count>
        </TitleForYear>
        <TitleForMount>
          <Count onClick={() => setPrevMonth()}>
            <Prev />
          </Count>
          {MONTH_NAMES[month]}
          <Count onClick={() => setNextMonth()}>
            <Next />
          </Count>
        </TitleForMount>
        <TitleForWeeks>
          {WEEKS.map((el, index) => (
            <WeekItem key={index}>{el}</WeekItem>
          ))}
        </TitleForWeeks>
        <DaysContainer>{returnDays()}</DaysContainer>
      </Container>
    </>
  );
};

const Container = styled.div`
  font-size: 11px;
  width: 250px;
  font-weight: 700;
  height: 250px;
  cursor: pointer;
  border-radius: 22px;
  border: 1px solid #ebeef7;
  box-shadow: 0 10px 30px rgba(27, 48, 110, 0.03);
  position: absolute;
  top: 80%;
  left: 24%;
  text-align: center;
  z-index: 10;
`;

const TitleForMount = styled.div`
  display: flex;
  background: ${(props) => props.theme.style.global.fontColor};
  justify-content: space-between;
  color: #fff;
  padding: 8px 30px;
  animation: pop 0.4s ease-in-out;
  @keyframes pop {
    0% {
      opacity: 0;
      transform: rotateX(0deg);
    }
    50% {
      opacity: 0.5;
      transform: rotateX(180deg);
    }
    100% {
      opacity: 1;
      transform: rotateX(360deg);
    }
  }
`;

const TitleForYear = styled.div`
  display: flex;
  border-radius: 22px 22px 0px 0px;
  background: ${(props) => props.theme.style.global.fontColor};
  color: #fff;
  padding: 8px 30px;
  justify-content: space-between;
  animation: pop 0.2s ease-in-out;
  @keyframes pop {
    0% {
      opacity: 0;
      transform: rotateX(0deg);
    }
    50% {
      opacity: 0.5;
      transform: rotateX(180deg);
    }
    100% {
      opacity: 1;
      transform: rotateX(360deg);
    }
  }
`;

const TitleForWeeks = styled.div`
  background: ${(props) => props.theme.style.global.fontColor};
  color: #fff;
  padding: 10px;
  display: grid;
  grid-template-columns: repeat(7, 1fr);
  animation: pop 0.6s ease-in-out;
  @keyframes pop {
    0% {
      opacity: 0;
      transform: rotateX(0deg);
    }
    50% {
      opacity: 0.5;
      transform: rotateX(180deg);
    }
    100% {
      opacity: 1;
      transform: rotateX(360deg);
    }
  }
`;
const WeekItem = styled.div``;

const DaysContainer = styled.div`
  display: grid;
  padding: 10px;
  border-radius: 0px 0px 22px 22px;
  background: #fff;
  grid-template-columns: repeat(7, 1fr);
  animation: pop 0.8s ease-in-out;
  @keyframes pop {
    0% {
      opacity: 0;
      transform: rotateX(0deg);
    }
    50% {
      opacity: 0.5;
      transform: rotateX(180deg);
    }
    100% {
      opacity: 1;
      transform: rotateX(360deg);
    }
  }
`;

const DayItem = styled.div`
  background: ${(props) =>
    props.current ? props.theme.style.global.fontColor : "inherit"};
  padding: 5px;
  color: ${(props) =>
    props.current ? "#fff" : props.theme.style.global.fontColor};
  transition: 0.2s;
  &:hover {
    background: ${(props) =>
      !props.current && props.theme.style.global.background};
  }
`;
const Count = styled.div``;

const UnactiveDayes = styled.div`
  color: grey;
  padding: 5px;
`;
