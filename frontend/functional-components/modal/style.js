import styled from "styled-components";

export const ModalContainer = styled.div`
  text-align: center;
  margin: auto;
  text-align: center;
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background-color: ${(props) => props.theme.style.global.fontColor}d1;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: 2s;
  display: flex;
  flex-direction: column;
  animation: gradient 0.5s ease-in-out;
  transition-delay: 0.5s;
  @keyframes gradient {
    0% {
      opacity: 0;
      background: inherit;
    }
    100% {
      opacity: 1;
      background-color: ${(props) => props.theme.style.global.fontColor}d1;
    }
  }
`;
export const ModalBody = styled.div`
  text-align: left;
  width: 1120px;
  max-width: 90%;
  margin: 0 auto 15px auto;
  max-height: 550px;
  overflow-x: hidden;
  overflow-y: auto;
  padding: 20px;
  color: ${(props) =>
    props.theme.style.global.darkFontColor
      ? props.theme.style.global.darkFontColor
      : props.theme.style.global.fontColor};
  box-shadow: 0 10px 15px rgba(27, 48, 110, 0.13);
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  background-color: ${(props) => props.theme.style.global.modalBackground};
  font-size: 14px;
  font-style: normal;
  letter-spacing: normal;
  line-height: 20px;
  animation: box 0.5s ease-in-out;
  @media (max-width: 1400px) and (min-width: 850px) {
    width: 1040px;
    max-width: 88%;
  }

  &::-webkit-scrollbar {
    width: 5px;
  }
  @keyframes box {
    0% {
      opacity: 0;
      transform: scale(0.5);
    }
    50% {
      opacity: 0.5;
      transform: scale(1.5);
    }
    100% {
      opacity: 1;
      transform: scale(1);
    }
  }
`;

export const CloseContainer = styled.div`
  width: 1120px;
  max-width: 90%;
  height: 24px;
  margin: 0 auto 15px auto;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  @media (max-width: 1400px) and (min-width: 850px) {
    width: 1040px;
    max-width: 88%;
  }
`;

export const CloseButton = styled.div`
  stroke: ${(props) => props.theme.style.global.closeButtonContentColor};
  cursor: pointer;
  width: 24px;
  height: 24px;
  box-shadow: 0 10px 20px rgba(27, 48, 110, 0.05);
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  border: ${(props) => props.theme.style.global.modalCloseButtonBorder};
  background-color: ${(props) => props.theme.style.global.modalBackground};
  animation: box 0.5s ease-in-out;
  @keyframes box {
    0% {
      opacity: 0;
      transform: scale(0.5);
    }
    50% {
      opacity: 0.5;
      transform: scale(1.5);
    }
    100% {
      opacity: 1;
      transform: scale(1);
    }
  }
`;
