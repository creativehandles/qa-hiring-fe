import React from "react";
import { useDispatch } from "react-redux";
import {
  ModalBody,
  ModalContainer,
  CloseButton,
  CloseContainer,
} from "./style";
import { close } from "../../redux/slices/modals";
import { Close } from "../svg-component";

export const Modal = ({ children }) => {
  const dispatch = useDispatch();
  return (
    <ModalContainer>
      <CloseContainer>
        <CloseButton onClick={() => dispatch(close())}>
          <Close />
        </CloseButton>
      </CloseContainer>
      <ModalBody dangerouslySetInnerHTML={{ __html: children }}>{}</ModalBody>
    </ModalContainer>
  );
};
