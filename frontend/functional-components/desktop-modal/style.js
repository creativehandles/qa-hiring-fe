import styled from "styled-components";

export const ModalContainer = styled.div`
  text-align: center;
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background-color: ${(props) => props.theme.style.global.fontColor}d1;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: 2s;
  display: flex;
  flex-direction: column;
  animation: gradient 0.5s ease-in-out;
  transition-delay: 0.5s;
  @keyframes gradient {
    0% {
      opacity: 0;
      background: inherit;
    }
    50% {
      opacity: 0.5;
      transform: scale(1.5);
    }
    100% {
      opacity: 1;
      background-color: ${(props) => props.theme.style.global.fontColor}d1;
    }
  }
`;
export const ModalBody = styled.div`
  margin-bottom: 15px;
  max-height: 550px;
  overflow: auto;
  max-width: 861px;
  display: flex;
  ${(props) =>
    props.theme.style.global.darkFontColor
      ? props.theme.style.global.darkFontColor
      : props.theme.style.global.fontColor};
  box-shadow: 0 10px 15px rgba(27, 48, 110, 0.13);
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  background-color: #ffffff;
  font-size: 14px;
  font-style: normal;
  letter-spacing: normal;
  line-height: 20px;
  animation: box 0.5s ease-in-out;
  &::-webkit-scrollbar {
    width: 20px;
  }
  @keyframes box {
    0% {
      opacity: 0;
      transform: scale(0.5);
    }
    100% {
      opacity: 1;
      transform: scale(1);
    }
  }
`;
export const CloseButton = styled.div`
  cursor: pointer;
  width: 24px;
  top: 15px;
  right: 15px;
  height: 24px;
  position: absolute;
`;
export const DescriptionContainer = styled.div`
  background-color: #ffffff;
  display: grid;
  grid-gap: 25px;
  flex-direction: column;
  padding: 80px 70px;
`;

export const ContentContainer = styled.div`
  background-color: ${(props) => props.theme.style.global.background};
  padding: 80px 70px;
  display: flex;
  flex-direction: column;
  position: relative;
`;

export const Title = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 20px;
  font-weight: 900;
  padding: 0px 54px;
  text-align: left;
`;
