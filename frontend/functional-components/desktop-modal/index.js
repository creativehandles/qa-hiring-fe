import React from "react";
import { useDispatch } from "react-redux";
import {
  ModalBody,
  ModalContainer,
  ContentContainer,
  CloseButton,
} from "./style";
import { Close } from "../svg-component";
import { close } from "../../redux/slices/modals";

export const DesktopModal = ({ children }) => {
  const dispatch = useDispatch();

  return (
    <ModalContainer>
      <ModalBody>
        <ContentContainer>
          <CloseButton onClick={() => dispatch(close())}>
            <Close />
          </CloseButton>
          {children}
        </ContentContainer>
      </ModalBody>
    </ModalContainer>
  );
};
