import {
  Container,
  JobContainer,
  JobTitle,
  ShortText,
  ApplyButton,
  DescriptionContainer,
  Description,
  ApplyContainer,
  DescriptionTitleUpperCase,
} from "./style";
import { useSelector } from "react-redux";
import useTranslation from "../../hooks/useTranslation";

export const JobDescription = ({ job, scrollToForm }) => {
  const { showLanguage } = useSelector(
    (state) => state.styles.styles.jobDescription
  );
  const { t } = useTranslation();

  return (
    <Container>
      <JobContainer>
        <JobTitle>{job.name}</JobTitle>
        <ApplyContainer>
          <div>
            {job.company_name && <ShortText>{job.company_name}</ShortText>}
            {job.city?.name && (
              <ShortText>
                {job.city?.name}
                {showLanguage && ", "}
              </ShortText>
            )}
            {showLanguage && <ShortText>{job.language}</ShortText>}
          </div>
          <ApplyButton onClick={scrollToForm}>{t("apply")}</ApplyButton>
        </ApplyContainer>
      </JobContainer>
      <DescriptionContainer>
        <DescriptionTitleUpperCase>
          {t("jobDescription")}
        </DescriptionTitleUpperCase>
        <Description
          dangerouslySetInnerHTML={{ __html: job.description }}
        ></Description>
      </DescriptionContainer>
    </Container>
  );
};
