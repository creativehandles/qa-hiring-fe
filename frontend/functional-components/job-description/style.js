import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.style.global.background};
`;

export const JobContainer = styled.div`
  border: ${(props) => props.theme.style.detail.bannerBorder};
  background-color: ${(props) => props.theme.style.detail.bannerBackground};
  padding: 40px 30px;
`;

export const DescriptionContainer = styled.div`
  width: 316px;
  border: ${(props) => props.theme.style.detail.bannerBorder};
  background-color: ${(props) => props.theme.style.detail.bannerBackground};
  margin: 20px auto;
  padding: 20px;
  & > span {
    display: block;
    margin: 0 auto;
    display: flex;
    justify-content: center;
  }
  & p {
    padding-bottom: 7px;
  }

  & ul {
    list-style-type: disc;
    padding: 7px 0 7px 16px;
  }
`;

export const JobTitle = styled.div`
  color: ${(props) => props.theme.style.job.titleColor};
  font-family: ${(props) => props.theme.style.job.titleFontFamily};
  font-size: ${(props) => props.theme.style.job.titleSize};
  font-weight: ${(props) => props.theme.style.job.titleWeight};
  letter-spacing: ${(props) => props.theme.style.job.letterSpacing};
`;
export const ShortText = styled.span`
  font-family: ${(props) => props.theme.style.job.tagsFontFamily};
  color: ${(props) => props.theme.style.job.tagsColor};
  font-size: ${(props) => props.theme.style.job.tagsSize};
  line-height: ${(props) => props.theme.style.job.tagsLineHeight};
  font-weight: ${(props) => props.theme.style.job.tagsWeight};
  letter-spacing: 0.7px;
  &:first-child {
    &:after {
      content: "";
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background: ${(props) => props.theme.style.job.tagsColor};
      display: inline-block;
      width: 3px;
      height: 3px;
      margin: 3px 5px;
    }
  }
`;

export const ApplyButton = styled.button`
  padding: 8px 25px;
  font-family: ${(props) => props.theme.style.job.buttonFontFamily};
  border-radius: ${(props) => props.theme.style.job.buttonRadius};
  background-color: ${(props) => props.theme.style.job.buttonBackground};
  outline: none;
  border: none;
  letter-spacing: 0.6px;
  color: ${(props) => props.theme.style.job.buttonColor};
  font-size: ${(props) => props.theme.style.job.buttonSize};
  font-weight: ${(props) => props.theme.style.job.buttonWeight};
  text-transform: uppercase;
  cursor: pointer;
  transition: 0.5s;
  &:hover {
    opacity: 0.7;
  }
`;

export const DescriptionTitle = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 13px;
  font-weight: 800;
  margin-bottom: 17px;
`;

export const DescriptionTitleUpperCase = styled(DescriptionTitle)`
  text-transform: uppercase;
`;

export const Description = styled.div`
  font-family: ${(props) => props.theme.style.job.descriptionFontFamily};
  color: ${(props) => props.theme.style.job.descriptionColor};
  font-size: ${(props) => props.theme.style.job.descriptionSize};
  font-weight: ${(props) => props.theme.style.job.descriptionWeight};
  line-height: ${(props) => props.theme.style.job.descriptionLineHeight};
  margin-bottom: 56px;
  overflow-x: auto;
`;
export const ApplyContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 15px;
`;
export const Contact = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  & > span {
    margin-left: 15px;
    font-size: 14px;
    font-weight: 300;
  }
`;
