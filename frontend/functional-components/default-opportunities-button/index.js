import React from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { setSeeAllOpportunities } from "../../redux/slices/all-opportunities";
import { mobileBreakpoint } from "../../constants/appConstants";
import useTranslation from "../../hooks/useTranslation";

export const DefaultOpportunitiesButton = ({ screenWidth }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const isMobile = screenWidth < mobileBreakpoint;

  const onButtonClick = () => {
    if (!isMobile) {
      router.push("/opportunities");
      return;
    }
    // Do a toggle to false and then true in order to trigger the scrollIntoView
    dispatch(setSeeAllOpportunities(false));
    dispatch(setSeeAllOpportunities(true));
  };

  return (
    <StyledDefaultOpportunitiesButton
      isMobile={isMobile}
      onClick={onButtonClick}
    >
      {t("uploadSeeAllOpportunitiesButtonName")}
    </StyledDefaultOpportunitiesButton>
  );
};

export const StyledDefaultOpportunitiesButton = styled.button`
  width: ${(props) =>
    props.isMobile
      ? props.theme.style.upload.defaultOpportunitiesButtonWidthMobile
      : props.theme.style.upload.defaultOpportunitiesButtonWidthDesktop};
  padding: ${(props) =>
    props.theme.style.upload.defaultOpportunitiesButtonPadding};
  min-height: 50px;
  border-radius: 5px;
  border: ${(props) =>
    props.theme.style.upload.defaultOpportunitiesButtonBorder};
  color: ${(props) =>
    props.theme.style.upload.defaultOpportunitiesColor
      ? props.theme.style.upload.defaultOpportunitiesColor
      : props.theme.style.upload.defaultOpportunitiesButtonColor};
  background-color: ${(props) =>
    props.theme.style.upload.defaultOpportunitiesButtonBackground};
  font-size: ${(props) =>
    props.isMobile
      ? props.theme.style.upload.defaultOpportunitiesFontSizeMobile
      : props.theme.style.upload.defaultOpportunitiesFontSizeDesktop};
  margin: 0px auto;
  font-weight: ${(props) =>
    props.theme.style.upload.defaultOpportunitiesFontWeight};
  & :hover {
    cursor: pointer;
    background-color: ${(props) =>
      props.theme.style.upload.defaultOpportunitiesButtonHoverBgr
        ? props.theme.style.upload.defaultOpportunitiesButtonHoverBgr
        : props.theme.style.upload.defaultOpportunitiesButtonColor};
    color: ${(props) => props.theme.style.upload.hoverTextCollor};
    transition: all 0.3s;
    opacity: ${(props) => props.theme.style.upload.hoverOpacity};
    border: none;
  }
`;
