import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CVfiled } from "../../functional-components/cv-filed";
import { DragAndDrop } from "../../functional-components/drag-and-drop";
import { LoaderOpportuniies } from "../../functional-components/loader";
import { NoOpenVacancies } from "../../functional-components/no-open-vacancies";
import { UploadAgreement } from "../../functional-components/upload-agreement";
import { setCV } from "../../redux/slices/CV";
import { fetchVacanciesForCandidate } from "../../redux/slices/vacancies/open-vacancies/functions/search-for-candidate";
import { getAPIURL } from "../../utils/api-url/index";
import useTranslation from "../../hooks/useTranslation";
import { fetchDefaultVacancies } from "../../redux/slices/vacancies/default/functions";

export const Upload = ({ enabledFetchDefaultVacancies }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const { results } = useSelector((state) => state.styles.styles);
  const reserveCV = useSelector((state) => state.CV.reserveCV);
  const candidate = useSelector((state) => state.candidate.value);
  const [isAccept, setIsAccept] = useState(false);
  const defaultVacancies = useSelector((state) => state.defaultVacancies.value);
  const defaultVacanciesLoading = useSelector(
    (state) => state.defaultVacancies.loading
  );
  const openVacancies = useSelector((state) => state.openVacancies.value);
  const file = useSelector((state) => state.CV.value);
  const dispatch = useDispatch();
  const filterState = useSelector((state) => state.filters);
  const { t } = useTranslation();

  useEffect(() => {
    if (!openVacancies.length) {
      if (file) {
        if (localStorage.getItem("candidate")) {
          setIsLoaded(true);
          setIsAccept(true);
        } else {
          setIsLoaded(false);
          setIsAccept(false);
        }
      } else {
        setIsLoaded(true);

        setIsAccept(true);
      }
    } else {
      if (reserveCV) {
        setIsLoaded(false);
        setIsAccept(false);
      }
    }
  }, [file]);

  useEffect(() => {
    if (candidate.id) {
      if (!file) {
        if (reserveCV) {
          return null;
        }

        dispatch(setCV(candidate.cv));
      }
      dispatch(fetchVacanciesForCandidate({ apiURL: getAPIURL() }));
    }
  }, [candidate.id, filterState]);

  useEffect(() => {
    if (file && openVacancies) setIsAccept(true);
  }, []);

  useEffect(() => {
    if (enabledFetchDefaultVacancies) {
      dispatch(fetchDefaultVacancies({ apiURL: getAPIURL() }));
    }
  }, [filterState]);

  if (!defaultVacancies.length && !defaultVacanciesLoading) {
    return <NoOpenVacancies mobile />;
  }

  return (
    <>
      {!file && <DragAndDrop />}
      {file && !isAccept && (
        <>
          <LoaderOpportuniies
            isLoaded={isLoaded}
            setIsLoaded={setIsLoaded}
            stillLoaded={t("loadingIconName")}
            alreadyLoaded={t("resultsIconName")}
            data={openVacancies}
            mobile
          />
          <UploadAgreement
            isLoaded={isLoaded}
            setIsAccept={setIsAccept}
            mobile
          />
        </>
      )}
      {file && isAccept && <CVfiled data={openVacancies} file={file} />}
    </>
  );
};
