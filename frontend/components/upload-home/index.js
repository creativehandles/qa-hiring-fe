import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { DragAndDropHome } from "../../functional-components/drag-and-drop-home";
import { LoaderOpportuniies } from "../../functional-components/loader";
import { UploadAgreement } from "../../functional-components/upload-agreement";
import { setCV } from "../../redux/slices/CV";
import { fetchVacanciesForCandidate } from "../../redux/slices/vacancies/open-vacancies/functions/search-for-candidate";
import { getAPIURL } from "../../utils/api-url/index";
import useTranslation from "../../hooks/useTranslation";

export const UploadHome = () => {
  const { t } = useTranslation();
  const [isLoaded, setIsLoaded] = useState(false);
  const [isAccept, setIsAccept] = useState(false);
  const openVacancies = useSelector((state) => state.openVacancies.value);
  const candidate = useSelector((state) => state.candidate.value);
  const dispatch = useDispatch();
  const file = useSelector((state) => state.CV.value);
  const router = useRouter();
  const [needFetch, setNeedFetch] = useState(false);
  const filterState = useSelector((state) => state.filters);
  if (isAccept && file)
    router.push({ pathname: "/opportunities/", query: { seeDeafult: false } });

  useEffect(() => {
    if (openVacancies.length && file) {
      setIsLoaded(true);
    }
  }, []);

  useEffect(() => {
    if (candidate.id && !file) {
      setNeedFetch(false);
      dispatch(fetchVacanciesForCandidate({ apiURL: getAPIURL() }));
      setIsLoaded(true);
      dispatch(setCV(candidate.cv));
    }
  }, [candidate.id, filterState]);

  return (
    <MainContainer>
      <Container>
        {!file && (
          <DragAndDropHome
            setIsAccept={setIsAccept}
            setNeedFetch={setNeedFetch}
          ></DragAndDropHome>
        )}
        {file && !isAccept && (
          <>
            <UploadContainer>
              <LoaderOpportuniies
                needFetch={needFetch}
                isLoaded={isLoaded}
                stillLoaded={t("loadingIconName")}
                alreadyLoaded={t("resultsIconName")}
                setIsLoaded={setIsLoaded}
              />
            </UploadContainer>
            <AgreementContainer>
              <UploadAgreement
                big
                isAccept={isAccept}
                isLoaded={isLoaded}
                setIsAccept={setIsAccept}
                setIsLoaded={setIsLoaded}
                setNeedFetch={setNeedFetch}
              />
            </AgreementContainer>
          </>
        )}
      </Container>
    </MainContainer>
  );
};
const MainContainer = styled.div`
  width: 100%;
  display: flex;
  padding: 80px 0px;
  justify-content: center;
  align-items: center;
  background: ${(props) => props.theme.style.mainHome.background};
`;
const Container = styled.div`
  width: 100%;
  max-width: 1160px;
  margin: 0 auto;
  padding: 0px 20px;
  display: flex;
  flex-direction: row-reverse;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 1400px) and (min-width: 850px) {
    padding: 0px 65px;
  }
`;

const UploadContainer = styled.div`
  display: flex;
  align-items: center;
  background: ${(props) => props.theme.style.loading.uploadBackground};
  box-shadow: ${(props) => props.theme.style.loading.uploadShadow};
  border-radius: ${(props) => props.theme.style.loading.uploadRadius};
  border: ${(props) => props.theme.style.loading.uploadBorder};
  width: 25%;
  width: 315px;
  height: 315px;
`;
const AgreementContainer = styled.div`
  max-width: 588px;
`;
