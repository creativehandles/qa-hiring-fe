import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import useFetch from "react-fetch-hook";
import createTrigger from "react-use-trigger";
import useTrigger from "react-use-trigger/useTrigger";

import { ListOfOpportunities } from "../../functional-components/opportunities/list";
import { OpportunitiesDescription } from "../../functional-components/opportunities-description";
import { useDispatch, useSelector } from "react-redux";
import { DragAndDropHome } from "../../functional-components/drag-and-drop-home";
import { CVComponent } from "../common/CV-component";
import { NoOpenVacancies } from "../../functional-components/no-open-vacancies";
import { LoaderOpportuniies } from "../../functional-components/loader";
import { UploadAgreement } from "../../functional-components/upload-agreement";
import { TextSection } from "../../functional-components/text-section";
import { setCV } from "../../redux/slices/CV";
import { fetchVacanciesForCandidate } from "../../redux/slices/vacancies/open-vacancies/functions/search-for-candidate";
import { getAPIURL } from "../../utils/api-url/index";
import FilterContainer from "../../functional-components/filters/filter-container";
import ConfigStyles from "../../utils/config-styles";
import { useRouter } from "next/router";
import prettyUrl from "../../utils/pretty-url";
import { appendQueryParams } from "../../utils/query-params";
import useTranslation from "../../hooks/useTranslation";

const fetchVacanciesTrigger = createTrigger();

// Note, for text, we take the translatable key from the .config file
export const OpportunitiesDesktop = ({ text }) => {
  const { t } = useTranslation();
  const file = useSelector((state) => state.CV.value);
  const reserveCV = useSelector((state) => state.CV.reserveCV);
  const openVacancies = useSelector((state) => state.openVacancies.value);
  const openVacanciesLoading = useSelector(
    (state) => state.openVacancies.loading
  );

  const filterState = useSelector((state) => state.filters);
  const [isAccept, setIsAccept] = useState(true);
  const candidate = useSelector((state) => state.candidate?.value);
  const router = useRouter();
  const query = router.query;
  const [isLoaded, setIsLoaded] = useState(false);
  const [seeDeafult, setSeeDafault] = useState(query.seeDeafult ? false : true);
  const vacancy = useSelector((state) => state.vacancy.value);
  const dispatch = useDispatch();
  const hasVacancy = Object.keys(vacancy).length;
  const description = file
    ? t("opportunitiesTextCVUploaded")
    : t("opportunitiesTextCVNotUploaded");
  const jobContainerRef = useRef(null);
  const localStyles = ConfigStyles();
  const isFiltersEnabled = localStyles.manualFilters.filtersEnabled;
  const { showOpportunitiesVideo, navigateToFirstOpportunity } = useSelector(
    (state) => state.styles.styles.mainOpportunities
  );
  const queryState = useSelector((state) => state.query);
  const uRLWithQueryParams = appendQueryParams(`${getAPIURL()}vacancies`);
  const requestVacanciesTriggerValue = useTrigger(fetchVacanciesTrigger);
  const { isLoading: defaultVacanciesLoading, data: response } = useFetch(
    uRLWithQueryParams,
    { depends: [filterState, requestVacanciesTriggerValue] }
  );
  const defaultVacancies = response && response.data && response.data.items;

  const navigateAway =
    navigateToFirstOpportunity &&
    openVacancies.length > 0 &&
    file &&
    isAccept &&
    !query.id; // Only if not viewing a job detail already

  if (navigateAway) {
    const { id, slug, name } = openVacancies[0];
    router.push(
      {
        pathname: "/opportunities/[id]/[name]",
        query: { id, name },
      },
      `/opportunities/${id}/${prettyUrl(slug)}`,
      { shallow: true }
    );
  }

  useEffect(() => {
    if (!defaultVacanciesLoading) {
      fetchVacanciesTrigger();
    }
  }, [queryState.query]);

  useEffect(() => {
    if (vacancy.slug && jobContainerRef.current) {
      jobContainerRef.current.scrollTo(0, 0);
    }
  }, [vacancy.slug]);

  useEffect(() => {
    if (!openVacancies.length) {
      if (file) {
        if (localStorage.getItem("candidate")) {
          setIsLoaded(true);
          setIsAccept(true);
        } else {
          setIsLoaded(false);
        }
      } else {
        setIsLoaded(false);
      }
    }
  }, [file]);

  useEffect(() => {
    if (candidate.id) {
      if (!file) {
        if (reserveCV) {
          return null;
        }

        dispatch(setCV(candidate.cv));
      }
      dispatch(fetchVacanciesForCandidate({ apiURL: getAPIURL() }));
    } else {
      setSeeDafault(true);
    }
  }, [candidate.id, filterState]);

  useEffect(() => {
    if (openVacanciesLoading) setSeeDafault(false);
  }, [openVacanciesLoading]);

  const returnUpload = () => {
    if (!file && hasVacancy)
      return (
        <DragAndDropContainer>
          <DragAndDropHome setIsAccept={setIsAccept} onlyCV fill />
        </DragAndDropContainer>
      );
    else return "";
  };

  const returnDescription = () => {
    const jobCount = (
      <JobCount
        dangerouslySetInnerHTML={{
          __html: t("weFoundNoOfJobs", { noOfJobs: openVacancies.length }),
        }}
      />
    );
    if (hasVacancy)
      return (
        <>
          <OpportunitiesDescription text={t(text)} job={vacancy} />
          {text && showOpportunitiesVideo && (
            <TextSection opportunities text={t(text)} reverse />
          )}
        </>
      );
    else {
      if (file)
        return openVacanciesLoading ? (
          <Loader />
        ) : (
          <ResultsContainer>{jobCount}</ResultsContainer>
        );
      else
        return (
          <CVContainer>
            {" "}
            <DragAndDropHome
              setIsAccept={setIsAccept}
              onlyCV
              data={openVacancies}
            />
            {text && showOpportunitiesVideo && (
              <TextSection opportunities text={t(text)} />
            )}
          </CVContainer>
        );
    }
  };

  const returnCurrentList = () => {
    if (openVacanciesLoading || defaultVacanciesLoading || navigateAway) {
      return (
        <SpinnerContainer>
          <Loader />
        </SpinnerContainer>
      );
    }

    if (
      (!defaultVacancies || !defaultVacancies.length) &&
      !defaultVacanciesLoading
    ) {
      return <NoOpenVacancies opportunities />;
    }

    if (!openVacancies.length && !seeDeafult && candidate.id) {
      return (
        <NoOpenVacancies
          opportunities
          setSeeDafault={setSeeDafault}
          hasDefaultVacancies
        />
      );
    }

    if (!openVacancies.length && seeDeafult) {
      return (
        <>
          <ContactContainer>
            {returnUpload()}
            <ListOfOpportunities
              noRedirect
              data={defaultVacancies}
              withoutLoadButton={false}
            />
          </ContactContainer>
          <JobContainer ref={jobContainerRef}>
            {returnDescription()}
          </JobContainer>
        </>
      );
    }
    if (openVacancies && file) {
      return (
        <>
          <ContactContainer>
            {returnUpload()}
            <ListOfOpportunities
              noRedirect
              data={openVacancies}
              withoutLoadButton={false}
            />
          </ContactContainer>
          <JobContainer ref={jobContainerRef}>
            {returnDescription()}
          </JobContainer>
        </>
      );
    } else
      return (
        <>
          <ContactContainer>
            {returnUpload()}
            <ListOfOpportunities
              noRedirect
              data={defaultVacancies}
              withoutLoadButton={false}
            />
          </ContactContainer>
          <JobContainer ref={jobContainerRef}>
            {returnDescription()}
          </JobContainer>
        </>
      );
  };
  return (
    <Container>
      <Banner>
        <HeadContainer>
          <Title>{t("opportunitiesTitle")}</Title>
          <DescriptionContainer>
            <DescriptionText
              dangerouslySetInnerHTML={{ __html: description }}
            ></DescriptionText>
            {file && (
              <CVComponent setSeeDafault={setSeeDafault} opportunities />
            )}
          </DescriptionContainer>
        </HeadContainer>
      </Banner>
      {isFiltersEnabled && <FilterContainer />}
      <HeroImage>
        <MainSection>
          {!isAccept && file && (
            <LoaderContainer>
              <LoaderOpportuniies
                isLoaded={isLoaded}
                setIsLoaded={setIsLoaded}
                stillLoaded={t("loadingIconName")}
                alreadyLoaded={t("resultsIconName")}
              />
              <UploadAgreement
                mobile
                isAccept={isAccept}
                isLoaded={isLoaded}
                setIsAccept={setIsAccept}
              />
            </LoaderContainer>
          )}
          {isAccept && returnCurrentList()}
          {!isAccept && !file && returnCurrentList()}
        </MainSection>
      </HeroImage>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  background: ${(props) => props.theme.style.global.background};
`;
const Banner = styled.div`
  flex-shrink: 0;
  background-color: ${(props) =>
    props.theme.style.mainOpportunities.bannerBackground};
  box-shadow: ${(props) => props.theme.style.mainOpportunities.bannerBoxShadow};
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const HeadContainer = styled.div`
  width: 100%;
  padding: 20px 20px 15px 20px;
  max-width: 1160px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (max-width: 1400px) and (min-width: 850px) {
    padding: 20px 60px 15px 60px;
  }
`;

const Title = styled.div`
  font-size: ${(props) => props.theme.style.mainOpportunities.titleSize};
  font-family: ${(props) =>
    props.theme.style.mainOpportunities.titleFontFamily};
  font-weight: ${(props) => props.theme.style.mainOpportunities.titleWeight};
  margin-bottom: ${(props) =>
    props.theme.style.mainOpportunities.titleBottomMargin};
  color: ${(props) => props.theme.style.mainOpportunities.titleColor};
  letter-spacing: ${(props) =>
    props.theme.style.mainOpportunities.letterSpacing};
  line-height: normal;
`;
const DescriptionText = styled.div`
  text-align: left;
  font-family: ${(props) =>
    props.theme.style.mainOpportunities.descriptionFontFamily};
  font-size: ${(props) => props.theme.style.mainOpportunities.descriptionSize};
  font-weight: ${(props) =>
    props.theme.style.mainOpportunities.descriptionWeight};
  margin-right: 10px;
  color: ${(props) => props.theme.style.mainOpportunities.descriptionColor};
`;

const DescriptionContainer = styled.div`
  display: flex;
  align-items: center;
`;

const HeroImage = styled.div`
  height: 100%;
  background-image: url(${(props) =>
    props.theme.style.mainOpportunities.heroImage});
  background-color: ${(p) =>
    p.theme.style.mainOpportunities.heroBackgroundColor};
  -webkit-box-shadow: ${(p) => p.theme.style.mainOpportunities.heroBoxShadow};
  background-size: cover;
`;

const MainSection = styled.section`
  height: 100%;
  flex: 1 0px;
  padding: 30px 20px;
  display: flex;
  max-width: 1160px;
  margin: 0 auto;
  justify-content: center;

  @media (max-width: 1400px) and (min-width: 850px) {
    padding: 30px 60px;
  }
`;

const ContactContainer = styled.div`
  grid-gap: 20px;
  margin-right: 30px;
  max-width: 430px;
  width: 100%;
  overflow: auto;
  &::-webkit-scrollbar {
    width: 5px;
  }
`;

const JobContainer = styled.div`
  max-width: 660px;
  width: 100%;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  overflow: auto;
  &::-webkit-scrollbar {
    width: 5px;
  }
`;

const CVContainer = styled.div`
  background: ${(props) =>
    props.theme.style.mainOpportunities.textSectionBackground};
  border-radius: ${(props) =>
    props.theme.style.mainOpportunities.textSectionBorderRadius};
  border: ${(props) => props.theme.style.mainOpportunities.textSectionBorder};
`;
const ResultsContainer = styled.div`
  width: 600px;
  display: flex;
  align-items: flex-start;
  padding: 50px;
  justify-content: center;
  font-size: 32px;
  color: ${(props) => props.theme.style.global.fontColor};
  padding: 123px;
`;

const LoaderContainer = styled.div`
  width: 100%;
`;

const JobCount = styled.span`
  text-align: center;
`;

const DragAndDropContainer = styled.div`
  flex-shrink: 0;
  border-radius: ${(props) =>
    props.theme.style.mainOpportunities.textSectionBorderRadius};
  border: ${(props) => props.theme.style.mainOpportunities.textSectionBorder};
  margin-bottom: 20px;
`;

const SpinnerContainer = styled.div`
  display: flex;
  align-items: center;
`;

const Loader = styled.div`
  border: 16px solid #fff;
  border-top: 16px solid ${(props) => props.theme.style.global.fontColor};
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
