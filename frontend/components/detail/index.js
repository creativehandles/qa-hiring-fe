import React, { useRef } from "react";
import { JobDescription } from "../../functional-components/job-description";
import { VacancyForm } from "../../functional-components/vacancy-form";

export const Detail = ({ job }) => {
  const target = useRef();
  const scrollToForm = () => {
    target.current.scrollIntoView({
      behavior: "smooth",
    });
  };
  return (
    <>
      <JobDescription scrollToForm={scrollToForm} job={job} />
      <VacancyForm target={target} mhub_id={job.mhub_id} mobile />
    </>
  );
};
