import React, { useState } from "react";
import styled from "styled-components";
import { SalaryFilter } from "../../functional-components/salary-filter";
import { JobFilter } from "../../functional-components/job-filter";
import {
  DatePickerCallendar,
  Warning,
} from "../../functional-components/svg-component";
import { Checkbox } from "../common/checkbox";
import { CVComponent } from "../common/CV-component";
import { ManualRanger } from "../common/manual-ranger";
import { DatePicker } from "../../functional-components/datepicker";

export const Settings = ({ mobile }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [userDate, setUserDate] = useState();

  if (mobile) {
    return (
      <Container>
        <MobileTitleContainer>
          <MobileTitle>Manage your settings:</MobileTitle>
        </MobileTitleContainer>
        <MobileMainSection>
          <MobileCardTitle>EMAIL PREFERENCES</MobileCardTitle>
          <MobileCardSection>
            <CardRow>
              <Text>Job alerts</Text>
              <Checkbox />
            </CardRow>
            <CardRow>
              <Text>From</Text>
              <DatepickerContainer onClick={() => setIsOpen(!isOpen)}>
                <DateFiled
                  readOnly
                  value={userDate ? userDate.toLocaleDateString() : "today"}
                />
                <DatePickerCallendar />
              </DatepickerContainer>
              {isOpen && (
                <DatePicker userDate={userDate} setUserDate={setUserDate} />
              )}
            </CardRow>
            <CardRow>
              <Text>LOGO updates</Text>
              <Checkbox />
            </CardRow>
          </MobileCardSection>
          <MobileCardTitle>Cv</MobileCardTitle>
          <CardForCV>
            <CVComponent settings />
          </CardForCV>
          <MobileCardTitle>FILTERS</MobileCardTitle>
          <MobileFilterSection>
            <JobFilter/>
          </MobileFilterSection>
          <MobileFilterSection>
            <SalaryFilter />
          </MobileFilterSection>
          <MobileFilterSection>
            <ManualRanger title="Schedule" />
          </MobileFilterSection>
        </MobileMainSection>
      </Container>
    );
  }
  return (
    <Container>
      <TitleContainer>
        <TitleText>Manage your settings</TitleText>
      </TitleContainer>
      <MainSection>
        <CardTitle>EMAIL PREFERENCES</CardTitle>
        <Card>
          <CardSection>
            <CardRow>
              <Text>Job alerts</Text>
              <Checkbox />
            </CardRow>
            <CardRow>
              <Text>From</Text>
              <DatepickerContainer onClick={() => setIsOpen(!isOpen)}>
                <DateFiled
                  readOnly
                  value={userDate ? userDate.toLocaleDateString() : "today"}
                />
                <DatePickerCallendar />
              </DatepickerContainer>
              {isOpen && (
                <DatePicker userDate={userDate} setUserDate={setUserDate} />
              )}
            </CardRow>
            <CardRow>
              <Text>LOGO updates</Text>
              <Checkbox />
            </CardRow>
          </CardSection>
          <Description>
            <Warning width="20px" />
            Job alert will seek positions based on your uploaded CV and saved
            filters and notify you on email when there is a match, so you can
            make sure you’re one of the first people applying.
          </Description>
        </Card>
        <CardTitle>Cv</CardTitle>
        <CardForCV>
          <CVComponent />
        </CardForCV>
        <CardTitle>MANUAL FILTERS</CardTitle>
        <Card>
          <FilterSection>
            <JobFilter/>
          </FilterSection>
          <FilterSection>
            <SalaryFilter />
            <ManualRanger style={{ marginTop: 10 }} title="Schedule" />
          </FilterSection>
        </Card>
      </MainSection>
    </Container>
  );
};
const Container = styled.div``;
const TitleContainer = styled.div`
  border: 1px solid #ebeef7;
  background-color: #ffffff;
  padding: 50px 80px;
  box-shadow: 0 10px 20px rgba(27, 48, 110, 0.05);
  display: flex;
  justify-content: center;
`;
const TitleText = styled.p`
  font-size: 40px;
  font-weight: 900;
  color: ${(props) => props.theme.style.global.fontColor};
  margin-right: 350px;
`;
const MainSection = styled.div`
  width: 796px;
  margin: 0 auto;
`;
const Card = styled.div`
  display: grid;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  border: 1px solid #ebeef7;
  background-color: #ffffff;
  padding: 30px;
  margin-top: 12px;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30px;
`;
const CardTitle = styled.p`
  font-size: 16px;
  font-weight: 800;
  letter-spacing: 1.28px;
  line-height: 14px;
  text-align: left;
  margin-top: 50px;
  text-transform: uppercase;
  color: ${(props) => props.theme.style.global.fontColor};
`;
const DatepickerContainer = styled.div`
  padding: 7px;
  width: 100px;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  border: 1px solid #ebeef7;
  background-color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  z-index: 1;
  cursor: pointer;
`;

const DateFiled = styled.input`
  border: 0;
  outline: none;
  width: 70%;
  font-weight: 700;
  font-size: 11px;
  cursor: pointer;
  color: ${(props) => props.theme.style.global.fontColor};
`;
const CardSection = styled.div`
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  border: 1px solid #ebeef7;
  background-color: #ffffff;
`;

const CardRow = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px;
  position: relative;
`;

const Text = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 14px;
  font-weight: 500;
`;
const Description = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-auto-flow: column;
  align-items: center;
  padding: 10px 10px;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  color: #828899;
  border: 1px solid #ebeef7;
  background-color: ${(props) => props.theme.style.global.background};
  font-size: 12px;
  height: 100%;
`;

const CardForCV = styled(Card)`
  padding: 10px 20px;
  grid-template-columns: 1fr;
  grid-auto-flow: column;
`;

const FilterSection = styled(CardSection)`
  border: none;
`;

const MobileTitle = styled(TitleText)`
  font-size: 20px;
  margin-right: 100px;
`;
const MobileCardTitle = styled(CardTitle)`
  font-size: 14px;
`;
const MobileMainSection = styled(MainSection)`
  width: 359px;
  padding: 20px 30px;
`;
const MobileCardSection = styled(CardSection)`
  width: 100%;
  margin-top: 10px;
`;

const MobileTitleContainer = styled(TitleContainer)`
  padding: 30px;
`;

const MobileFilterSection = styled.div`
  margin-top: 10px;
`;
