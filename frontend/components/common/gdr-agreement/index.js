import React from "react";
import styled from "styled-components";
import {
  BoxAgree,
  BoxDisagree,
} from "../../../functional-components/svg-component";
import { getGDPR } from "../../../utils/gdpr";

export const GdrAgreement = ({
  isChecked,
  handleCheckboxChange,
  label,
  center,
  isOpportunitiesPage,
  mobile,
}) => {
  const showIcon = () => {
    const icon = isChecked ? <BoxAgree /> : <BoxDisagree />;
    return icon;
  };
  return getGDPR() ? null : (
    <Container center={center}>
      <CustomCheckbox
        value={isChecked}
        onChange={() => handleCheckboxChange(!isChecked)}
        type="checkbox"
      />
      <CheckContainer isChecked={isChecked}>{showIcon()}</CheckContainer>
      <Label
        isOpportunitiesPage={isOpportunitiesPage}
        mobile={mobile}
        onClick={() => handleCheckboxChange(!isChecked)}
      >
        {label}
      </Label>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  margin: 10px 0px;
  width: 100%;
  justify-content: ${(props) => (props.center ? "center" : "flex-start")};
`;

const Label = styled.div`
  border: 1px solid rgba(0, 0, 0, 0);
  color: ${(props) =>
    props.isOpportunitiesPage
      ? props.theme.style.checkbox.opportunitiesColor
      : props.theme.style.checkbox.color};
  font-size: ${(props) =>
    props.mobile || props.isOpportunitiesPage
      ? props.theme.style.checkbox.fontSizeMobile
      : props.theme.style.checkbox.fontSizeDesktop};
  line-height: ${(props) => props.theme.style.checkbox.lineHeight};
  font-weight: ${(props) => props.theme.style.checkbox.fontWeight};
  font-family: ${(props) => props.theme.style.checkbox.fontFamily};
  letter-spacing: ${(props) => props.theme.style.checkbox.letterSpacing};
  text-align: left;
  margin-left: 20px;
  cursor: pointer;
`;
const CustomCheckbox = styled.input`
  position: absolute;
  left: 0;
  z-index: 0;
  opacity: 0;
  width: 20px;
  height: 20px;
  cursor: pointer;
`;
const CheckContainer = styled.div`
  fill: ${(props) =>
    props.theme.style.checkbox.checkFillColor
      ? props.theme.style.checkbox.checkFillColor
      : props.theme.style.checkbox.checkboxColor};
  stroke: ${(props) =>
    props.isChecked
      ? props.theme.style.checkbox.checkSignColor
        ? props.theme.style.checkbox.checkSignColor
        : props.theme.style.checkbox.checkboxColor
      : props.theme.style.checkbox.checkboxColor};
`;
