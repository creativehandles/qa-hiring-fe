import React, { useState } from "react";
import styled from "styled-components";
export const Checkbox = (props) => {
  const [isCheked, setIsChecked] = useState(true);
  return (
    <CheckBox
      {...props}
      onClick={() => setIsChecked(!isCheked)}
      isCheked={isCheked}
    >
      <Check isCheked={isCheked} />
    </CheckBox>
  );
};
const CheckBox = styled.div`
  background-color: ${(props) =>
    props.isCheked ? props.theme.style.global.fontColor : "#ffffff"};
  border: 1px solid #ebeef7;
  border-radius: 22px;
  width: 35px;
  height: 16px;
  padding: 2px;
  transition: 0.2s all;
`;
const Check = styled.div`
  width: 5px;
  background-color: ${(props) =>
    !props.isCheked ? props.theme.style.global.fontColor : "#ffffff"};
  transform: ${(props) =>
    !props.isCheked ? "translateX(0%)" : "translateX(185%)"};
  border-radius: 55px;
  padding: 5px;
  transition: 0.2s all;
`;
