import React, { useState } from "react";
import styled from "styled-components";

export const Select = ({ data = [], onChange = () => {} }) => {
  const newData = data.map((el) => {
    return {
      location: el,
      isSelected: false,
    };
  });
  const [values, setValues] = useState(newData);

  const onSleceted = (el) => {
    onChange(el.location);
    const newArr = values.map((item) => {
      return item.location === el.location
        ? { ...item, isSelected: true }
        : { ...item, isSelected: false };
    });
    setValues([...newArr]);
  };
  return (
    <Container>
      {values.map((el, index) => {
        return (
          <Location
            key={index}
            isSelected={el.isSelected}
            onClick={() => onSleceted(el)}
          >
            {el.location}
          </Location>
        );
      })}
    </Container>
  );
};

const Location = styled.button`
  color: #8798c7;
  outline: none;
  background: inherit;
  border: 0;
  width: 100%;
  height: 100%;
  text-align: center;
  font-size: 12px;

  &:last-child {
    border-radius: ${(props) => (props.isSelected ? "0px 5px 5px 0px" : "")};
  }
  &:first-child {
    border-radius: ${(props) => (props.isSelected ? "5px 0px 0px 5px" : "")};
  }

  ${(props) =>
    props.isSelected &&
    `
        background-color: ${props.theme.style.global.fontColor};
        color:#e9ecf3;
    `}
`;

const Container = styled.div`
  display: flex;
  border: 1px solid #e8ebf3;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  align-items: center;
  height: 30px;
  justify-content: space-between;
  margin-bottom: 10px;
`;
