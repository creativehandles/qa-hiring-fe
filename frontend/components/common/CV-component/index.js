import React from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";

import {
  CloseCV,
  DeleteCV,
  Download,
  PaperClipXXL,
  BigPaperClip,
  Dots,
  PaperClip,
} from "../../../functional-components/svg-component";
import { removeCV } from "../../../redux/slices/CV";

export const CVComponent = ({
  settings,
  opportunities,
  setSeeDafault = () => {},
}) => {
  const file = useSelector((state) => state.CV.value);

  const dounloadFile = () => {
    const url = window.URL.createObjectURL(new Blob([file]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", file.name);
    document.body.appendChild(link);
    link.click();
  };

  const dispatch = useDispatch();

  if (opportunities) {
    return (
      <ContainerOpportunities>
        <ClipContainer>
          <PaperClip viewBox="9 17 26 12" />
        </ClipContainer>
        <Text>{file.name}</Text>
        <DeleteFile
          onClick={() => {
            setSeeDafault(true);
            dispatch(removeCV());
          }}
        >
          <CloseCV />
        </DeleteFile>
      </ContainerOpportunities>
    );
  }
  if (settings) {
    return (
      <SettingsContainer>
        <ClipContainer>
          <PaperClipXXL />
        </ClipContainer>
        <MobileText>{file.name}</MobileText>
        <DotsContainer>
          <Dots />
        </DotsContainer>
      </SettingsContainer>
    );
  }

  return (
    <ContainerCV>
      <CVItem>
        <BigPaperClip />
        <SettingsTitleText>{file.name}</SettingsTitleText>
      </CVItem>
      <CVItem>
        <SettingText>{file.type}</SettingText>
        <SettingText>{file.size}kb</SettingText>
        <SettingText>
          {file.lastModifiedDate.toLocaleDateString().replaceAll("/", ".")}
        </SettingText>
        <ButtonsContainer>
          <ButtonCV onClick={() => dounloadFile()}>
            <Download />
          </ButtonCV>
          <ButtonCV>
            <DeleteCV />
          </ButtonCV>
        </ButtonsContainer>
      </CVItem>
    </ContainerCV>
  );
};

const DeleteFile = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0px 10px;

  border-left: 1px solid
    ${(props) =>
      props.theme.style.mainOpportunities.cvBorderColor
        ? props.theme.style.mainOpportunities.cvBorderColor
        : props.theme.style.mainOpportunities.cvColor};
  fill: ${(props) => props.theme.style.mainOpportunities.deleteColor};
  stroke: ${(props) => props.theme.style.mainOpportunities.deleteColor};
`;
const ContainerCV = styled.div`
  display: flex;
  width: 100%;
  height: 28px;
  fill: ${(props) => props.theme.style.mainOpportunities.cvColor};
  stroke: ${(props) => props.theme.style.mainOpportunities.cvColor};
  background: ${(props) => props.theme.style.mainOpportunities.cvBackground};
  justify-content: space-between;
  font-family: ${(props) => props.theme.style.mainOpportunities.cvfontFamily};
  color: ${(props) => props.theme.style.global.fontColor};
`;
const Text = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  align-self: center;
  color: ${(props) => props.theme.style.mainOpportunities.cvColor};
  margin-right: 10px;
  max-width: 120px;
`;

const ClipContainer = styled.div`
  position: relative;
  top: 5px;
  fill: ${(props) => props.theme.style.mainOpportunities.paperClipColor};
  stroke: ${(props) => props.theme.style.mainOpportunities.paperClipColor};

  svg {
    transform: rotate(45deg);
  }
`;

const SettingsContainer = styled(ContainerCV)`
  width: 100%;
`;

const SettingText = styled(Text)`
  color: #828899;
  font-weight: 300;
`;

const SettingsTitleText = styled(Text)`
  font-size: 16px;
  font-weight: 800;
  margin-right: 145px;
  margin-left: 20px;
`;
const ButtonsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 10px;
  margin-left: 50px;
`;

const ButtonCV = styled.button`
  outline: none;
  border: 1px solid #ebeef7;
  width: 24px;
  height: 24px;
  background: inherit;
  cursor: pointer;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const DotsContainer = styled.button`
  width: 25px;
  display: flex;
  margin-left: auto;
  cursor: pointer;
  align-items: center;
  box-shadow: 0 10px 20px rgba(27, 48, 110, 0.05);
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  background-color: #ffffff;
  outline: none;
  border: 1px solid #ebeef7;
`;

const MobileText = styled(Text)`
  width: auto;
  margin: 0;
  font-size: 14px;
  margin-left: 20px;
`;

const ContainerOpportunities = styled(ContainerCV)`
  border-radius: ${(props) =>
    props.theme.style.mainOpportunities.cvBorderRadius};
  border: ${(props) => props.theme.style.mainOpportunities.cvBorder};
  box-shadow: ${(props) => props.theme.style.mainOpportunities.cvBoxShadow};
  padding: 5px;
  width: auto;
`;

const CVItem = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-auto-flow: column;
`;
