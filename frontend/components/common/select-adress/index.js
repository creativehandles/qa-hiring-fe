import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
  DeleteValue,
  Drop,
} from "../../../functional-components/svg-component";

export const SelectAdress = ({
  data,
  onChange = () => {},
  defaultValue,
  width,
}) => {
  const [value, setValue] = useState(defaultValue ? [defaultValue] : []);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    onChange(value);
  }, [value]);

  const deleteValue = (town) => {
    setValue(value.filter((el) => el !== town));
  };

  const addTown = (town) => {
    if (!value.find((el) => el === town)) setValue([...value, town]);
    setIsOpen(!isOpen);
  };
  const listOfValues = data.map((el, index) => {
    return (
      <DropdownListItem key={index} onClick={() => addTown(el)}>
        {el}
      </DropdownListItem>
    );
  });

  const showValue = () => {
    return value.map((town, index) => {
      return (
        <DropdownValue key={index}>
          <div>{town}</div>
          <button onClick={() => deleteValue(town)}>
            <DeleteValue />
          </button>
        </DropdownValue>
      );
    });
  };

  return (
    <Dropdown width={width}>
      <DropdownContainer>
        {showValue()}
        {isOpen && <DropdownList>{listOfValues}</DropdownList>}
      </DropdownContainer>
      <DropButtton onClick={() => setIsOpen(!isOpen)}>
        <Drop />
      </DropButtton>
    </Dropdown>
  );
};

const Dropdown = styled.div`
  display: flex;
  width: 60%;
  justify-content: space-around;
  align-items: center;
  border: 1px solid #e8ebf3;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
`;
const DropdownContainer = styled.div`
  position: relative;
  width: 80%;
  flex-direction: column;
  justify-content: center;
  display: flex;
  height: 100%;
  align-items: flex-start;
`;
const DropButtton = styled.button`
  width: 20%;
  outline: none;
  border: 0;
  background: inherit;
  height: 100%;
  border-left: 1px solid #e8ebf3;
`;
const DropdownValue = styled.div`
  font-size: 12px;
  margin: 5px 0;
  display: flex;
  padding: 5px;
  background: ${(props) => props.theme.style.global.fontColor};
  color: #e9ecf3;
  height: 50%;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  margin-left: 10px;
  justify-content: space-evenly;
  color: #e4e8f3;
  align-items: center;
  & > button {
    outline: none;
    background: inherit;
    border: 0;
  }
`;
const DropdownList = styled.div`
  position: absolute;
  background: white;
  width: 100%;
  top: 50px;
  z-index: 1;
  border: 1px solid #e8ebf3;
  border-radius: 0px 0px 15px 15px;
`;
const DropdownListItem = styled.div`
  padding: 10px;
  border-bottom: 1px solid #e8ebf3;
  color: #3f5aa6;
`;
