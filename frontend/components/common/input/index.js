import React from "react";
import styled from "styled-components";

export const Input = (props) => {
  return <StyledInput {...props} />;
};

const StyledInput = styled.input`
  width: 100%;
  border: ${(props) => props.theme.style.input.border};
  background-color: ${(props) => props.theme.style.input.background};
  font-family: ${(props) => props.theme.style.input.fontFamily};
  color: ${(props) => props.theme.style.input.color};
  line-height: ${(props) => props.theme.style.input.lineHeight};
  font-weight: ${(props) => props.theme.style.input.weight};
  font-size: ${(props) => props.theme.style.input.size};
  outline: none;
  padding: 11px;
  transition: 0.5s;
  & :hover {
    box-shadow: ${(props) => props.theme.style.input.shadow};
  }
`;
