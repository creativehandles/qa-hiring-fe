import React from "react";
import styled from "styled-components";

export const Button = ({ children, title, ...props }) => {
  return (
    <StyledButton {...props}>
      {title}
      {children}
    </StyledButton>
  );
};

const StyledButton = styled.button`
  font-family: ${(props) => props.theme.style.button.fontFamily};
  padding: ${(props) => props.theme.style.button.padding};
  border-radius: ${(props) => props.theme.style.button.borderRadius};
  width: 100%;
  max-width: ${(props) => props.theme.style.button.width};
  background: ${(props) =>
    props.disabled
      ? props.theme.style.button.colorDisabled
      : props.theme.style.button.background};
  outline: none;
  color: ${(props) => props.theme.style.button.color};
  font-size: ${(props) => props.theme.style.button.fontSize};
  font-weight: ${(props) => props.theme.style.button.fontWeight};
  text-align: center;
  border: ${(props) =>
    props.disabled
      ? props.theme.style.button.border
      : props.theme.style.button.borderDisabled};
  cursor: ${(props) => (!props.disabled ? "pointer" : "not-allowed")};
`;
