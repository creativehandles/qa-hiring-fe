import React from "react";
import styled from "styled-components";
import MultiRange from "../multi-ranger";

export const ManualRanger = ({ title, ...props }) => {
  return (
    <Container {...props}>
      <Title>{title}</Title>
      <MultiRange
        style={{ width: "85%", height: 10, marginRight: 40 }}
      ></MultiRange>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  height: 95px;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  border: 1px solid #ebeef7;
  background-color: #ffffff;
  padding: 20px;
`;
const Title = styled.div`
  color: ${(props) => props.theme.style.global.fontColor};
  text-align: left;
  font-size: 12px;
  font-weight: 800;
  margin-bottom: 20px;
  text-transform: uppercase;
`;
