import React from "react";
import styled from "styled-components";
import {
  Calendar,
  Power,
  Ring,
} from "../../../functional-components/svg-component";

export const ContactDescription = ({ column }) => {
  return (
    <Container>
      <DescriptionItem column={column}>
        <Ring />
        <Text>
          You can change your email preferences or unsubscribe anytime.
        </Text>
      </DescriptionItem>
      <DescriptionItem column={column}>
        <Calendar />
        <Text>
          If you are not looking for a job right away, please select a start
          date.{" "}
        </Text>
      </DescriptionItem>
      <DescriptionItem column={column}>
        <Power />
        <Text>
          You can change your email preferences or unsubscribe anytime.
        </Text>
      </DescriptionItem>
    </Container>
  );
};

const Container = styled.div`
  align-items: flex-start;
  display: grid;
  grid-gap: 32px;
`;
const DescriptionItem = styled.div`
  padding: 10px;
  grid-gap: 20px;
  display: grid;
  align-items: center;
  justify-items: center;
  grid-auto-flow: ${(props) => (props.column ? "row" : "column")};
`;
const Text = styled.div`
  text-align: center;
`;
