import React from "react";
import styled from "styled-components";

export default function Spinner({ height = "25px", width = "25px" }) {
  return <Loader height={height} width={width}></Loader>;
}

const Loader = styled.div`
  border: 2px solid #f3f3f3;
  border-top: 2px solid ${(props) => props.theme.style.global.fontColor};
  border-radius: 50%;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  animation: spin 1s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
