import React from "react";
import addEventListener from "rc-util/lib/Dom/addEventListener";
import styled from "styled-components";

const tabWidthPercentage = 6;
const tabHeight = 30;

export default class Handle extends React.Component {
  state = {
    clickFocused: false,
  };

  onMouseUpListener;

  handle;

  componentDidMount() {
    // mouseup won't trigger if mouse moved out of handle,
    // so we listen on document here.
    this.onMouseUpListener = addEventListener(
      document,
      "mouseup",
      this.handleMouseUp
    );
  }

  componentWillUnmount() {
    if (this.onMouseUpListener) {
      this.onMouseUpListener.remove();
    }
  }

  setHandleRef = (node) => {
    this.handle = node;
  };

  setClickFocus(focused) {
    this.setState({ clickFocused: focused });
  }

  handleMouseUp = () => {
    if (document.activeElement === this.handle) {
      this.setClickFocus(true);
    }
  };

  handleMouseDown = (e) => {
    // avoid selecting text during drag
    // https://github.com/ant-design/ant-design/issues/25010
    e.preventDefault();
    // fix https://github.com/ant-design/ant-design/issues/15324
    this.focus();
  };

  handleBlur = () => {
    this.setClickFocus(false);
  };

  handleKeyDown = () => {
    this.setClickFocus(false);
  };

  clickFocus() {
    this.setClickFocus(true);
    this.focus();
  }

  focus() {
    this.handle.focus();
  }

  blur() {
    this.handle.blur();
  }

  render() {
    const {
      prefixCls,
      vertical,
      reverse,
      offset,
      style,
      disabled,
      min,
      max,
      value,
      tabIndex,
      ariaLabel,
      ariaLabelledBy,
      ariaValueTextFormatter,
      ...restProps
    } = this.props;

    const positionStyle = vertical
      ? {
          [reverse ? "top" : "bottom"]: `${offset}%`,
          [reverse ? "bottom" : "top"]: "auto",
          transform: reverse ? null : `translateY(+50%)`,
        }
      : {
          [reverse ? "right" : "left"]: `${offset}%`,
          [reverse ? "left" : "right"]: "auto",
          transform: `translateX(${reverse ? "+" : "-"}50%)`,
        };
    const elStyle = {
      ...style,
      ...positionStyle,
    };

    let mergedTabIndex = tabIndex || 0;
    if (disabled || tabIndex === null) {
      mergedTabIndex = null;
    }

    let ariaValueText;
    if (ariaValueTextFormatter) {
      ariaValueText = ariaValueTextFormatter(value);
    }

    const removedClassProps = restProps;
    if (removedClassProps.className) {
      delete removedClassProps.className;
    }

    let handleLabel = "";
    if (this.props.handleLabels) {
      handleLabel = this.props.handleLabels[offset];
    }

    return (
      <HandleComponent
        ref={this.setHandleRef}
        tabIndex={mergedTabIndex}
        {...removedClassProps}
        style={elStyle}
        onBlur={this.handleBlur}
        onKeyDown={this.handleKeyDown}
        onMouseDown={this.handleMouseDown}
        role="slider"
      >
        {handleLabel}
      </HandleComponent>
    );
  }
}

const HandleComponent = styled.div`
  z-index: 100;
  cursor: grab;
  top: -13px;
  width: ${tabWidthPercentage}%;
  height: ${tabHeight}px;
  border-radius: 3px;
  position: absolute;
  background-color: ${(props) =>
    props.disabled ? "#fff" : props.theme.style.filters.rangeFilter.thumbFill};
  &:focus {
    outline: none;
  }
`;
