import React from "react";
import styled from "styled-components";

const Track = (props) => {
  const { included, vertical, style } = props;
  let { length, offset, reverse } = props;
  if (length < 0) {
    reverse = !reverse;
    length = Math.abs(length);
    offset = 100 - offset;
  }

  const positonStyle = vertical
    ? {
        [reverse ? "top" : "bottom"]: `${offset}%`,
        [reverse ? "bottom" : "top"]: "auto",
        height: `${length}%`,
      }
    : {
        [reverse ? "right" : "left"]: `${offset}%`,
        [reverse ? "left" : "right"]: "auto",
        width: `${length}%`,
      };

  const elStyle = {
    ...style,
    ...positonStyle,
  };
  return included ? <TrackContainer style={elStyle} /> : null;
};

const TrackContainer = styled.div`
  position: absolute;
  left: 0;
  height: 4px;
  border-radius: 6px;
  background-color: ${(props) =>
    props.theme.style.filters.rangeFilter.rangeColor};
  z-index: 1;
`;

export default Track;
