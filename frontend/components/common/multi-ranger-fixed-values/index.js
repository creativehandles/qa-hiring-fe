import React from "react";
import Range from "./Range/Range";
import styled from "styled-components";

// Note:
// fixedValues is an array of possible labels.
// E.g. ["Today", "Yesterday", "Last week", "Last Month", "Unlimited"]
//
// However, the Range control accepts a handleLabels Object
// that contains the labels, indexed by their position between 0 and 100. E.g. :
//
// {
//   0: "Today",
//   25: "Yesterday",
//   50: "Last week",
//   75: "Last Month",
//   100: "Unlimited",
// }
//
export default function MultiRangeFixedValues({ data, onChange, fixedValues }) {
  const fixedValuesObject = {};

  if (!fixedValues.length || fixedValues.length < 2) {
    throw new Error("fixedValues must have min length 2");
  }

  const stepSize = 100 / (Object.keys(fixedValues).length - 1);
  fixedValues.forEach((value, index) => {
    fixedValuesObject[index * stepSize] = value;
  });

  const onValuesChangedInternal = (values) => {
    if (Array.isArray(values) && values.length === 2) {
      const lowValue = fixedValuesObject[values[0]];
      const highValue = fixedValuesObject[values[1]];
      if (lowValue && highValue) {
        onChange([lowValue, highValue]);
      }
    }
  };

  const convertedValues = data.map((label) => {
    const keys = Object.keys(fixedValuesObject);
    const value = keys.find((key) => fixedValuesObject[key] === label);
    return value;
  });

  return (
    <>
      <MultiRangeFixedValuesContainer>
        <Range
          step={stepSize}
          value={convertedValues}
          onChange={onValuesChangedInternal}
          allowCross={false}
        ></Range>
      </MultiRangeFixedValuesContainer>
      {Array.isArray(data) && data.length === 2 && (
        <RangeLabels>{`${data[0]} - ${data[1]}`}</RangeLabels>
      )}
    </>
  );
}

const MultiRangeFixedValuesContainer = styled.div`
  position: relative;
  top: 1px;
  width: 95%;
  margin: auto;
`;

const RangeLabels = styled.div`
  margin-top: 7px;
  width: 100%;
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 14px;
  font-weight: 400;
  text-align: center;
`;
