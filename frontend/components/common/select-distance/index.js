import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Drop } from "../../../functional-components/svg-component";

export const SelectDistance = ({
  data,
  onChange = () => {},
  defaultValue,
  width,
}) => {
  const [value, setValue] = useState(defaultValue);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    onChange(value);
  }, [value]);

  const listOfValues = data.map((el, index) => {
    return (
      <DropdownListItem
        key={index}
        onClick={() => {
          setValue(el);
          setIsOpen(!isOpen);
        }}
      >
        {el}
      </DropdownListItem>
    );
  });

  const showValue = value ? <DropdownValue>{value}</DropdownValue> : "";

  return (
    <Dropdown width={width}>
      <DropdownContainer>
        {showValue}
        {isOpen && <DropdownList>{listOfValues}</DropdownList>}
      </DropdownContainer>
      <DropButtton onClick={() => setIsOpen(!isOpen)}>
        <Drop />
      </DropButtton>
    </Dropdown>
  );
};

const Dropdown = styled.div`
  height: 50px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  border: 1px solid #e8ebf3;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  width: 38%;
`;
const DropdownContainer = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  height: 100%;
  align-items: center;
`;
const DropButtton = styled.button`
  width: 40%;
  outline: none;
  border: 0;
  background: inherit;
  height: 100%;
  border-left: 1px solid #e8ebf3;
`;
const DropdownValue = styled.div`
  display: flex;
  padding: 5px;
  width: 45%;
  height: 50%;
  border-radius: ${(props) => props.theme.style.global.borderRadius};
  margin-left: 10px;
  justify-content: space-evenly;
  color: #3f5aa6;
  align-items: center;
  & > button {
    outline: none;
    background: inherit;
    border: 0;
  }
`;
const DropdownList = styled.div`
  position: absolute;
  background: white;
  width: 100%;
  top: 50px;
  z-index: 1;
  border: 1px solid #e8ebf3;
  border-radius: 0px 0px 15px 15px;
`;
const DropdownListItem = styled.div`
  padding: 10px;
  border-bottom: 1px solid #e8ebf3;
  color: #3f5aa6;
`;
