import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";

const tabWidthPercentage = 6;
const tabHeight = 30;

export default function MultiRange({
  min = 0,
  max = 100,
  disabled,
  onChange = () => {},
  data,
}) {
  const [inputWidth, setInputWidth] = useState("");
  const [firstXPosition, setFirstXPosition] = useState(0);
  const [secondXPosition, setSecondXPosition] = useState(null);
  const [vizOne, setVizOne] = useState(2);
  const [vizTwo, setVizTwo] = useState(2);
  const target = useRef();
  const [width, setWidth] = useState();
  const firstValue = data && data.length === 2 ? data[0] : min;
  const secondValue = data && data.length === 2 ? data[1] : max;
  const prevFirstValue = useRef(firstValue);
  const prevSecondValue = useRef(firstValue);

  useEffect(() => {
    setWidth(target.current.clientWidth);
  }, []);

  useEffect(() => {
    // update only when something like a external change of value is done
    if (
      prevFirstValue.current !== firstValue ||
      prevSecondValue.current !== secondValue
    ) {
      setFirstXPosition((target.current.clientWidth * firstValue) / 100);
      setSecondXPosition((target.current.clientWidth * secondValue) / 100);
      prevFirstValue.current = firstValue;
      prevSecondValue.current = secondValue;
    }
  }, [firstValue, secondValue]);


  useEffect(() => {
    setInputWidth(secondXPosition - firstXPosition);
    const range = max - min;
    const calcFirstVal = Math.ceil(firstXPosition / (width / range) + min);
    const calcSecondVal = Math.ceil(secondXPosition / (width / range) + min);

    if (calcSecondVal >= 0 && calcFirstVal >= 0) {
      prevFirstValue.current = calcFirstVal;
      prevSecondValue.current = calcSecondVal;
      onChange([calcFirstVal, calcSecondVal]);
    }
  }, [firstXPosition, secondXPosition, min, max]);


  const moveFirst = (e) => {
    setVizOne(3);
    setVizTwo(2);
    const img = new Image();
    img.src = " ";
    e.dataTransfer.setDragImage(img, 0, 0);
    const leftRange = e.target.parentNode.getBoundingClientRect().left;
    if (e.pageX - leftRange < secondXPosition) {
      if (e.pageX - leftRange < 0) {
        setFirstXPosition(0);
      } else setFirstXPosition(e.pageX - leftRange);
    } else {
      setFirstXPosition(secondXPosition);
    }
  };

  const moveSecond = (e) => {
    setVizOne(2);
    setVizTwo(3);
    const img = new Image();
    img.src = " ";
    e.dataTransfer.setDragImage(img, 0, 0);

    const leftRange = Math.floor(
      e.target.parentNode.getBoundingClientRect().left
    );
    if (e.pageX - leftRange >= firstXPosition) {
      const secondXPos =
        e.pageX - leftRange >= width ? width : e.pageX - leftRange;
      setSecondXPosition(secondXPos);
    }
  };

  const touchMove = (e) => {
    setVizOne(3);
    setVizTwo(2);
    e.target.style.cursor = "move";
    const leftRange = e.target.parentNode.getBoundingClientRect().left;
    const pageCoordinate = e.changedTouches[0].clientX;
    if (pageCoordinate - leftRange < secondXPosition) {
      if (pageCoordinate - leftRange < 0) {
        setFirstXPosition(0);
      } else setFirstXPosition(pageCoordinate - leftRange);
    } else {
      setFirstXPosition(secondXPosition);
    }
  };
  const touchMoveSecond = (e) => {
    setVizOne(2);
    setVizTwo(3);
    e.target.style.cursor = "grab";
    const leftRange = Math.floor(
      e.target.parentNode.getBoundingClientRect().left
    );
    const pageCoordinate = e.changedTouches[0].clientX;
    if (
      pageCoordinate - leftRange >= firstXPosition &&
      pageCoordinate - leftRange <= width
    ) {
      setSecondXPosition(pageCoordinate - leftRange);
    }
  };

  return (
    <>
      <Container ref={target}>
        <FirstThumb
          disabled={disabled}
          style={{ left: firstXPosition, zIndex: vizOne }}
          draggable={disabled ? "false" : "true"}
          onTouchMove={(e) => touchMove(e)}
          onDragEnd={(e) => moveFirst(e)}
          onDrag={(e) => moveFirst(e)}
          onDragStart={(e) => moveFirst(e)}
        ></FirstThumb>
        <Range
          disabled={disabled}
          style={{ left: firstXPosition, width: inputWidth }}
        ></Range>
        <Rail width={`${width * (1 + tabWidthPercentage / 100)}px`}></Rail>
        <SecondThumb
          disabled={disabled}
          style={{ left: secondXPosition, zIndex: vizTwo }}
          draggable={disabled ? "false" : "true"}
          onTouchMove={(e) => touchMoveSecond(e)}
          onDragEnd={(e) => moveSecond(e)}
          onDrag={(e) => moveSecond(e)}
          onDragStart={(e) => moveSecond(e)}
        ></SecondThumb>
      </Container>
      <RangeLabels>{`${firstValue}% - ${secondValue}%`}</RangeLabels>
    </>
  );
}

const Container = styled.div`
  width: 95%;
  height: 20px;
  pointer-events: all;
  position: relative;
  padding: 0;
  box-sizing: border-box;
  margin: 1px;
  border-radius: 3px;
`;
const FirstThumb = styled.div`
  top: -13px;
  height: ${tabHeight}px;
  width: ${tabWidthPercentage}%;
  border-radius: 3px;
  position: absolute;
  background-color: ${(props) =>
    props.disabled ? "#fff" : props.theme.style.filters.rangeFilter.thumbFill};
`;

const SecondThumb = styled.div`
  top: -13px;
  width: ${tabWidthPercentage}%;
  height: ${tabHeight}px;
  border-radius: 3px;
  position: absolute;
  background-color: ${(props) =>
    props.disabled ? "#fff" : props.theme.style.filters.rangeFilter.thumbFill};
  touch-action: pan-x;
`;

const Range = styled.div`
  position: absolute;
  width: 100%;
  height: 4px;
  background-color: ${(props) =>
    props.theme.style.filters.rangeFilter.rangeColor};
  z-index: 1;
`;

const RangeLabels = styled.div`
  width: 100%;
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: 14px;
  font-weight: 400;
  text-align: center;
`;

const Rail = styled.div`
  position: absolute;
  width: ${(props) => props.width};
  height: 4px;
  background-color: ${(props) =>
    props.theme.style.filters.rangeFilter.railColor};
`;
