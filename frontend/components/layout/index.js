import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Back } from "../../functional-components/svg-component";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { Modal } from "../../functional-components/modal";
import { DesktopModal } from "../../functional-components/desktop-modal";
import { DragAndDropHome } from "../../functional-components/drag-and-drop-home";
import ConfigStyles from "../../utils/config-styles";
import { fetchDefaultVacancies } from "../../redux/slices/vacancies/default/functions";
import { clearCandidate, setCandidate } from "../../redux/slices/candidate";
import { clearOpenVacancies } from "../../redux/slices/vacancies/open-vacancies";
import { getAPIURL } from "../../utils/api-url/index";
import { clearNotification } from "../../redux/slices/notifications";
import { manageUserSession } from "../../utils/session";
import { setUploadedThisSession } from "../../redux/slices/CV";
import { logInfoToConsole } from "../../utils/debug";
import { pushToDataLayer } from "../../utils/gtm";
import useTranslation from "../../hooks/useTranslation";

export const Layout = ({ children, enabledFetchDefaultVacancies }) => {
  const state = useSelector((state) => state);
  const localStyles = ConfigStyles();
  const dispatch = useDispatch();
  const { layout } = useSelector((state) => state.styles.styles);
  const cv = useSelector((state) => state.CV.value);
  const reserveCV = useSelector((state) => state.CV.reserveCV);
  const router = useRouter();
  const isOpportinitiesPage = router.pathname === "/opportunities";
  const isOpportinitiesPageOrSub = router.pathname.includes("/opportunities");
  const openNewWindow = localStyles.openNewWindow;
  const { message, messageId } = useSelector((state) => state.notifications);
  const filterState = useSelector((state) => state.filters);
  const [isNotificationOpen, setIsNotificationOpen] = useState(false);
  const openVacanciesLoading = useSelector(
    (state) => state.openVacancies.loading
  );
  const { googleTagManager } = localStyles;
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(setUploadedThisSession(false));
    manageUserSession();
  }, []);

  useEffect(() => {
    if (openVacanciesLoading) {
      setIsNotificationOpen(false);
      dispatch(clearNotification());
    }
  }, [openVacanciesLoading]);

  useEffect(() => {
    const {
      GAID,
      tenantID,
      tenantGAID,
      tenantFBID,
      tenantLIID,
      GAfourID,
      tenantGAfourID,
      tenantADWpixel,
      tenantADWlabel,
      tenantSklikID,
      tenantSklikKonverze,
    } = googleTagManager;
    if (GAID) {
      // Note: must pass in undefined in case there are blank string values
      // These params can be customizable by JobOne, so we must do this check
      const data = {
        event: "virtual",
        GAID,
        tenantID: tenantID === "" ? undefined : tenantID,
        tenantGAID: tenantGAID === "" ? undefined : tenantGAID,
        tenantFBID: tenantFBID === "" ? undefined : tenantFBID,
        tenantLIID: tenantLIID === "" ? undefined : tenantLIID,
        GAfourID: GAfourID === "" ? undefined : GAfourID,
        tenantGAfourID: tenantGAfourID === "" ? undefined : tenantGAfourID,
        tenantADWpixel: tenantADWpixel === "" ? undefined : tenantADWpixel,
        tenantADWlabel: tenantADWlabel === "" ? undefined : tenantADWlabel,
        tenantSklikID: tenantSklikID === "" ? undefined : tenantSklikID,
        tenantSklikKonverze:
          tenantSklikKonverze === "" ? undefined : tenantSklikKonverze,
      };

      logInfoToConsole("dataLayer.push for: " + router.asPath);
      pushToDataLayer(data);
    }
  }, [router.asPath]);

  useEffect(() => {
    if (!cv) {
      dispatch(clearCandidate());
      dispatch(clearOpenVacancies());
    }
  }, [cv, reserveCV]);

  useEffect(() => {
    if (enabledFetchDefaultVacancies) {
      dispatch(fetchDefaultVacancies({ apiURL: getAPIURL() }));
    }
  }, [filterState]);

  useEffect(() => {
    if (localStorage.getItem("candidate")) {
      dispatch(setCandidate(JSON.parse(localStorage.getItem("candidate"))));
    }
  }, []);

  useEffect(() => {
    let timeout;
    if (message) {
      const currentNotificationId = messageId;
      setIsNotificationOpen(true);
      timeout = setTimeout(() => {
        if (messageId === currentNotificationId) {
          setIsNotificationOpen(false);
          dispatch(clearNotification());
        }
      }, 10000);
    }

    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [messageId]);

  const returnModal = () => {
    switch (state.modal.value) {
      case "gdpr":
        return <Modal>{localStyles.text.agreement}</Modal>;
      case "upload":
        return (
          <DesktopModal>
            <DragAndDropHome onlyCV data={[]} />
          </DesktopModal>
        );

      default:
        return null;
    }
  };

  const showBackButton = () => {
    const pathName =
      router.pathname.includes("/detail") ||
      router.pathname.includes("/opportunities");
    const buttonName = router.pathname.includes("/opportunities")
      ? t("layoutBackButtonOpportunities")
      : t("layoutBackButtonDetail");
    if (pathName) {
      return (
        <BackButton onClick={() => router.push("/")}>
          <Back backColor={layout.backIconColor} />
          <span>{buttonName}</span>
        </BackButton>
      );
    } else {
      return <div></div>;
    }
  };

  return (
    <>
      <NotificationContainer isOpen={isNotificationOpen}>
        {message}
      </NotificationContainer>
      <Container isOpportinitiesPageOrSub={isOpportinitiesPageOrSub}>
        <Header layout={layout}>
          <ShadowContainer
            logoPosition={localStyles.logoPosition}
            isOpportinitiesPageOrSub={isOpportinitiesPageOrSub}
          >
            {showBackButton()}
            <HeaderTitle
              rel="noopener noreferrer"
              target={openNewWindow ? "_blank" : "_self"}
              href={localStyles.logoLink}
            >
              <Logo
                logoPosition={localStyles.logoPosition}
                isOpportinitiesPage={isOpportinitiesPage}
                src={localStyles.logo}
              ></Logo>
            </HeaderTitle>
          </ShadowContainer>
        </Header>
        <ChildrenContainer isOpportinitiesPageOrSub={isOpportinitiesPageOrSub}>
          {children}
        </ChildrenContainer>
        <Footer>
        </Footer>
      </Container>
      {returnModal()}
    </>
  );
};

const Container = styled.div`
  height: 100%;
  display: ${(props) => (props.isOpportinitiesPageOrSub ? "flex" : "")};
  flex-direction: column;
  overflow-y: ${(props) =>
    props.isOpportinitiesPageOrSub ? "hidden" : "auto"};
  background: ${(props) => props.theme.style.global.background};
  color: ${(props) => props.theme.style.global.fontColor};
  font-size: ${(props) => props.theme.style.global.fontSize};
  font-weight: ${(props) => props.theme.style.global.fontWeight};
  line-height: ${(props) => props.theme.style.global.lineHeight};
`;
const Footer = styled.footer`
  text-align: center;
  color: ${(props) => props.theme.style.global.footerColor || "#828899"};
  font-size: 14px;
  font-weight: 500;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
  & > a {
    text-decoration: none;
  }
`;

const ShadowContainer = styled.div`
  width: 100%;
  align-items: center;
  display: flex;
  flex-direction: ${(props) =>
    props.logoPosition === "reverse" ? "row-reverse" : "row"};
  padding: ${(props) =>
    props.theme.style.global.shadowContainerPaddingMobile
      ? props.theme.style.global.shadowContainerPaddingMobile
      : "15px 20px"};
  max-width: ${(props) =>
    props.isOpportinitiesPageOrSub ? "1160px" : "1143px"};
  margin: 0 auto;
  background: inherit;
  justify-content: space-between;
  height: 100%;
  @media (max-width: 1400px) and (min-width: 850px) {
    padding: ${(props) =>
      props.theme.style.global.shadowContainerPadding
        ? props.theme.style.global.shadowContainerPadding
        : "15px 60px"};
  }
`;

const Header = styled.header`
  width: 100%;
  flex-shrink: 0;
  box-shadow: ${(props) => props.theme.style.global.headerBoxShadow};
  z-index: 9999;
  position: relative;
  background-color: ${(props) =>
    props.layout?.headerBackgroundColor || "inherit"};
  height: ${(props) => props.theme.style.global.headerHeight || "auto"};
  @media (max-width: 850px) {
    height: ${(props) => props.theme.style.global.headerMobileHeight || "auto"};
  }
`;
const HeaderTitle = styled.a`
  cursor: ${(props) => (props.href ? "pointer" : "none")};
  pointer-events: ${(props) => (props.href ? "all" : "none")};
  color: ${(props) => props.theme.style.global.fontColor};
  display: inline;
  justify-self: end;
`;

const ChildrenContainer = styled.div`
  height: ${(props) => (props.isOpportinitiesPageOrSub ? "100%" : "")};
`;

const BackButton = styled.button`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-template-rows: 1fr;
  outline: none;
  cursor: pointer;
  border: none;
  background: inherit;
  text-transform: uppercase;
  color: ${(props) => props.theme.style.global.backButtonColor};
  font-weight: ${(props) => props.theme.style.global.backBottonWeight};
  font-size: ${(props) => props.theme.style.global.backButtonSize};
  letter-spacing: ${(props) =>
    props.theme.style.global.backButtonLetterSpacing};
  transition: 1s;
  gap: ${(props) =>
    props.theme.style.global.backButtonGap
      ? props.theme.style.global.backButtonGap
      : 0};

  & svg {
    align-self: center;
  }

  & span {
    align-self: center;
    margin-top: ${(props) =>
      props.theme.style.global.backButtonSpanMarginTop
        ? props.theme.style.global.backButtonSpanMarginTop
        : 0};
  }
`;
const Logo = styled.img`
  display: flex;
  width: ${(props) => props.theme.style.logo.width};
  height: ${(props) => props.theme.style.logo.height};
  align-items: center;
  justify-content: ${(props) =>
    props.logoPosition === "reverse" ? "flex-start" : "flex-end"};
  @media (max-width: 850px) {
    width: ${(props) => props.theme.style.logo.mobileWidth};
    height: ${(props) => props.theme.style.logo.mobileHeight};
  }
`;

export const NotificationContainer = styled.div`
  width: 100%;
  max-width: 320px;
  background: ${(props) => props.theme.style.global.background};
  color: ${(props) => props.theme.style.global.fontColor};
  font-weight: 600;
  border-radius: 4px;
  position: fixed;
  right: 0%;
  top: 10%;
  padding: 30px;
  align-items: center;
  display: flex;
  z-index: 100;
  transition: 1s;
  transform: translateX(${(props) => (props.isOpen ? "0%" : "100%")});
`;
export const ErrorMessage = styled.div`
  margin-left: 10px;
`;
