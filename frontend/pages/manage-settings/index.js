import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Layout } from "../../components/layout";
import { renderSettingsPage } from "../../utils/render";

const ManageSettings = () => {
  const globalStyles = useSelector((state) => state.styles.styles);
  const theme = { style: globalStyles.styles };
  const [screenWidth, setScreenWidth] = useState();
  useEffect(() => {
    setScreenWidth(window.innerWidth);
    const setScreenSize = (e) => {
      setScreenWidth(e.currentTarget.innerWidth);
    };

    window.addEventListener("resize", setScreenSize);
    return () => {
      window.removeEventListener("resize", setScreenSize);
    };
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <Layout>
        {globalStyles.settingsOrder.map((el) =>
          renderSettingsPage(el, screenWidth)
        )}
      </Layout>
    </ThemeProvider>
  );
};

export default ManageSettings;
