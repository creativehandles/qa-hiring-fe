import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Layout } from "../../../../components/layout";
import { fetchCurrentVacancy } from "../../../../redux/slices/vacancies/current_vacancy/functions";
import { renderDetail } from "../../../../utils/render";
import { clearMessage } from "../../../../redux/slices/vacancies/current_vacancy/index";
import { getAPIURL } from "../../../../utils/api-url/index";

const DetailPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { value, message } = useSelector((state) => state.vacancy);
  const globalStyles = useSelector((state) => state.styles.styles);
  const dispatch = useDispatch();
  useEffect(() => {
    if (id && isNaN(Number(id))) router.push("/");
    if (Number.isInteger(Number(id))) {
      dispatch(fetchCurrentVacancy({ id, apiURL: getAPIURL() }));
    }
  }, [id]);

  useEffect(() => {
    if (message === "Error") {
      router.push("/");
      dispatch(clearMessage());
    }
  }, [message]);

  const theme = { style: globalStyles.styles };
  return (
    <ThemeProvider theme={theme}>
      <Head>
        <title>{value.name}</title>
        <meta name="description" content={value.meta_description} />
        <meta name="tile" content={value.meta_title} />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, viewport-fit=cover"
        />
      </Head>
      <Layout>
        {globalStyles.componentsOrderDetail.map((el) =>
          renderDetail(el, value)
        )}
      </Layout>
    </ThemeProvider>
  );
};

export default DetailPage;
