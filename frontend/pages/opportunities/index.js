import React from "react";
import { ThemeProvider } from "styled-components";
import { Layout } from "../../components/layout";
import { renderOpportunities } from "../../utils/render";
import Head from "next/head";
import { useSelector } from "react-redux";

const OpportunitiesPage = () => {
  const globalStyles = useSelector((state) => state.styles.styles);
  const tags = {
    ...globalStyles.metaTags.Home,
    ...globalStyles.metaTags.opportunities,
  };
  const theme = {
    style: globalStyles.styles,
  };
  // eslint-disable-next-line no-undef
  const href = process.env.NEXT_PUBLIC_COMPANY_FE_URL;

  return (
    <>
      <Head>
        <title>{tags.title}</title>
        <meta name="description" content={tags.description} />
        <meta name="title" content={tags.title} />
        {href && tags.ogImage && (
          <meta name="og:image" content={href + tags.ogImage} />
        )}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, viewport-fit=cover"
        />
      </Head>
      <ThemeProvider theme={theme}>
        <Layout>
          {globalStyles.opportunitiesOrder.map((opp, index) =>
            renderOpportunities(opp, index)
          )}
        </Layout>
      </ThemeProvider>
    </>
  );
};

export default OpportunitiesPage;
