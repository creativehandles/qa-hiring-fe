import React, { useEffect } from "react";
import { ThemeProvider } from "styled-components";
import { Layout } from "../../../../components/layout";
import { renderOpportunities } from "../../../../utils/render";
import Head from "next/head";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { fetchCurrentVacancy } from "../../../../redux/slices/vacancies/current_vacancy/functions";
import {
  clearMessage,
  setCurId,
} from "../../../../redux/slices/vacancies/current_vacancy";
import { getAPIURL } from "../../../../utils/api-url/index";

const VacancyPage = () => {
  const { value, message, curId } = useSelector((state) => state.vacancy);
  const router = useRouter();
  const dispatch = useDispatch();
  const { id } = router.query;
  const globalStyles = useSelector((state) => state.styles.styles);
  const theme = {
    style: globalStyles.styles,
  };

  useEffect(() => {
    id && dispatch(setCurId(id));
  }, [id]);

  useEffect(() => {
    if (curId) {
      if (isNaN(Number(curId))) router.push("/");
      if (Number.isInteger(Number(curId))) {
        dispatch(fetchCurrentVacancy({ id, apiURL: getAPIURL() }));
      }
    }
  }, [curId]);

  useEffect(() => {
    if (message === "Error") {
      router.push("/");
      dispatch(clearMessage());
    }
  }, [message]);

  return (
    <>
      <Head>
        <title>{value?.name}</title>
        <meta name="description" content={value?.meta_description} />
        <meta name="tile" content={value?.meta_title} />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, viewport-fit=cover"
        />
      </Head>
      <ThemeProvider theme={theme}>
        <Layout>
          {globalStyles.opportunitiesOrder.map(renderOpportunities)}
        </Layout>
      </ThemeProvider>
    </>
  );
};

export default VacancyPage;
