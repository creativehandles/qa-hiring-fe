import { ThemeProvider } from "styled-components";
import { useState, useEffect } from "react";
import Head from "next/head";
import { useSelector } from "react-redux";

import { renderUpload } from "../utils/render";
import { Layout } from "../components/layout";

function Home() {
  const globalStyles = useSelector((state) => state.styles.styles);
  const theme = { style: globalStyles.styles };
  const [screenWidth, setScreenWidth] = useState();
  const tags = globalStyles.metaTags.Home;
  const order = globalStyles.componentsOrderHome;
  // eslint-disable-next-line no-undef
  const href = process.env.NEXT_PUBLIC_COMPANY_FE_URL;

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    const setScreenSize = (e) => {
      setScreenWidth(e.currentTarget.innerWidth);
    };

    window.addEventListener("resize", setScreenSize);
    return () => {
      window.removeEventListener("resize", setScreenSize);
    };
  }, []);

  return (
    <>
      <Head>
        <title>{tags.title}</title>
        <meta name="description" content={tags.description} />
        <meta name="title" content={tags.title} />
        {href && tags.ogImage && (
          <meta name="og:image" content={href + tags.ogImage} />
        )}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, viewport-fit=cover"
        />
      </Head>
      <ThemeProvider theme={theme}>
        {screenWidth ? (
          <Layout width={screenWidth}>
            {order.map((el, index) => renderUpload(el, screenWidth, index))}
          </Layout>
        ) : null}
      </ThemeProvider>
    </>
  );
}

export default Home;
