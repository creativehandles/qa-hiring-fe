import store from "../redux";
import { Provider } from "react-redux";
import ConfigStyles from "../utils/config-styles";
import { createGlobalStyle } from "styled-components";
import Head from "next/head";

const localStyles = ConfigStyles();
const { webFonts } = localStyles.fonts;
const { icon, defaultFont } = localStyles;
const returnFonts = () => {
  let customFonts = "";
  let mainFont;
  webFonts.forEach((el, index) => {
    const returnCustomFonts = () => {
      let links = "";
      el.links.forEach((link) => {
        links += `
        @font-face {
          font-family: '${el.name}';
          ${link}
        }        
        `;
      });
      return links;
    };
    if (index === 0) {
      mainFont = el.name;
    }
    customFonts += returnCustomFonts();
  });
  return { customFonts, mainFont };
};

const GlobalStyle = createGlobalStyle`
${returnFonts().customFonts}
  * {
    font-family: ${defaultFont};
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    min-height: 0; /* This is required for flex to work properly */
  }

  html,body {
    padding: 0;
    margin: 0;
    height: 100%;
  }

  body {
    #__next {
      height: 100%;
    }
  }

  a {
    color: inherit;
    text-decoration: underline;
  }

  ul {
     list-style: none;
   }

   /* This is required for next/image used in the project */
  .image-custom {
    width: 100% !important;
    position: relative !important;
    height: unset !important;
  } 
`;

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <GlobalStyle />
      <Head>
        <link rel="shortcut icon" href={icon} />
      </Head>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
