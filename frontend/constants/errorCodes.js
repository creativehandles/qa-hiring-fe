export const errorStatusCodesUploadCV = [
  { code: "default", key: "somethingWentWrong" },
  { code: "400", key: "uploadCVErrorCode400Text" },
  { code: "408", key: "uploadCVErrorCode408Text" },
  { code: "500", key: "uploadCVErrorCode500Text" },
];
