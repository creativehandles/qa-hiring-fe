export const mobileBreakpoint = 850;

export const linkedInRegex = new RegExp(
  "^https?://((www|ww).)?linkedin.com/((in/[^/]+/?)|(pub/[^/]+/((w|d)+/?){3}))$"
);

export const emailRegex = new RegExp(/^\S+@\S+\.\S+$/);

export const telephoneRegex = new RegExp(
  "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$"
);
