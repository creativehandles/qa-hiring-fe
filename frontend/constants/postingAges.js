export const postingAges = [
  { label: "Today", dbValue: "today" },
  { label: "Yesterday", dbValue: "yesterday" },
  { label: "Last week", dbValue: "last_week" },
  { label: "Last Month", dbValue: "last_month" },
  { label: "Unlimited", dbValue: "unlimited" },
];
