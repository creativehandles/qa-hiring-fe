import { useContext } from "react";
import { LanguageContext } from "../context/LanguageContext";
import { getLanguage } from "../utils/translations";

// The function 't' returns the translated value for a key passed in
export default function useTranslation() {
  const { localization } = useContext(LanguageContext);

  function replacePlaceholders(translation, options) {
    let retVal = translation;

    // Replace the object name occurences in translation with Object value
    if (options && typeof options === "object") {
      Object.entries(options).forEach((option) => {
        const regexPattern = new RegExp(`{{\\s*${option[0]}\\s*}}`, "g");
        retVal = retVal.replaceAll(regexPattern, option[1]);
      });
    }
    return retVal;
  }

  function t(key, options) {
    if (!key) {
      return "";
    }

    // Give priority to specificTranslations
    if (localization.specificTranslations[key]) {
      return replacePlaceholders(
        localization.specificTranslations[key],
        options
      );
    }

    if (localization.commonTranslations[key]) {
      return replacePlaceholders(localization.commonTranslations[key], options);
    }

    console.warn(
      `Translation '${key}' for locale '${getLanguage()}' not found.`
    );
    return key;
  }
  return {
    t,
  };
}
