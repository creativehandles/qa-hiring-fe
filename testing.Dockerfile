FROM ubuntu:20.04

# Set Environment Variables
#
# set bash commands to non-interactive mode
ARG DEBIAN_FRONTEND=noninteractive
# set Timezone
ARG TZ=${TZ}

# install packages
RUN LC_ALL=en_US.UTF-8 apt-get update \
    && apt-get upgrade -y \
    # install applications
    && apt-get install -y --no-install-recommends software-properties-common \
    curl \
    gettext-base \
    language-pack-en-base \
    openssl \
    vim \
    zip unzip \
    # install apache2 mods
    && apt-get install -y --no-install-recommends libapache2-mod-php7.4 \
    # install PHP and PHP libraries
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/apache2 \
    && apt-get update \
    && apt-get install -y --no-install-recommends php7.4-fpm \
    php7.4-bcmath \
    php7.4-ctype \
    php7.4-curl \
    php7.4-fileinfo \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-memcached \
    php7.4-mysql \
    php7.4-pdo \
    php7.4-redis \
    php7.4-tidy \
    php7.4-tokenizer \
    php7.4-xml \
    php7.4-xmlrpc \
    php7.4-zip

# do the cleaning and configurations
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && sed -i -e "s/;\?daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.4/fpm/php-fpm.conf \
    && mkdir -p /run/php/ \
    # install composer
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.0.6 \
    # add user group and user
    && groupadd -g 1000 www \
    && useradd -u 1000 -ms /bin/bash -g www www \
    # create log directories and log files
    && touch /var/log/php7.4-fpm.log

# copy configurations
#
# copy PHP pool config
COPY ./docker/config/www.pool.conf /etc/php/7.4/fpm/pool.d/www.conf

# set work directory
WORKDIR /var/www

# copy backend
COPY ./backend /var/www/backend

# run backend build scripts
#
# set working directory to backend
WORKDIR /var/www/backend

# make .env file and add parameters
RUN touch .env && \
    echo "APP_ENV=testing" >> /var/www/backend/.env && \
    echo "APP_DEBUG=false" >> /var/www/backend/.env && \
    echo "LOG_CHANNEL=daily" >> /var/www/backend/.env && \
    echo "TELESCOPE_ENABLED=false" >> /var/www/backend/.env && \
    echo "SESSION_DRIVER=file" >> /var/www/backend/.env && \
    echo "SESSION_LIFETIME=2880" >> /var/www/backend/.env && \
    echo "PERSONAL_ACCESS_CLIENT_ID=" >> /var/www/backend/.env && \
    echo "PERSONAL_ACCESS_CLIENT_SECRET=" >> /var/www/backend/.env && \
    echo "PASSWORD_GRANT_CLIENT_ID=" >> /var/www/backend/.env && \
    echo "PASSWORD_GRANT_CLIENT_SECRET=" >> /var/www/backend/.env && \
    echo "MAIL_FROM_NAME=\"\${APP_NAME}\"" >> /var/www/backend/.env && \
# file system configurations
    echo "ALLOWED_FILE_EXTENSIONS=" >> /var/www/backend/.env && \
    echo "ALLOWED_FILE_CATEGORIES=\"temporary,avatar,cv\"" >> /var/www/backend/.env && \
    echo "DEFAULT_RECORDS_PER_PAGE=10" >> /var/www/backend/.env && \
# this param is from Job#1 Demo to set the grace period before deleting a candidate who's CV failed to get parsed by mHub
    echo "DELETE_FAILED_CANDIDATES_AFTER_HOURS=1" >> /var/www/backend/.env && \
# https://www.php.net/manual/en/dateinterval.construct.php
    echo "GDPR_AGREED_PERIOD=P10Y" >> /var/www/backend/.env && \
# https://www.php.net/manual/en/dateinterval.construct.php
    echo "GDPR_DISAGREED_PERIOD=P3M" >> /var/www/backend/.env && \
# cron notation. See https://ole.michelsen.dk/blog/schedule-jobs-with-crontab-on-mac-osx/
    echo "GET_VACANCIES_FROM_MHUB_FREQUENCY=\"*/30 * * * *\"" >> /var/www/backend/.env && \
# system configurations - notification configurations
    echo "NOTIFICATIONS_SEND_EMAIL=true" >> /var/www/backend/.env && \
    echo "NOTIFICATIONS_SEND_SMS=false" >> /var/www/backend/.env && \
    echo "NOTIFICATIONS_SEND_SLACK=false" >> /var/www/backend/.env && \
    echo "NOTIFICATIONS_SMS_SERVICE_PROVIDER=\"\"" >> /var/www/backend/.env && \
# this param sets the grace period before deleting incomplete candidates. i.e. candidates without an email.
    echo "COMPANY_DELETE_INCOMPLETE_CANDIDATES_OLDER_THAN=24" >> /var/www/backend/.env

# set permissions to backend directory
RUN chown -R www:www /var/www/backend

# set user to www
USER www

# build backend
ARG APP_ENV=testing
RUN composer install


# set user to root
USER root

# set working directory back to www
WORKDIR /var/www

# copy entrypoint
COPY docker/entrypoint.testing.sh /entrypoint.sh
RUN ["chmod", "+x", "/entrypoint.sh"]

ENTRYPOINT ["/entrypoint.sh"]