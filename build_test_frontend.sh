#!/usr/bin/bash

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

echo 'Building frontend'

cd "${PROJECT_DIRECTORY}/frontend"
rm -rf out
npm install
npm run build --styles="${TENANT}" --apiurl="${BACKEND_URL}/api/v1/" --targetdir="${TENANT}" --company_fe_url="${FRONTEND_URL}/" --language="${LOCALE}" --gtm_id="${GTM_ID}"
ln -s "builds/${TENANT}" out
chown -R www:www out
cp .htaccess.example out/.htaccess
