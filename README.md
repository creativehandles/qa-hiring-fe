## How to setup the application with Docker

### Clone the repository

1. Run `git clone git@bitbucket.org:username/job-one-career-pages.git` to clone the git repository.
1. Run `cd job-one-career-pages` to go into the cloned directory.

### Create the databases

1. Create a database for production environment with `utf8mb4` charset and `utf8mb4_general_ci` collation.
1. Create another database for testing environment with `utf8mb4` charset and `utf8mb4_general_ci` collation.

### Build the Docker images

1. There are two Dockerfiles available to build 2 Docker images.
   1. testing.Dockerfile to build the testing image
   1. production.Dockerfile to build the production image
1. Below environment variables should be available in your shell environment to build above Docker images.
   1. COMPOSE_PROJECT_NAME
   1. DOCKER_HOST_IP
   1. PHP_IDE_CONFIG
   1. APP_TIMEZONE
   1. TENANTS
   1. API_URLS
   1. FE_URLS
1. Or else you can make a copy of `.docker.env.example` file and rename it as `.env`. Run `cp .docker.env.example .env`.
   Docker compose will retrieve the environment variables from the `.env` file if they are not available in the shell environment.
   Above variables are explained in the `.docker.env.example`.
1. Run `docker-compose build --force-rm testing` to build the testing image.
1. Run `docker-compose build --force-rm production` to build the production image.

### Create and run a Docker container from the testing image

1. Below environment variables should be available in your shell environment to run a Docker container from the testing image.
   1. APP_NAME `// name of the backend application`
   1. APP_KEY `// Key used for ecryption and decryption. Should be 32 characters long. Should not be changed once set.`
   1. APP_URL `// URL of the backend application. Include the protocol of the URL. i.e. http:// or https://`
   1. APP_TIMEZONE `// timezone of the application. See https://www.php.net/manual/en/timezones.php`
   1. APP_LOCALE `// locale of the application in ISO 639-1 format. See https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes.`
   1. DB_HOST `// host address of the database service`
   1. DB_PORT `// TCP port of the database service`
   1. DB_DATABASE `// name of the database`
   1. DB_USERNAME `// username of the user in the database service`
   1. DB_PASSWORD `// password of the user in the database service`
   1. MHUB_BASEURL `// URL of the mHub API`
   1. SUPER_ADMIN_EMAIL `email of the super admin`
   1. SUPPORT_EMAIL `// email of the technical support`
   1. COMPANY_NAME `name of the company the application is installed for`
   1. COMPANY_CONTACT_EMAIL `// contact email of the company`
   1. COMPANY_FE_URL `// URL of the FE website of the company without the protocol`
   1. COMPANY_MHUB_USERNAME `// mHub username issued for the company`
   1. COMPANY_MHUB_PASSWORD `// mHub password issued for the company`
   1. PASSPORT_PRIVATE_KEY `// RSA private key used for Laravel Passport. Should not be changed once set.`
   1. PASSPORT_PUBLIC_KEY `// RSA public key used for Laravel Passport. Should not be changed once set.`
1. Or else you can use the `.env` file created in the previous section and set values to above environment variables in it.
1. Run `docker-compose run testing` to run the start and run a Docker container. A Docker container will start and run the
   unit tests. And will terminate after running the tests.

### Create and run a Docker container from the production image

1. Below environment variables should be available in your shell environment to run a Docker container from the production image.
   1. FE_URL `// URL of the frontend application without the protocol. Ex: job-career`
   1. BE_URL `// URL of the backend application without the protocol. Ex: job-career-admin`
   1. APP_NAME `// name of the backend application`
   1. APP_KEY `// Key used for ecryption and decryption. Should be 32 characters long. Should not be changed once set.`
   1. APP_URL `// URL of the backend application. Include the protocol of the URL. i.e. http:// or https://`
   1. APP_TIMEZONE `// timezone of the application. See https://www.php.net/manual/en/timezones.php`
   1. APP_LOCALE `// locale of the application in ISO 639-1 format. See https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes.`
   1. DB_HOST `// host address of the database service`
   1. DB_PORT `// TCP port of the database service`
   1. DB_DATABASE `// name of the database`
   1. DB_USERNAME `// username of the user in the database service`
   1. DB_PASSWORD `// password of the user in the database service`
   1. REDIS_HOST `// host address of the Redis service`
   1. REDIS_PORT `// TCP port of the Redis service`
   1. REDIS_PASSWORD `// password of the Redis service`
   1. REDIS_DB `// index of the Redis database for persisting data`
   1. REDIS_CACHE_DB `// index of the Redis database for cache`
   1. MAIL_HOST `// host address of the mail service`
   1. MAIL_PORT `// TCP port of the mail service`
   1. MAIL_USERNAME `// username of the mail service`
   1. MAIL_PASSWORD `// password of the mail service`
   1. MAIL_ENCRYPTION `// encryption scheme of mail connection`
   1. MAIL_FROM_ADDRESS `// email address which should be set as the from address in emails`
   1. MHUB_BASEURL `// URL of the mHub API`
   1. MHUB_MOCK_API_ENDPOINTS `// Set to true if API requests to mHub should be mocked. Otherwise set to false.`
   1. SUPER_ADMIN_EMAIL `email of the super admin`
   1. SUPPORT_EMAIL `// email of the technical support`
   1. COMPANY_NAME `name of the company the application is installed for`
   1. COMPANY_CONTACT_EMAIL `// contact email of the company`
   1. COMPANY_FE_URL `// URL of the FE website of the company without the protocol`
   1. COMPANY_MHUB_USERNAME `// mHub username issued for the company`
   1. COMPANY_MHUB_PASSWORD `// mHub password issued for the company`
   1. SENTRY_LARAVEL_DSN `// set to https://8562fb357f6c4a1287263d83fa046370@o572821.ingest.sentry.io/5737810`
   1. SENTRY_TRACES_SAMPLE_RATE `// set to 1`
   1. PASSPORT_PRIVATE_KEY `// RSA private key used for Laravel Passport. Should not be changed once set.`
   1. PASSPORT_PUBLIC_KEY `// RSA public key used for Laravel Passport. Should not be changed once set.`
1. Or else you can use the `.env` file created in a previous section and set values to above environment variables in it.
1. Customize the frontend application if needed. Check the section **Customize the frontend application** below for more instructions.
1. Run `docker-compose run -d --service-ports production client_name` to run the start and run a Docker container.
   Replace the `client_name` with the name of the client. Check the section **Customize the frontend application** below
   for more instructions. If a custom config file has not been created for the client use `default`.

## How to deploy updates to the application

### Update the application

1. Login to your web server via SSH.
1. Go to the project directory. `cd /path-to-your-projet`
1. Run `git stash`.
1. Run `git fetch`.
1. Run `git checkout branch_name`. Replace `branch_name` with the name of the branch.
1. Run `git pull`.
1. Run `docker-compose down --rmi all` to stop and remove the running Docker container
1. Run `docker-compose build --force-rm` to build the testing and production images again.
1. Run `docker-compose run testing` to run the unit tests.
1. Run `docker-compose run -d --service-ports production client_name` start and run the production Docker container.
   Replace the `client_name` with the name of the client. Check the section **Customize the frontend application** below
   for more instructions. If a custom config file has not been created for the client use `default`.

## Backend application

#### Super admin

Super admin is a user in the backend application with the role "Super admin". The super admin has all the permissions in the backend application.

Email of the super admin should be set in the env param `SUPER_ADMIN_EMAIL`.

#### Creating new users

New users can be created in the backend application via the Users module. The default password of every new user is `password`.
New users can change their password using the "Forgot your password? / Zapomněli jste heslo?" link in the login screen.

#### Changing settings (email content)

Currently the content of the "Successfully Applied For Vacancy" email can be changed.

1. Go to `Settings` admin module.
2. Click on `Update` button for the `Successfully Applied For Vacancy email` setting.
3. Select the `Locale` from the dropdown and set the content of email in the `Value` field.
4. Click on `Update` button to save the changes.

## Frontend application

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

#### Career Pages Specific info

##### Translations

Please see document "fe-translations.md" in the repo for more info.

##### Config files

There are 3 types of config files in the project:

- config.example.json -- a json which contains all of the possible conditions and rules
- config.default.json -- a json which contains default settings
- config.client.json -- a json which is used to override default settings. Are overrided only the settings which are mentioned in the config.client.json. For example, if your client config contains only a setting for a button color, it means that all of the other settings will be taken from the default config. When you're changing the order of the components, if there's no properties for a component, it will take the default properties of the first component of this type from the default config.

To apply the custom styles, you should run the scripts as follows: npm run build --styles=[name] or npm run dev --styles=[name]

For example, if you run npm run build --styles=client, it will take config.client.json.
If you run npm run build --styles=pepsi, it will take config.pepsi.json.

##### Windows Users

Use the following command to start the webserver:
npm run dev:win --styles=[name]
Use regular Command Prompt. Do not use Powershell.

##### Setting the API URL's

When running a _build_ or for _local dev testing_, the API URL must be defined in the parameter --apiurl when invoking the npm run build or npm run dev commands.

E.g.

- npm run dev --styles=superface --apiurl=https://job-career-admin.testing2.creativehandles.com/api/v1/
- npm run build --styles=datasentics --apiurl=https://job-career-admin.testing2.creativehandles.com/api/v1/

Note: for development, the value for apiurl parameter when running or building superface and datasentics (customer environments), must point to the testing instance.

##### Setting the Language

When running a _build_ or for _local dev testing_, there should be a param called "language" which specifies which language translations should be used. E.g. --language=en

##### Setting the Build Output folder

When running a _build_, there should be a param called "targetdir" which specifies to which folder the output should be copied to. E.g. --targetdir=company1

##### Setting the Company FE URL

When running a _build_, there should be a param called "company_fe_url" which specifies where the FE code will be hosted. E.g. --company_fe_url=http://job-career.testing2.creativehandles.com/

Note that the above param should end with a slash.

##### Setting the Google Tag Manager(GTM) ID

When running a _build_, there should be a param called "gtm*id" which specifies the GTM ID for analytics. E.g. --gtm_id=GTM-KSNWM22. \_IMPORTANT* - if you are not wanting to pass a value for gtm_id, then you can pass in "INVALID", as npm doesn't allow null values to be passed in.

Please see document "important-gtm-info.md" in the repo for more info.

#### Customize the frontend application

Frontend application can be customized using a `frontend/config.[client_name].json` file. In the `frontend` folder, a default config file
can be found with default configurations: `config.default.json`. Run `cp frontend/config.default.json frontend/config.[client_name].json`
to make a copy of `frontend/config.default.json` and rename as `frontend/config.[client_name].json`. Then set the customization in that config file
according to the properties explained below.

Then run `docker-compose build --force-rm production` to rebuild the production Docker image. After that
run `docker-compose run -d --service-ports production [client_name]` to start and run a production Docker container.
Replace `[client_name]` with the name of the client.

The properties in that file are explained below.

- `logo`

  This property is used to customize the logo, which is displayed in the upper-right corner.
  It’s better to have it as a svg, but it also can be a png or any other format. Set it as an html element.

  Example:

  `"logo": "<svg height='20' width='45'><text x='0' y='15' fill='#192a59'>LOGO</text></svg>",`

- `gallery`

  This property is an object with the `height` property, which sets the height of images and video elements in the main page.
  The widths of the videos and the images are responsive and set by the container.

  Example:

  `"gallery": { "height": "201" },`

- `styles`

  This is the main property for global styles. It's value is an object with the below properties:

  - `titleColor` - sets the color of the header in the home page
  - `fontColor` - sets the color of the texts in all pages
  - `buttonColor` - sets the color of the buttons in the home page and in the Opportunities page
  - `fontSize` - sets the size of the texts in all pages
  - `background` - sets the background color of all pages
  - `borderRadius` - sets the radius of the boundaries of the main elements, such as cards, drag and drop, and small parts such as buttons

  Example:

  ```json
  "styles": {
      "titleColor": "#192a59",
      "fontColor": "#192a59",
      "buttonColor": "#3f5aa6",
      "fontSize": "14px",
      "background": "#fafbff",
      "borderRadius": "5px"
  },
  ```

- `agreement`

  This property is an object with the properties `home` and `vacancy`, and the values of these properties determine whether the GDPR agreement component
  will be displayed in particular pages.

  Example:

  `"agreement": { "home": "true", "vacancy": "true" },`

- `text`

  This property is an object with the `agreement` property, which specifies the text that is displayed when GDPR agreement button is clicked.

  Example:

  ```json
  "text": {
      "agreement": "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
  }
  ```

- `metaTags`

  This property is an object with the `Home` property. It's value is an object with properties containing values for meta tags of the home page.

  Example:

  ```json
  "metaTags": {
     "Home": {
          "title": "Home",
          "description": "Meta description",
          "keywords": "Meta Keywords",
          "robots": "Meta robots",
          "meta-title": "Meta title"
     }
  },
  ```

- `componentsOrderHome`

  This property defines the components of the home page. It is an array of objects with below properties:

  - `componentName` - name of the component
  - `componentProps` - an object with the properties passed to the component

  The main feature of this property is that, the components will be displayed in the order of the objects that are added.

  The object with the `componentName` property set to `"main"` represents the main component of the home page. There can only be one such component.
  `componentProps` property of that object is an empty object.

  The objects with `componentName` property set to `"text"` represent the text sections in the home page. There can be any no. of such components.
  `componentProps` property of these objects is an object with below properties:

  - `images` - an array of links to images that is displayed in the component
  - `background` - background color of the text
  - `reverse` - the displaying order of the text and the images. If it is set to `false`, then the images will be displayed to the left,
    and the text to the right. If it is set to `true`, then vice versa.
  - `header` - the header displayed above the text
  - `text` - the text displayed in the component

  Example:

  ```json
  "componentsOrderHome": [
      {
          "componentName": "text",
          "componentProps": {
              "images": [
                  "https://picsum.photos/200/300",
                  "https://picsum.photos/200/300"
              ],
              "background": "#fff",
              "reverse": "false",
              "header": "Our company values",
              "text": "Proin sodales pulvinar sic tempor."
          }
      },
      {
          "componentName": "main",
          "componentProps": {}
      },
      {
           "componentName": "text",
           "componentProps": {
                "background": "#fff",
                "reverse": "false",
                "header": "Our company cares",
                "text": "Proin sodales pulvinar sic tempor. <iframe title='vimeo-player' src='https://player.vimeo.com/video/76979871' frameborder='0' allowfullscreen></iframe>"
           }
      }
  ],
  ```

- `componentsOrderDetail`

  This property defines the components of the opportunity details page in the mobile version. It accepts the same values as the `componentsOrderHome` property.

  Example:

  ```json
  "componentsOrderDetail": [
      {
          "componentName": "main",
          "componentProps": {}
      }
  ],
  ```

- `OpportunitiesOrder`

  This property defines the components of the Opportunities page in the desktop version. It accepts the same values as the `componentsOrderHome` property.

  Example:

  ```json
  "OpportunitiesOrder": [
      {
          "componentName": "main",
          "componentProps": {
              "text": "<iframe title='vimeo-player' src='https://player.vimeo.com/video/76979871' frameborder='0' allowfullscreen></iframe> Not sure about applying? Watch this video to see how it is to work in our technology driven company."
          }
      }
  ],
  ```

## Common issues

#### Throwing `throw er; // Unhandled 'error' event` error when running `npm run dev` or `npm run prod`

This is a very common issue with `npm run dev` or `npm run prod` and has a very generic solution. See [How to Fix "throw er; // Unhandled 'error' event"?](https://peterthaleikis.com/posts/how-to-fix-throw-er-unhandled-error-event.html) for more details.
Run below commands to solve the error.

1. `rm -rf node_modules`
1. `rm -f package-lock.json`
1. `npm cache clear --force`
1. `npm install`
1. `npm run dev` or `npm run prod`

#### Throwing `ERROR: Service 'production' failed to build : Build failed` error when building the production image by running `docker-compose build --force-rm production`

This happens due to a time out during the build of the image. A know solution is to increase the allocated resources to
the Docker in the host computer. i.e. CPU and RAM allocations.
