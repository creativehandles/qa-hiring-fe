#!/usr/bin/env python3

import os
import sys
import yaml
import json

input_file = sys.argv[1]
output_folder = sys.argv[2]
base_name = input_file.split('.')[0]
output_path = os.path.join(
    output_folder, '{}.json'.format(base_name))

with open(input_file, 'r') as f:
    d = yaml.safe_load(f)

with open(output_path, 'w') as f:
    variables = []
    for k, v in d.items():
        variables.append({
            "name": k,
            "value": v,
            "slotSetting": False
        })
    json.dump(variables, f)

print('Writing variables: {}'.format(variables))
