## Settings

#### General steps

1. Create a new azure container registry in the resource group *common* named *careerpages* (if you choose different name change the pipelines)
2. Enable admin user access to the container registry resource in *Access keys* on the resource page


#### Environment preparation - Do those only when adding a new environment

1. Create a new resource group: careerpages-*environment*
2. Create a new maria db in the resource group: careerpages-*environment* (do not enforce SSL connection)
   1. Enable Allow access to Azure services in Connection security settings
   2. Save the database connection credentials to variables-*environment*.yml
3. Modify both pipelines by adding a new environment parameter value and add conditional logic for the new environment

#### Settings for the new client

1. Create users in BE
   1. Atlas
   2. OneHub
      1. add ATS account
      2. add rights for OneHub 
2. Create variables/*clientname*-variables.yml (and variables/*clientname*-*environment*-variables.yml for other environments) file with client-specific variables (look at the example: datasentics-variables.yml)
3. Adjust both azure-pipelines-infra.yml azure-pipelines-multitenant.yml based on comments in the pipeline to add a new client

- you can run the IaC pipeline after this step

#### Settings of the webapp

- preconditions: The IaC must already by run for the *clientname*, *environment* pair.

1. Validate careerpages-*environment* resource group:
   1. Web App careerpages-*client* was created
   2. App Insights careerpages-*client* was created
2. Go to a new Web App, go to App Insights:
   1. Click on Turn on Application Insights
   2. Select existing Insights careerpages-*client*
3. Go to Custom domains in the left panel: 
   1. turn on HTTPS Only
   2. Add custom domain and write domain (eg. mytimi.jobno.one)
      1. Go to DNS resource in portal and fallow manual under DNS propagation
      2. Validate
      3. Add custom domain
   3. Add custom domain and write domain (eg. admin-mytimi.jobno.one)
      1. Go to DNS resource in portal and fallow manual under DNS propagation
      2. Validate
      3. Add custom domain
4. Go to TSL/SSL settings in the left panel:
   1. Add private key certificate and Create App Service Managed Certificates
   2. Create private certificate for both domains
5. Go to Custom domains in the left panel:
   1. Add binding for each domain 

## Deployment steps

#### Run a CI/CD pipeline for a new client 
1. Go to file **variables/general-variables.yml** and add a new client into **TENANTS, API_URLS**
2. Create /variables/*clientname*-*environment*-variables.yml file for a new client
5. Run **CI and CD** pipeline

---
#### *Note:* regarding the capacity of the App Service Plan:
- current plan: P2V2
- for scaling up or down
1. go to azure-pipelines-infra.yml
2. in the stage CreatingResources > inline script > change the --sku value in line (66): az appservice plan create --name \$(resourceGroupName) --resource-group $(resourceGroupName) --is-linux --sku **P2V2**
          
