#!/usr/bin/bash

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

echo 'Clearing backend cache'

cd "${PROJECT_DIRECTORY}/backend"

php artisan optimize:clear

echo "Deploying the commit ${COMMIT}"

cd "${PROJECT_DIRECTORY}"

git stash
git fetch
git -c advice.detachedHead=false checkout "${COMMIT}"

echo 'Deploying backend started'

# backend deployement scripts
cd "${PROJECT_DIRECTORY}/backend"

if [ "${DEPLOYMENT_ENV}" == "test" ]
then
  composer install
else
  composer install --no-dev
fi

php artisan migrate --force
php artisan db:seed --force
php artisan cms:populate-permissions
php artisan config:cache
php artisan route:trans:cache
php artisan view:cache
php artisan queue:restart

npm install

if [ "${DEPLOYMENT_ENV}" == "test" ]
then
  npm run dev
else
  npm run prod
fi

if [ "${DEPLOYMENT_ENV}" == "test" ]
then
  php artisan apidoc:generate
fi

chown -R apache:apache bootstrap/cache
chown -R apache:apache storage

if [ "${DEPLOYMENT_ENV}" == "test" ]
then
  php artisan migrate --force --env=testing
  php artisan test --exclude-group ignore
fi

echo 'Deploying backend finished'

echo 'Deploying frontend started'

# frontend deployement scripts
cd "${PROJECT_DIRECTORY}/frontend"
rm -rf out
npm install
npm run build --styles="${TENANT}" --apiurl="${BACKEND_URL}/api/v1/" --targetdir="${TENANT}" --company_fe_url="${FRONTEND_URL}/" --language="${LOCALE}" --gtm_id="${GTM_ID}"
ln -s "builds/${TENANT}" out
chown -R apache:apache out
cp .htaccess.example out/.htaccess

echo 'Deploying frontend finished'
