## Important GTM Info

### Adding GTM IDs

In the application, we are performing `window.dataLayer.push()` to push events to Google Tag Manager (GTM)’s data layer.

The GTM ID itself needs to be set as a build parameter. Please refer the README.md file for more info.

The Google Analytics(GA) ID’s that are passed in these push events can be configured in each customer’s .config file.

E.g. the default customer has the config of the following format.

  ```json
  "googleTagManager": { 
      "GAID": "GAIDXXX", 
      "tenantID": "default", 
      "tenantGAID": "tenantGAIDXXX",
      "tenantFBID": "tenantFBIDXXX", 
      "tenantLIID": "tenantLIIDXXX" 
  },
  ```

This will result in the following payload in GTM data layer:

  ```
  { 
    event: "gtm.load",  
    gtm: {uniqueEventId: 5,  
    start: 1633325591029},   
    GAID: "GAIDXXX",  
    tenantID:"default",   
    tenantGAID: "tenantGAIDXXX",  
    tenantFBID:"tenantFBIDXXX",  
    tenantLIID: "tenantLIIDXXX" 
  }
  ```

If either **GTM ID or GAID is blank,** then a data layer push event will **not** take place.

In the individual tenants, the `googleTagManager` tag values are inherited from `default`. However, they can be overridden. E.g. values for superface can take the following format:

  ```json
  "googleTagManager": {
    "GAID": "GAIDBBB",
    "tenantID": "superface",
    "tenantGAID": "",
    "tenantFBID": "",
    "tenantLIID": ""
  },
  ```

If the parameters (e.g. “tenantGAID") are left out, they will inherit their values from default.config.json.

### Important note

Remember to send `undefined` for any variables you don’t want to send in each `datalayer.push()` request. As GTM remembers the previous set of variables that were passed.

https://www.simoahava.com/gtm-tips/remember-to-flush-unused-data-layer-variables/
