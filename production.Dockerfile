FROM ubuntu:20.04

# Set Environment Variables
#
# set bash commands to non-interactive mode
ARG DEBIAN_FRONTEND=noninteractive
# set Timezone
ARG TZ=${TZ}

# install packages
RUN LC_ALL=en_US.UTF-8 apt-get update \
    && apt-get upgrade -y \
    # install applications
    && apt-get install -y --no-install-recommends software-properties-common \
    apache2 \
    cron \
    curl \
    gettext-base \
    language-pack-en-base \
    openssl \
    supervisor \
    vim \
    zip unzip \
    # install apache2 mods
    && apt-get install -y --no-install-recommends libapache2-mod-php7.4 \
    # enable apache2 mods
    && a2enmod rewrite deflate headers proxy_fcgi setenvif \
    # install PHP and PHP libraries
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/apache2 \
    && apt-get update \
    && apt-get install -y --no-install-recommends php7.4-fpm \
    php7.4-bcmath \
    php7.4-ctype \
    php7.4-curl \
    php7.4-fileinfo \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-memcached \
    php7.4-mysql \
    php7.4-pdo \
    php7.4-redis \
    php7.4-tidy \
    php7.4-tokenizer \
    php7.4-xml \
    php7.4-xmlrpc \
    php7.4-zip

# do the cleaning and configurations
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && sed -i -e 's/;\?daemonize\s*=\s*yes/daemonize = no/g' /etc/php/7.4/fpm/php-fpm.conf \
    && mkdir -p /run/php/ \
    # install composer
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.0.6 \
    # add user group and user
    && groupadd -g 1000 www \
    && useradd -u 1000 -ms /bin/bash -g www www \
    # create log directories and log files
    && touch /var/log/php7.4-fpm.log \
    && mkdir -p /var/log/httpd \
    && touch /var/log/httpd/access_log /var/log/httpd/error_log \
    && ln -sf /dev/stdout /var/log/httpd/access_log \
    && ln -sf /dev/stderr /var/log/httpd/error_log

# copy configurations
#
# copy PHP pool config
COPY ./docker/config/www.pool.conf /etc/php/7.4/fpm/pool.d/www.conf

# add a configuration to /etc/apache2/apache2.conf
RUN echo "ServerName 127.0.0.1" >> /etc/apache2/apache2.conf

# copy apache2 vhosts config
COPY ./docker/config/vhosts.conf /etc/apache2/sites-available/vhosts.conf

# symlink apache2 configs
RUN a2enconf php7.4-fpm \
    && ln -sf /etc/apache2/sites-available/vhosts.conf /etc/apache2/sites-enabled/vhosts.conf

# copy crontab
COPY ./docker/config/crontab /etc/cron.d/www

# set permissions of crontab
# @see https://serverfault.com/a/924780
RUN chmod -R 644 /etc/cron.d \
# create the log file to be able to run tail
    && mkdir -p /var/log/cron \
    && touch /var/log/cron/www.log \
    && chown -R www:www /var/log/cron

# copy supervisor config
COPY ./docker/config/supervisord.conf /etc/supervisord.conf

# copy supervisor program config
COPY ./docker/config/job-career.supervisor.conf /etc/supervisord.d/job-career.conf

# make the directory for log files of supervisor and assign permissions
RUN mkdir -p /var/log/supervisor \
    && chmod -R guo+w /var/log/supervisor

# install Node.js and nvm
ARG NODE_VERSION='16.8.0'
ARG NPM_FETCH_RETRIES=2
ARG NPM_FETCH_RETRY_FACTOR=10
ARG NPM_FETCH_RETRY_MINTIMEOUT=10000
ARG NPM_FETCH_RETRY_MAXTIMEOUT=60000
ARG NVM_DIR=/home/www/.nvm

# install nvm (a Node Version Manager)
RUN mkdir -p $NVM_DIR \
    && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install ${NODE_VERSION} \
    && nvm use ${NODE_VERSION} \
    && nvm alias ${NODE_VERSION} \
    && npm config set fetch-retries ${NPM_FETCH_RETRIES} \
    && npm config set fetch-retry-factor ${NPM_FETCH_RETRY_FACTOR} \
    && npm config set fetch-retry-mintimeout ${NPM_FETCH_RETRY_MINTIMEOUT} \
    && npm config set fetch-retry-maxtimeout ${NPM_FETCH_RETRY_MAXTIMEOUT} \
    && npm install -g gulp \
    && npm install -g cross-env
# wouldn't execute when added to the RUN statement in the above block
RUN ln -s `npm bin --global` /home/www/.node-bin
# wouldn't execute when added to the RUN statement in the above block
# Source NVM when loading bash since ~/.profile isn't loaded on non-login shell
RUN echo "" >> ~/.bashrc && \
    echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

# add NVM binaries to root's .bashrc
USER root

RUN echo "" >> ~/.bashrc && \
    echo 'export NVM_DIR="/home/www/.nvm"' >> ~/.bashrc && \
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.bashrc

# add PATH for node
ENV PATH $PATH:/home/www/.node-bin

# Make it so the node modules can be executed with 'docker-compose exec'.
# We'll create symbolic links into '/usr/local/bin'.
RUN find $NVM_DIR -type f -name node -exec ln -s {} /usr/local/bin/node \; && \
    NODE_MODS_DIR="$NVM_DIR/versions/node/$(node -v)/lib/node_modules" && \
    ln -s $NODE_MODS_DIR/bower/bin/bower /usr/local/bin/bower && \
    ln -s $NODE_MODS_DIR/gulp/bin/gulp.js /usr/local/bin/gulp && \
    ln -s $NODE_MODS_DIR/npm/bin/npm-cli.js /usr/local/bin/npm && \
    ln -s $NODE_MODS_DIR/npm/bin/npx-cli.js /usr/local/bin/npx && \
    ln -s $NODE_MODS_DIR/vue-cli/bin/vue /usr/local/bin/vue && \
    ln -s $NODE_MODS_DIR/vue-cli/bin/vue-init /usr/local/bin/vue-init && \
    ln -s $NODE_MODS_DIR/vue-cli/bin/vue-list /usr/local/bin/vue-list

# set work directory
WORKDIR /var/www

# copy backend
COPY ./backend /var/www/backend

# run backend build scripts
#
# set working directory to backend
WORKDIR /var/www/backend

# make .env file and add parameters
RUN touch .env && \
    echo "APP_ENV=production" >> /var/www/backend/.env && \
    echo "APP_DEBUG=false" >> /var/www/backend/.env && \
    echo "LOG_CHANNEL=stdout" >> /var/www/backend/.env && \
    echo "TELESCOPE_ENABLED=false" >> /var/www/backend/.env && \
    echo "SESSION_DRIVER=file" >> /var/www/backend/.env && \
    echo "SESSION_LIFETIME=2880" >> /var/www/backend/.env && \
    echo "PERSONAL_ACCESS_CLIENT_ID=" >> /var/www/backend/.env && \
    echo "PERSONAL_ACCESS_CLIENT_SECRET=" >> /var/www/backend/.env && \
    echo "PASSWORD_GRANT_CLIENT_ID=" >> /var/www/backend/.env && \
    echo "PASSWORD_GRANT_CLIENT_SECRET=" >> /var/www/backend/.env && \
    echo "MAIL_FROM_NAME=\"\${APP_NAME}\"" >> /var/www/backend/.env && \
# file system configurations
    echo "ALLOWED_FILE_EXTENSIONS=" >> /var/www/backend/.env && \
    echo "ALLOWED_FILE_CATEGORIES=\"temporary,avatar,cv\"" >> /var/www/backend/.env && \
    echo "DEFAULT_RECORDS_PER_PAGE=10" >> /var/www/backend/.env && \
# this param is from Job#1 Demo to set the grace period before deleting a candidate who's CV failed to get parsed by mHub
    echo "DELETE_FAILED_CANDIDATES_AFTER_HOURS=1" >> /var/www/backend/.env && \
# https://www.php.net/manual/en/dateinterval.construct.php
    echo "GDPR_AGREED_PERIOD=P10Y" >> /var/www/backend/.env && \
# https://www.php.net/manual/en/dateinterval.construct.php
    echo "GDPR_DISAGREED_PERIOD=P3M" >> /var/www/backend/.env && \
# cron notation. See https://ole.michelsen.dk/blog/schedule-jobs-with-crontab-on-mac-osx/
    echo "GET_VACANCIES_FROM_MHUB_FREQUENCY=\"*/30 * * * *\"" >> /var/www/backend/.env && \
# system configurations - notification configurations
    echo "NOTIFICATIONS_SEND_EMAIL=true" >> /var/www/backend/.env && \
    echo "NOTIFICATIONS_SEND_SMS=false" >> /var/www/backend/.env && \
    echo "NOTIFICATIONS_SEND_SLACK=false" >> /var/www/backend/.env && \
    echo "NOTIFICATIONS_SMS_SERVICE_PROVIDER=\"\"" >> /var/www/backend/.env && \
# this param sets the grace period before deleting incomplete candidates. i.e. candidates without an email.
    echo "COMPANY_DELETE_INCOMPLETE_CANDIDATES_OLDER_THAN=24" >> /var/www/backend/.env

# set permissions to backend directory
RUN chown -R www:www /var/www/backend

# set user to www
USER www

# build backend
RUN composer install --no-dev && \
    php artisan storage:link && \
    npm install && \
    npm run prod

# set user to root
USER root

# copy frontend and set permissions
COPY ./frontend /var/www/frontend
RUN chown -R www:www /var/www/frontend

# run frontend build scripts
#
# set working directory to frontend
WORKDIR /var/www/frontend

# set user to www
USER www

# build frontend
RUN npm install \
    && rm -rf out

ARG TENANTS=${TENANTS}
ARG API_URLS={$API_URLS}
ARG FE_URLS={$FE_URLS}
ARG TENANT_LOCALES={$TENANT_LOCALES}
ARG GTM_IDS={$GTM_IDS}

# copy FE build script and run it
COPY docker/fe_build.sh /fe_build.sh
RUN bash /fe_build.sh

# set working directory back to www
WORKDIR /var/www

# set user to root
USER root

# copy entrypoint
COPY docker/entrypoint.production.sh /entrypoint.sh
RUN ["chmod", "+x", "/entrypoint.sh"]

ENTRYPOINT ["/entrypoint.sh"]